# -*- coding: utf-8 -*-

import allure
from POM.loginpage import LoginPage

@allure.feature ("Basic temp test")
@allure.story("check open page")
@allure.title("test good url")
def test_001_basic(app):
   lp2 = LoginPage(app.wd)
   lp2.get("https://google.com")
   pass


@allure.feature ("Basic temp test")
@allure.story("check open page")
@allure.title("test bad url")
def test_011_basic(app):
   lp2 = LoginPage(app.wd)
   lp2.get("https://googladsae.com")
   pass

