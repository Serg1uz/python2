# -*- coding: utf-8 -*-


import pytest
from fixture.application import Application

fixture = None
target = None


@pytest.yield_fixture()
def app():
    global fixture
    if fixture is None:
        fixture = Application()
    yield fixture
    fixture.destroy()


