# -*- coding: utf-8 -*-

import os
from selenium import webdriver


class Application:
    def __init__(self):
        profile = webdriver.FirefoxProfile()
        profile.set_preference("browser.download.folderList", 2)
        profile.set_preference("browser.download.manager.showWhenStarting", False)
        profile.set_preference("browser.download.dir",
                               '/home/sergeych/Downloads')  # !!! Установка каталога для Загрузки
        profile.set_preference("browser.helperApps.neverAsk.saveToDisk",
                               "text/csv;application/vnd.ms-excel")  # !!! Перечисление типов файлов для которых не будет выпадать окно загрузки
        self.wd = webdriver.Firefox(firefox_profile=profile)
        self.wd.implicitly_wait(5)

    def destroy(self):
        self.wd.quit()

