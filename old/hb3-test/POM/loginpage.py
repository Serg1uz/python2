# -*- coding: utf-8 -*-
from POM.basepage import Basepage
from selenium.webdriver.common.by import By


class LoginPage(Basepage):
    # default uri
    _url = "https://processing-stage.megadepotllc.com/login"

    # Locators
    _username = (By.NAME, 'username1')
    _password = (By.NAME, 'password')
    _login_btn = (By.XPATH, ".//*[@id='app']/div[2]/div[2]/div/div/div[3]/label/div/a/span")

    # def __init__(self, wd):
    #     super().__init__(wd)
    #     # open default url
    #     # self.open()
    #     # self.wait_page_loaded(timeout=5)

    @property
    def username_field(self):
        return super().get_element(self._username)

    @property
    def password_field(self):
        return super().get_element(self._password)

    @property
    def login_btn(self):
        return super().get_element(self._login_btn)

    def open(self):
        self.get(self._url)

    def login(self, usern, passw):
        self.username.send_keys(usern)
        self.password.send_keys(passw)
        self.login_btn.click()
        self.wait_page_loaded()
