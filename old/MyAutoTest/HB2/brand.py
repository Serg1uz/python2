
import unittest

from selenium import webdriver

base_url="http://handbook2-stage.uttc-usa.com"

class Brand(unittest.TestCase):

    def setUp(self):
        self.driver=webdriver.Firefox()
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()

        self.driver.get(base_url)

    def test_Brand(self):
        print("")
        print("Brand")
        driver = self.driver
        driver.get(base_url + "/manufacturer/brands")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Brand").click()
        driver.find_element_by_id("brand-name").send_keys("Tests")
        driver.find_element_by_id("select2-brand-company-uri-container").click()
        driver.find_element_by_css_selector(
            "span.select2-search.select2-search--dropdown > input.select2-search__field").click()
        driver.find_element_by_css_selector("li.select2-results__option.select2-results__option--highlighted").click()
        driver.find_element_by_id("brand-rating").send_keys("2")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("brand-name").clear()
        driver.find_element_by_id("brand-name").send_keys("Tests 2")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()
