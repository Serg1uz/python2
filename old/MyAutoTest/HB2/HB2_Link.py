#Файл с глобальными переменнами определяемые линки
#File with global variables defined links

#Main first link
BaseLink="http://handbook2-stage.uttc-usa.com"

#Tester Login and password
Login=""
Password=""

#Sublinks

    #Manuf_Terms
    Manuf_Terms="/manufacturer/terms"

    #Statistica
    Statistica="/help/article/statistica"

    #Коллектив
    Employees="/corporation/employees"

    #Коллектив: схема расположения
    EmployeesScheme="/corporation/working/employees/schedules/scheme"

    #Список задач
    Issues="/corporation/working/issues"

    #Catalog
        #База направлений
        IndustrialsProducts="/catalog/group/industrial-products"

    #Corporation
        #Calendar Turns
        CalendarTurns="/corporation/calendar-turns"
        #Companies
        Companies="/corporation/companies"
        #Holidays
        Holidays="/corporation/holidays"
        #Issue Spreadsheet
        IssueSpreadsheet="/corporation/working/issues/spreadsheet"
        #Locations
        Locations="/corporation/locations"
        #Positions
        Positions="/corporation/positions"
        #Showcases
        Showcases="/corporation/showcases"
        #Units
        Units="/corporation/units"

    #Help
        #Algorythm CS
        AlgorythmCS="/help/article/customer-service"
        #Algorytm Alex
        AlgorythmAlex="/help/article/algoritmy-dla-aleksa"
        #Algorytm works
        AlgorythmWork="/help/article/algoritmy-raboty"
        #Reading articles
        ReadingArticles="/help/articles/revisions/readings"
        #Statistika
        StatistikaHelp="/help/article/statistica"
        #Articles for reading
        ArticlesMustReading="/help/articles/revisions/readings/take"
        #Working with Manuf.
        WorkingWithManuf="/help/article/working-with-manufacturers"

    #Manufacturers
        #Companies
        CompaniesM="/manufacturer/companies"
        #Contacts
        Contacts="/manufacturer/contacts"
        #Resselers
        Resselers="/manufacturer/resellers"
        #Talk_logs
        TalkLogs="/manufacturer/talks/creativity"
        #Terms
        Terms="/manufacturer/terms"
        #Brands
        Brands="/manufacturer/brands"
            #Alert
            BrandsAlert="/manufacturer/brands/statuses?states=alert&amp;employees="
            #Warning
            BrandsWarning="/manufacturer/brands/statuses?states=warning&amp;employees="
        #Statistics
            #Current/Overdue
            CurrentOverdue="/manufacturer/brands/statuses/summary/by-status"
            #Full Current/Overdue
            FullCurrentOverdue="/manufacturer/brands/statuses"
            #Task Current/Overdue
            TaskCurrentOverdue="/manufacturer/reminders/summary"
            #Add Brand to DataBase
            AddBrandToDataBase="/manufacturer/brands/creativity"
            #Statistics Brands uped
            StatisticBrandsUped="/manufacturer/brands/status/account-set-up/changes/summary"
            #Status Brand
            StatusBrand="/manufacturer/brands/statuses/changes/summary"

        #Supports
            #Invoices
            Invoices="/support/invoices"
            #Reports
            Reports="/support/reports"
            #Workgroups
            Workgroups="/support/workgroups"
            #Worklists
            Worklists="/support/worklists"
            #Works
            Works="/support/works"
            #Contractors
            SupportsContractors="/support/contractors"
            #Contracts
            SupportsContacts="/support/contracts"
        #Training
            #Courses
            Courses="/help/courses/revisions"
            #Exams
            Exams="/help/exams"
            #Questions
            Questions="/help/questions"
            #Video lessons
            VideoLessons="/help/article/video-uroki"
        #Transportation
            #Employee Ride
            EmployeeRide="/corporation/transportation/employees/rides"
            #Employee Ride Worksheet
            EmployeeRideWorksheet="/corporation/transportation/employees/rides/worksheet"
            #Employee Taxi Ride
            EmployeeTaxiRide="/corporation/transportation/employees/taxies/rides"
            #Report (Transportation)
            ReportTransportation="/corporation/transportation/reports"
            #Report Employee
            TransportationReportEmployee="/corporation/transportation/reports/employees"
            #Report Taxi
            ReportTaxi="/corporation/transportation/reports/taxies"
            #Taxi
            Taxi="/corporation/transportation/taxies"
            #Taxi Ride
            TaxiRide="/corporation/transportation/taxies/rides"
            #Taxi Ride Worksheet
            TaxiRideWorksheet="/corporation/transportation/taxies/rides/worksheet"
            #Add Ride
            AddRide="/corporation/transportation/employee/taxi/ride"
        #Work Log
            #Dynamics
            Dynamics="/corporation/working/employees/logs/dynamics"
            #IssueLogs
            IssueLogs="/corporation/working/issues/logs"
            #EmployeesLogs
            EmployeesLogs="/corporation/working/employees/logs"
            #EmployeesIssueLogs
            EmployeesIssueLogs="/corporation/working/employees/issues/logs"
            #Absences
            Absences="/corporation/working/absences"
            #Timesheets
            Timesheets="/corporation/working/timesheets"
            #Timesheets employees
            TimesheetsEmployees="/corporation/working/timesheets/employees"
            #Timesheets issues
            TimesheetsIssues="/corporation/working/timesheets/issues"
            #Timesheets employees??????? <li><a href="/corporation/working/timesheets/employees" tabindex="-1">Отчет по сотруднику</a></li>
            TimesheetsEmployee="/corporation/working/timesheets/employees"
            #Absences reason
            AbsencesReasons="/corporation/working/absences/reasons"
            #Shifts
            Shifts="/corporation/working/shifts"
            #Shedules
            Shedules="/corporation/working/employees/schedules"
            #Absences Timesheet
            AbsencesTimesheet="/corporation/working/absences/timesheet"
            #Issues Timesheet
            IssuesTimesheet="/corporation/working/issues/logs/timesheet"
            #EmployeesTimesheet
            EmployeesTimesheet="/corporation/working/employees/logs/timesheet"
        #Corporated mail
            #Inbox
            Inbox="/corporation/inbox"
            #Outbox
            Outbox="/corporation/outbox"
        #Settings
            #Routine
            Routine="/corporation/working/routine"
            #Settings label
            SettingsLabels="/corporation/working/issues/labels"
            #Settings procrastinations
            SettingsProcrastinations="/manufacturer/procrastinations"
        #Turniket
            #Attedance
            Attedance="/corporation/working/attendances"
            #Attedance Events
            AttedanceEvents="/corporation/working/attendances/events"
            #Presence
            Presence="/corporation/working/presences"
        #Users
#href="/access/user" data-toggle="dropdown"><div class="avatar"><div class="employee-icon icon"><img class="photo" src="/uploads/corporation/employee/photo.45x45/aokunev.jpg" srcset="/uploads/corporation/employee/photo.90x90/aokunev.jpg 2x" width="45" alt="" data-src="/uploads/corporation/employee/photo.550x550/aokunev.jpg" data-original-title="Александр Окунев"></div>Александр Окунев</div></a><ul id="w13" class="dropdown-menu"><li><a class="message" href="#corporation/outbox/message" tabindex="-1">Compose message</a></li>
#<li><a class="message" href="#corporation/working/employee/issue/log" tabindex="-1">Create Work Log</a></li>
#<li><a class="inbox" href="/corporation/inbox" tabindex="-1">Inbox</a></li>
#<li><a class="settings" href="/access/settings" tabindex="-1">Settings</a></li>
#<li><a class="logout" href="/auth/logout" data-method="post" tabindex="-1">Logout</a></li></ul></li></ul>