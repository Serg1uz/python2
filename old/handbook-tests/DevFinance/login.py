from base import base_url, login, password


class LoginPage:
    def __init__(self, driver):
        self.driver = driver

    def loginform(self):
        driver = self.driver
        driver.get(base_url + "/auth/login")
        driver.find_element_by_id("loginForm-username").send_keys(login)
        driver.find_element_by_id("loginForm-password").send_keys(password)
        driver.find_element_by_name("login-button").click()
