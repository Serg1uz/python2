import time
import os

base_url = "https://devfinance2-stage.itdevelopcorp.com"
login = "aokunev"
password = "7777777"


class PageCUD:
    def __init__(self, driver):
        self.driver = driver

    def clickSaveButton(self):
        self.driver.find_element_by_css_selector("button.save").click()

    def clickUpdateButton(self):
        self.driver.find_element_by_link_text("Update").click()

    def clickCompleteButton(self):
        self.driver.find_element_by_link_text("Complete").click()

    def clickCloseButton(self):
        self.driver.find_element_by_link_text("Close").click()

    def clickOpenButton(self):
        self.driver.find_element_by_link_text("Open").click()

    def clickDeleteButton(self):
        self.driver.find_element_by_link_text("Delete").click()
        self.driver.find_element_by_css_selector("button.delete").click()

    def clickDeleteOne(self):
        self.driver.find_element_by_css_selector("button.delete").click()

    def clickCancelButton(self):
        self.driver.find_element_by_link_text("Cancel").click()

    def clickSuccessButton(self):
        self.driver.find_element_by_css_selector(".alert-success > button.close").click()

    def clickShowButton(self):
        self.driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()

    def clickReviseButton(self):
        self.driver.find_element_by_link_text("Revise").click()

    def cke_editor(self, div_id):
        self.driver.find_element_by_xpath("//div[@id='" + div_id + "']//div[contains(@class, 'cke_wysiwyg_div')]").send_keys("Autotest")

    def cke_editor_new(self, div_id):
        self.driver.find_element_by_xpath("//div[@id='" + div_id + "']//div[contains(@class, 'cke_wysiwyg_div')]").clear()
        self.driver.find_element_by_xpath("//div[@id='" + div_id + "']//div[contains(@class, 'cke_wysiwyg_div')]").send_keys("Autotest New")

    def dropDownList(self, div_class, xpath_2):
        self.driver.find_element_by_id(div_class).click()
        self.driver.find_element_by_xpath(xpath_2).click()

    def dropDownListCss(self, div_class, xpath_2):
        self.driver.find_element_by_css_selector(div_class).click()
        self.driver.find_element_by_xpath(xpath_2).click()

    def dropDownListXpath(self, div_class, xpath_2):
        self.driver.find_element_by_xpath(div_class).click()
        self.driver.find_element_by_xpath(xpath_2).click()

    def dropDownListThree(self, div_class, xpath_1, xpath_2):
        self.driver.find_element_by_id(div_class).click()
        time.sleep(2)
        self.driver.find_element_by_xpath(xpath_1).click()
        self.driver.find_element_by_xpath(xpath_2).click()

    def dropDownListDateFinance(self, div_class, xpath_1, xpath_2, xpath_3, xpath_4):
        self.driver.find_element_by_id(div_class).click()
        time.sleep(2)
        self.driver.find_element_by_xpath(xpath_1).click()
        self.driver.find_element_by_xpath(xpath_2).click()
        self.driver.find_element_by_xpath(xpath_3).click()
        self.driver.find_element_by_xpath(xpath_4).click()

    def upload(self, input_id):
        self.driver.find_element_by_id(input_id).send_keys(os.getcwd() + "/../Handbook/test.png")
