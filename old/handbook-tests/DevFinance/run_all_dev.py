# -*- coding: utf-8 -*-
from account_dev import *
from vendor_dev import *
from currency_dev import *
from accounting_account_dev import *
from create_inventory_dev import *
from report_payroll_dev import *
from transfer_dev import *
from payroll_wage_dev import *
from vacation_dev import *
from loss_dev import *
from advance_dev import *
from import_from_db import *
from pages_dev import *
from billing_dev import *
from accountability_dev import *
from working_view_dev import *


if __name__ == "__main__":
    unittest.main()
