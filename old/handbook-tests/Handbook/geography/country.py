# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Country(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_country(self):
        print("")
        print("Country")
        driver = self.driver
        driver.get(base_url + "/geography/countries")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Country").click()
        driver.find_element_by_id("country-code").send_keys("4b")
        driver.find_element_by_id("select2-country-macroRegionCode-container").click()
        driver.find_element_by_css_selector("input.select2-search__field").send_keys("Eur")
        driver.find_element_by_css_selector("li.select2-results__option.select2-results__option--highlighted").click()
        driver.find_element_by_id("country-name").send_keys("Test")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("country-name").clear()
        driver.find_element_by_id("country-name").send_keys("Test 1")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def test_country_with_region(self):
        print("")
        print("Country with Region")
        driver = self.driver
        driver.get(base_url + "/geography/countries")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Country").click()
        driver.find_element_by_id("country-code").send_keys("4z")
        driver.find_element_by_id("select2-country-macroRegionCode-container").click()
        driver.find_element_by_css_selector("input.select2-search__field").send_keys("Eur")
        driver.find_element_by_css_selector("li.select2-results__option.select2-results__option--highlighted").click()
        driver.find_element_by_id("country-name").send_keys("Test")
        self.clickSaveButton()
        driver.find_element_by_link_text("create a new one").click()
        driver.find_element_by_id("region-code").send_keys("1SS")
        driver.find_element_by_id("region-name").send_keys("1SS")
        self.clickSaveButton()
        driver.find_element_by_xpath("(//a[contains(text(),'Test')])[2]").click()
        self.clickDeleteButton()
        self.clickCancelButton()
        driver.find_element_by_link_text("1SS").click()
        self.clickDeleteButton()
        driver.find_element_by_xpath("(//a[contains(text(),'Test')])").click()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
