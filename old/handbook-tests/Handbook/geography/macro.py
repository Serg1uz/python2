# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Macro(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_macro(self):
        print("")
        print("Macro Region")
        driver = self.driver
        driver.get(base_url + "/geography/macro-regions")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Macro Region").click()
        driver.find_element_by_id("macroRegion-code").send_keys("39")
        driver.find_element_by_id("macroRegion-name").send_keys("Test")
        driver.find_element_by_id("macroRegion-continent").send_keys("Test")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("macroRegion-name").clear()
        driver.find_element_by_id("macroRegion-name").send_keys("Test 1")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def test_macro_delete_with_country(self):
        print("")
        print("Macro Region delete with Country")
        driver = self.driver
        driver.get(base_url + "/geography/macro-regions")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Macro Region").click()
        driver.find_element_by_id("macroRegion-code").send_keys("35")
        driver.find_element_by_id("macroRegion-name").send_keys("Test")
        driver.find_element_by_id("macroRegion-continent").send_keys("Test")
        self.clickSaveButton()
        driver.find_element_by_link_text("create a new one").click()
        driver.find_element_by_id("country-code").send_keys("A2")
        driver.find_element_by_id("country-name").send_keys("A2")
        self.clickSaveButton()
        driver.find_element_by_xpath("//dd[@id='macro-region/35']/a").click()
        self.clickDeleteButton()
        self.clickCancelButton()
        driver.find_element_by_link_text("A2").click()
        self.clickDeleteButton()
        self.clickSuccessButton()
        driver.get(base_url + "/geography/macro-region/35")
        self.clickDeleteButton()
        self.clickSuccessButton()

    def test_macro_delete_with_country_and_pz(self):
        print("")
        print("Macro Region delete with Country and Postal Zone")
        driver = self.driver
        driver.get(base_url + "/geography/macro-regions")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Macro Region").click()
        driver.find_element_by_id("macroRegion-code").send_keys("61")
        driver.find_element_by_id("macroRegion-name").send_keys("Test")
        driver.find_element_by_id("macroRegion-continent").send_keys("Test")
        self.clickSaveButton()
        driver.find_element_by_link_text("create a new one").click()
        driver.find_element_by_id("country-code").send_keys("Z2")
        driver.find_element_by_id("country-name").send_keys("Z1")
        self.clickSaveButton()
        driver.find_element_by_link_text("create a new one").click()
        driver.find_element_by_id("region-code").send_keys("12E")
        driver.find_element_by_id("region-name").send_keys("12E")
        self.clickSaveButton()
        driver.find_element_by_link_text("create a new one").click()
        driver.find_element_by_id("postalZone-postcode").send_keys("12EE1")
        self.dropDownListCss("span.select2-selection__placeholder",
                             "//ul[@id='select2-postalZone-type-results']/li[2]")
        self.dropDownListCss("span.select2-selection__placeholder",
                             "//ul[@id='select2-postalZone-regionCode-results']/li")
        driver.find_element_by_id("postalZone-city").send_keys("Test")
        self.clickSaveButton()
        driver.find_element_by_link_text("Z1").click()
        self.clickDeleteButton()
        self.clickCancelButton()
        driver.find_element_by_link_text("12E").click()
        self.clickDeleteButton()
        self.clickCancelButton()
        driver.find_element_by_link_text("12EE1").click()
        self.clickDeleteButton()
        self.clickSuccessButton()
        driver.find_element_by_xpath("//div[@id='breadcrumbs-pjax-container']/ul/li[4]").click()
        driver.find_element_by_link_text("Z1").click()
        driver.find_element_by_link_text("12E").click()
        self.clickDeleteButton()
        self.clickSuccessButton()
        driver.find_element_by_link_text("Z1").click()
        self.clickDeleteButton()
        self.clickSuccessButton()
        driver.get(base_url + "/geography/macro-region/61")
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
