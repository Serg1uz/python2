import glob
# import random
# import string
import sys
import unittest
import datetime
import os
import requests

sys.path.append("..")

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from base import base_url, PageCUD
from login import LoginPage

class TestHolidays(unittest.TestCase, PageCUD):
    @classmethod
    def setUpClass(cls):
        # Добавление  профиля для Firefox, что бы не было окна выбора загрузки
        cls.profile = webdriver.FirefoxProfile()
        cls.profile.set_preference("browser.download.folderList", 2)
        cls.profile.set_preference("browser.download.manager.showWhenStarting", False)
        cls.profile.set_preference("browser.download.dir",
                                   '/home/sergeych/Downloads')  # !!! Установка каталога для Загрузки
        cls.profile.set_preference("browser.helperApps.neverAsk.saveToDisk",
                                   "text/csv;application/vnd.ms-excel")  # !!! Перечисление типов файлов для которых не будет выпадать окно загрузки

        #Регистрация драйвера
        cls.driver = webdriver.Firefox(log_path="../Webdriver.log",firefox_profile=cls.profile)
        cls.driver.implicitly_wait(10)
        cls.driver.maximize_window()
        cls.driver.title
        #Авторизация на сайте
        login = LoginPage(cls.driver)
        login.loginform()



    # Проверка открытие основной странички теста
    def test_1_openurl(self):
        print ("")
        print ("Open url Holidays")
        driver=self.driver
        driver.get(base_url + "/corporation/holidays")
        req = requests.get(driver.current_url).status_code
        assert (req == 200 or req == 300), " Can't open url: " + driver.current_url
        pass

    # Проверка Advanced info panel
    # Проверка Hot key btn
    def test_2_advanced_info_hbtn(self):
        print("")
        print("Advanced info: hot key btn")
        driver = self.driver
        driver.get(base_url + "/corporation/holidays")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_xpath("//a[@class='hotkey btn']").click()
        hdt = driver.find_element_by_xpath("//*[@id='modalHeader']/h1")
        # print (hdt.text)
        assert hdt.text == "Hotkey"
        driver.find_element_by_xpath("//div[@id='modalHeader']/button[@class='close']").click()
        pass

    # Проверка Personalise grid btn
    def test_3_advanced_info_personalizebtn(self):
        print("")
        print("Advanced info: personalize columns btn")
        driver = self.driver
        driver.get(base_url + "/corporation/holidays")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_xpath("//button[@data-original-title='Personalize grid settings']").click()
        driver.implicitly_wait(10)
        txt=driver.find_element_by_xpath("//label[@for ='theme-grid-corporation-holiday']")
        print(txt.text)
        driver.find_element_by_xpath("//button[@data-original-title='Save grid settings']").click()
        try:
            assert txt.text == "Grid Theme"
        except AssertionError as error:
            print (error)
        driver.implicitly_wait(15)
        pass

    # Проверка configuration btn
    def test_4_advanced_info_confbtn(self):
        print ("")
        print ("Advanced info: configuration columns btn")
        driver = self.driver
        driver.get(base_url + "/corporation/holidays")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        WebDriverWait(driver, 5).until_not(EC.visibility_of_element_located((By.XPATH,"//div[@class='modal-backdrop fade']")))
        driver.find_element_by_xpath(" //button[@id='personalize-grid-edit']").click()
        driver.implicitly_wait(15)
        btnapply=driver.find_element_by_xpath("//button[@id='personalize-grid-apply']")
        btnapplyD=btnapply.is_displayed()
        btnapply.click()
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        assert btnapplyD == True
        pass

    # Проверка експорт в csv
    def test_5_advanced_info_expcsv(self):
        print("")
        print("Advanced info: export csv btn")
        driver = self.driver
        driver.get(base_url + "/corporation/holidays")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        WebDriverWait(driver, 5).until_not(
            EC.visibility_of_element_located((By.XPATH, "//div[@class='fade modal in']")))
        if not driver.find_element_by_xpath("//a[@id='mysql-export-csv']").is_displayed():
            driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_xpath("//a[@id='mysql-export-csv']").click()
        driver.implicitly_wait(120)
        downloaddir = "/home/sergeych/Downloads/"
        td = datetime.date.today()
        filename = "Corporation Holiday " + td.strftime("%Y-%m-%d") + " *.csv"
        ff = downloaddir + filename
        files = glob.glob(ff)
        chfile = False
        if len(files) > 0: chfile = True

        if chfile:
            for f in files: os.remove(f)

        assert chfile == True
        pass

    # Проверка експорт в xls
    def test_6_advanced_info_expxls(self):
        print("")
        print("Advanced info: export xls btn")
        driver = self.driver
        driver.get(base_url + "/corporation/holidays")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        if not driver.find_element_by_xpath("//a[@id='mysql-export-csv']").is_displayed():
            driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_xpath("//a[@id='mysql-export-xls']").click()
        driver.implicitly_wait(120)
        downloaddir = "/home/sergeych/Downloads/"
        td = datetime.date.today()
        filename = "Corporation Holiday " + td.strftime("%Y-%m-%d") + " *.xls"
        ff = downloaddir + filename
        files = glob.glob(ff)
        chfile = False
        if len(files) > 0: chfile = True
        print(files)
        if chfile:
            for f in files: os.remove(f)

        assert chfile == True
        pass

    # Проверка save configuration
    def test_7_advanced_info_saveconfig(self):
        print("")
        print("Advanced info: save config btn")
        driver = self.driver
        driver.get(base_url + "/corporation/holidays")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_xpath(" //button[@class='btn btn-default save-configuration']").click()
        ch = driver.find_element_by_xpath(" //input[@id='dynaGridConfigFilter-isShared']").is_displayed()
        driver.find_elements_by_xpath("//div[@class ='modal-header']/button[@ class ='close']")[1].click()
        assert ch == True
        pass

    # Проверка фулл текст поиска
    def test_8_fulltextsearch(self):
        print("")
        print("Full text search: check status code")
        driver = self.driver
        driver.get(base_url + "/corporation/holidays")
        driver.find_element_by_xpath("//input[@id='holidaySearch-query']").send_keys("test")
        driver.find_element_by_xpath("//button[@class='search']").click()
        driver.implicitly_wait(10)
        req = requests.get(driver.current_url).status_code
        print(req)
        assert (req == 200) or (req == 302)
        # Reset filters / Сброс фильтров(поиска)
        print("Reset filter")
        driver.find_element_by_xpath("//a[@class='refresh']").click()
        txt = driver.find_element_by_xpath("//input[@id='holidaySearch-query']").text
        req = requests.get(driver.current_url).status_code
        assert (txt == '') and ((req == 200) or (req == 302))
        pass

    # Проверка фильтра
    def test_9_filters(self):
        print("")
        print("Filters: check status code")
        driver = self.driver
        driver.get(base_url + "/corporation/holidays")
        driver.find_element_by_xpath("//a[@class='show-advanced']").click()
        driver.find_element_by_xpath("//input[@class='select2-search__field']").click()
        driver.find_element_by_xpath(
            "//ul[@id='select2-holidaySearch-names-results']/li[@aria-selected='false']").click()
        driver.find_elements_by_xpath("//button[@class='search']")[1].click()
        txtsearch = driver.find_element_by_xpath("//li[@class='select2-selection__choice']").text
        txtsearch = txtsearch[2:].lstrip()
        print("search text = " + txtsearch)
        resultsearch = driver.find_element_by_xpath("//td[@class='name']/a").text
        print("result text = " + resultsearch)
        assert txtsearch == resultsearch, 'Filter is not work'
        # Reset filters / Сброс фильтров(поиска)
        print("Reset filter")
        driver.find_element_by_xpath("//a[@class='refresh']").click()
        txt = driver.find_element_by_xpath("//input[@id='holidaySearch-query']").text
        req = requests.get(driver.current_url).status_code
        assert (txt == '') and ((req == 200) or (req == 302))
        pass

    # Проверка основного функционала
    def test_a1_main(self):
        print("")
        print("Main functional")
        driver = self.driver
        driver.get(base_url + "/corporation/holidays")
        driver.find_element_by_link_text("Create Holiday").click()
        txt = driver.find_element_by_xpath("//header/h1").text
        req = requests.get(driver.current_url).status_code
        assert (txt == 'Create Holiday') and ((req == 200) or (req == 302)), "Don't open create page"

        requiredinputs=driver.find_elements_by_xpath("//input[@aria-required='true']")
        for ri in requiredinputs:
            ri.click()
            ri.clear()
        driver.find_element_by_xpath("//body").click()

        for ri in requiredinputs:
            assert ri.get_attribute("aria-invalid") == 'true', 'test validation empty field failed for - '+ri.get_attribute("id")

        # Заполнение всех полей
        td = datetime.date.today()
        nameholiday='autotest - '+ td.strftime("%Y-%m-%d")
        driver.find_element_by_id('holiday-name').clear()
        driver.find_element_by_id('holiday-name').send_keys(nameholiday)
        driver.find_element_by_id('holiday-startYear').clear()
        driver.find_element_by_id('holiday-startYear').send_keys(td.year)
        driver.find_element_by_id('holiday-endYear').clear()
        driver.find_element_by_id('holiday-endYear').send_keys(td.year+2)
        driver.find_element_by_id('select2-holiday-rule-container').click()
        driver.find_element_by_xpath("//ul[@class='select2-results__options']/li[@aria-selected='false']").click()
        driver.find_element_by_id('holiday-month').clear()
        driver.find_element_by_id('holiday-month').send_keys(td.month)
        driver.find_element_by_id('holiday-day').clear()
        driver.find_element_by_id('holiday-day').send_keys(td.day)
        driver.find_element_by_xpath("//div[@role='textbox']").send_keys('Holidays create by autotest at '+td.strftime("%Y-%m-%d"))
        self.clickSaveButton()
        req = requests.get(driver.current_url).status_code
        assert ((req == 200) or (req == 302)), 'Failed create url ' + driver.current_url
        assert driver.find_element_by_xpath("//dd[@class='name']").text == nameholiday, 'Failed create holiday'

    #     open new tab for finance holiday
        driver.execute_script("window.open('https://finance2-stage.megadepotllc.com/corporation/holidays');")
        driver.switch_to.window(driver.window_handles[1])
        driver.find_element_by_id("loginForm-username").send_keys('aokunev')
        driver.find_element_by_id("loginForm-password").send_keys('7777777')
        driver.find_element_by_name("login-button").click()

        driver.get("https://finance2-stage.megadepotllc.com/corporation/holidays/import")
        driver.find_element_by_id("holidaySearch-query").send_keys('"'+nameholiday+'"')
        driver.find_element_by_xpath("//button[@class='search']").click()

        txtresult=driver.find_element_by_xpath("//td[@class='name']").text
        assert txtresult == nameholiday, 'Failed synchronise create'

        driver.switch_to.window(driver.window_handles[0])
        self.clickUpdateButton()
        req = requests.get(driver.current_url).status_code
        assert ((req == 200) or (req == 302)), 'Failed create url ' + driver.current_url
        nameholiday=nameholiday+'-update'
        driver.find_element_by_id('holiday-name').clear()
        driver.find_element_by_id('holiday-name').send_keys(nameholiday)
        self.clickSaveButton()

        driver.switch_to.window(driver.window_handles[1])
        driver.get("https://finance2-stage.megadepotllc.com/corporation/holidays/import")
        driver.find_element_by_id("holidaySearch-query").clear()
        driver.find_element_by_id("holidaySearch-query").send_keys('"'+nameholiday+'"')
        driver.find_element_by_xpath("//button[@class='search']").click()

        txtresult = driver.find_element_by_xpath("//td[@class='name']").text
        assert txtresult == nameholiday, 'Failed synchronise update'

        driver.switch_to.window(driver.window_handles[0])
        self.clickDeleteButton()
        driver.switch_to.window(driver.window_handles[1])
        driver.get("https://finance2-stage.megadepotllc.com/corporation/holidays/import")
        driver.find_element_by_id("holidaySearch-query").clear()
        driver.find_element_by_id("holidaySearch-query").send_keys('"'+nameholiday+'"')
        driver.find_element_by_xpath("//button[@class='search']").click()

        assert driver.find_element_by_xpath("//div[@class='empty']").is_displayed() == True, 'Failed synchronise delete'
        pass









    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()
        # cls.driver.title


if __name__ == '__main__':
    unittest.main()