# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Brand(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_brand(self):
        print("")
        print("Brand")
        driver = self.driver
        driver.get(base_url + "/manufacturer/brands")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Brand").click()
        driver.find_element_by_id("brand-name").send_keys("Tests")
        driver.find_element_by_id("select2-brand-company-uri-container").click()
        driver.find_element_by_css_selector(
            "span.select2-search.select2-search--dropdown > input.select2-search__field").click()
        driver.find_element_by_css_selector("li.select2-results__option.select2-results__option--highlighted").click()
        driver.find_element_by_id("brand-rating").send_keys("2")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("brand-name").clear()
        driver.find_element_by_id("brand-name").send_keys("Tests 2")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def test_brand_all(self):
        print("")
        print("Brand all")
        driver = self.driver
        driver.get(base_url + "/manufacturer/brands")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Brand").click()

        text = ["brand-name", "reminder-todo", "reminder-situation", "reminder-followUp", "reminder-takenAction",
                "brief-competitor", "brief-bestseller", "brief-organicSearch", "brief-checkOnAmazon",
                "brief-checkOnEbay"]
        for item in text:
            driver.find_element_by_id("%s" % (item)).send_keys("Test")

        cke = ["cke_brand-passportInitialQuestion", "cke_brand-passportInitialPricingAnalysis", "cke_brandStatus-note",
               "cke_brand-referral", "cke_brief-category", "cke_brief-comment"]
        for item in cke:
            self.cke_editor(item)

        driver.find_element_by_id("select2-brandStatus-code-container").click()
        driver.find_element_by_xpath("//ul[@id='select2-brandStatus-code-results']/li").click()
        driver.find_element_by_id("select2-brandStatus-substatus-uri-container").click()
        driver.find_element_by_xpath("//ul[@class='select2-results__options']/li[@aria-selected='false']").click()
        driver.find_element_by_id("reminder-todoTime").click()
        driver.find_element_by_css_selector("td.day.active").click()
        driver.find_element_by_css_selector("span.hour.active").click()
        driver.find_element_by_id("reminder-followUpPhone").send_keys("1111111111")
        driver.find_element_by_id("reminder-followUpEmail").send_keys("test@dfsdf.sd")

        driver.find_element_by_xpath("//div[@id='reminder-takenActionTime-datetime']/span").click()
        driver.find_element_by_xpath("//div[6]/div[3]/table/tbody/tr[3]/td[3]").click()
        driver.find_element_by_xpath("//div[6]/div[2]/table/tbody/tr/td/span[11]").click()
        driver.find_element_by_id("reminder-followUpTime").click()
        driver.find_element_by_xpath("//div[7]/div[3]/table/tbody/tr[3]/td[3]").click()
        driver.find_element_by_xpath("//div[7]/div[2]/table/tbody/tr/td/span[11]").click()

        driver.find_element_by_xpath("//input[@placeholder='Select Responsible Employees']").click()
        driver.find_element_by_xpath("//ul[@id='select2-reminder-employees-uri-results']/li[2]").click()
        driver.find_element_by_xpath("//input[@placeholder='Select Responsible Department']").click()
        driver.find_element_by_xpath("//ul[@id='select2-reminder-units-uri-results']/li[2]").click()
        driver.find_element_by_id("select2-brand-company-uri-container").click()
        driver.find_element_by_css_selector(
            "span.select2-search.select2-search--dropdown > input.select2-search__field").clear()
        driver.find_element_by_css_selector(
            "span.select2-search.select2-search--dropdown > input.select2-search__field").click()
        driver.find_element_by_css_selector("li.select2-results__option.select2-results__option--highlighted").click()

        driver.find_element_by_css_selector("span.select2-selection__placeholder").click()
        driver.find_element_by_id("brand-rating").send_keys("3")
        driver.find_element_by_xpath("(//input[@placeholder='Select Categories'])").click()
        driver.find_element_by_css_selector("li.select2-results__option.select2-results__option--highlighted").click()
        driver.find_element_by_xpath("(//input[@placeholder='Select Work Through'])").click()
        driver.find_element_by_css_selector("li.select2-results__option.select2-results__option--highlighted").click()
        driver.find_element_by_xpath("(//input[@placeholder='Select Competitors'])").click()
        driver.find_element_by_css_selector("li.select2-results__option.select2-results__option--highlighted").click()
        driver.find_element_by_id("select2-brand-reseller-uri-container").click()
        driver.find_element_by_xpath("//ul[@id='select2-brand-reseller-uri-results']/li[2]").click()
        driver.find_element_by_id("brand-priceMin").send_keys("1")
        driver.find_element_by_id("brand-priceMax").send_keys("2")

        driver.find_element_by_id("brief-googleShoppingUrl").send_keys("http://google.com")
        self.upload("brand-attachments-upload")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("reminder-todo").clear()
        driver.find_element_by_id("reminder-todo").send_keys("Test 1")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()
        driver.back()
        driver.find_element_by_css_selector("ul.breadcrumb > li:nth-child(4) > a").click()
        driver.find_element_by_link_text("Restore").click()
        driver.find_element_by_css_selector("button.restore").click()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
