# -*- coding: utf-8 -*-
import sys
import time
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Company(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_company(self):
        print("")
        print("Company")
        driver = self.driver
        driver.get(base_url + "/manufacturer/companies")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Company").click()
        driver.find_element_by_id("company-name").send_keys("Test")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("company-name").clear()
        driver.find_element_by_id("company-name").send_keys("Test 1")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def test_company_all(self):
        print("")
        print("Company all")
        driver = self.driver
        driver.get(base_url + "/manufacturer/companies")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Company").click()
        driver.find_element_by_id("company-name").send_keys("Test")

        time.sleep(2)
        self.cke_editor("cke_company-description")

        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("company-name").clear()
        driver.find_element_by_id("company-name").send_keys("Test 1")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
