# -*- coding: utf-8 -*-
import sys
import time
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Categories(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_categories(self):
        print("")
        print("Categories")
        driver = self.driver
        driver.get(base_url + "/manufacturer/categories")
        # driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.implicitly_wait(10)
        driver.find_element_by_link_text("Create Category").click()
        driver.find_element_by_id("category-name").send_keys("Test")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("category-name").clear()
        driver.find_element_by_id("category-name").send_keys("Test 12")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def test_categories_with_brand(self):
        print("")
        print("Categories all")
        driver = self.driver
        driver.get(base_url + "/manufacturer/categories")
        # driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.implicitly_wait(10)
        driver.find_element_by_link_text("Create Category").click()
        time.sleep(2)
        driver.find_element_by_id("category-name").send_keys("Test")
        driver.find_element_by_css_selector("input.select2-search__field").send_keys("autote")
        time.sleep(2)
        driver.find_element_by_css_selector("li.select2-results__option.select2-results__option--highlighted").click()
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("category-name").clear()
        driver.find_element_by_id("category-name").send_keys("Test 12")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickCancelButton()
        self.clickUpdateButton()
        time.sleep(2)
        driver.find_element_by_class_name("select2-selection__clear").click()
        driver.find_element_by_css_selector("h1").click()
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
