# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Substatus(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_substatus(self):
        print("")
        print("Substatuses")
        driver = self.driver
        driver.get(base_url + "/manufacturer/substatuses")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Substatus").click()
        driver.find_element_by_id("substatus-name").send_keys("autotest")
        driver.find_element_by_id("select2-substatus-code-container").click()
        driver.find_element_by_xpath("//ul[@id='select2-substatus-code-results']/li[@aria-selected='false']").click()
        self.cke_editor("cke_substatus-description")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("substatus-name").clear()
        driver.find_element_by_id("substatus-name").send_keys("autotest New")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
