# -*- coding: utf-8 -*-
import sys
import time
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Talk(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_talk(self):
        print("")
        print("Talk")
        driver = self.driver
        driver.get(base_url + "/manufacturer/brand/newcon-optik")
        driver.find_element_by_css_selector("p.action > a.create").click()
        time.sleep(2)
        self.cke_editor("cke_talk-note")
        self.clickSaveButton()
        driver.find_element_by_link_text("Update").click()
        self.cke_editor_new("cke_talk-note")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def test_talk_all(self):
        print("")
        print("Talk all")
        driver = self.driver
        driver.get(base_url + "/manufacturer/brand/newcon-optik")
        driver.find_element_by_css_selector("p.action > a.create").click()
        time.sleep(2)
        self.dropDownList("select2-talk-caller-uri-container",
                          "//span[2]/ul/li[4]")
        self.dropDownListThree("talk-callTime",
                               "//tr[4]/td[3]", "//span[14]")
        self.cke_editor("cke_talk-note")
        self.clickSaveButton()
        self.clickUpdateButton()
        self.cke_editor_new("cke_talk-note")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
