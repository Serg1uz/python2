# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Promo(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_promo(self):
        print("")
        print("Promo")
        driver = self.driver
        driver.get(base_url + "/manufacturer/term/kanomax-usa")
        try:
            driver.find_element_by_link_text("create a new one").click()
        except:
            driver.find_element_by_link_text("Create").click()
        driver.find_element_by_id("promo-couponCode").send_keys("12312")
        self.cke_editor("cke_promo-description")
        self.clickSaveButton()
        self.clickUpdateButton()
        self.cke_editor_new("cke_promo-description")
        driver.switch_to.default_content()
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def test_promo_all(self):
        print("")
        print("Promo all")
        driver = self.driver
        driver.get(base_url + "/manufacturer/term/kanomax-usa")
        try:
            driver.find_element_by_link_text("create a new one").click()
        except:
            driver.find_element_by_link_text("Create").click()
        driver.find_element_by_id("promo-couponCode").send_keys("12312")
        driver.find_element_by_id("promo-startTime").send_keys("2017-10-25 11:00:00")
        driver.find_element_by_id("promo-expirationTime").send_keys("2017-10-26 11:00:00")
        self.cke_editor("cke_promo-description")
        self.clickSaveButton()
        self.clickUpdateButton()
        self.cke_editor_new("cke_promo-description")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
