# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Links(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_links(self):
        print("")
        print("Links")
        driver = self.driver
        try:
            driver.get(base_url + "/manufacturer")
            driver.find_element_by_xpath("//th[2]/a").click()
        except:
            print ("Error in " + driver.current_url)

        try:
            driver.get(base_url + "/manufacturer/creativities")
            driver.find_element_by_link_text("Brand").click()
        except:
            print ("Error in " + driver.current_url)

        try:
            driver.get(base_url + "/manufacturer/talks/creativity")
            driver.find_element_by_css_selector("time.year.is-current").click()
        except:
            print ("Error in " + driver.current_url)

        try:
            driver.get(base_url + "/manufacturer/talks/calling")
            driver.find_element_by_link_text("2017").click()
        except:
            print ("Error in " + driver.current_url)

        try:
            driver.get(base_url + "/manufacturer/brands/statuses")
            driver.find_element_by_link_text("Warning Time").click()
        except:
            print ("Error in " + driver.current_url)

        try:
            driver.get(base_url + "/manufacturer/talks/calling")
            driver.find_element_by_link_text("2017").click()
        except:
            print ("Error in " + driver.current_url)

        try:
            driver.get(base_url + "/manufacturer/reminders/summary")
            driver.find_element_by_link_text("Past").click()
        except:
            print ("Error in " + driver.current_url)

        try:
            driver.get(base_url + "/manufacturer/brands/statuses/changes")
            driver.find_element_by_link_text("New").click()
        except:
            print ("Error in " + driver.current_url)

        try:
            driver.get(base_url + "/manufacturer/brands/statuses/changes/summary")
            driver.find_element_by_link_text("2017").click()
        except:
            print ("Error in " + driver.current_url)

        try:
            driver.get(base_url + "/manufacturer/brands/statuses/summary")
            driver.find_element_by_link_text("Alert Time").click()
        except:
            print ("Error in " + driver.current_url)

        try:
            driver.get(base_url + "/manufacturer/brands/statuses/summary/by-status")
            driver.find_element_by_link_text("Alert Time").click()
        except:
            print ("Error in " + driver.current_url)

        try:
            driver.get(base_url + "/manufacturer/brands/statuses/summary/by-responsible")
            driver.find_element_by_link_text("Alert Time").click()
        except:
            print ("Error in " + driver.current_url)

        try:
            driver.get(base_url + "/manufacturer/brands/status/account-set-up/changes")
            driver.find_element_by_link_text("2").click()
        except:
            print ("Error in " + driver.current_url)

        try:
            driver.get(base_url + "/manufacturer/brands/status/account-set-up/changes/summary")
            driver.find_element_by_link_text("2017").click()
        except:
            print ("Error in " + driver.current_url)

        try:
            driver.get(base_url + "/manufacturer/brands/statuses/check")
            self.clickSuccessButton()
        except:
            print ("Error in " + driver.current_url)

        try:
            driver.get(base_url + "/manufacturer/brands/statuses/board")
            driver.find_element_by_xpath("//h3[contains(text(),'Brand Status Summary Warning')]")
        except:
            print ("Error in " + driver.current_url)

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
