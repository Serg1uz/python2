# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Reseller(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_reseller(self):
        print("")
        print("Reseller")
        driver = self.driver
        driver.get(base_url + "/manufacturer/resellers")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Reseller").click()
        driver.find_element_by_id("reseller-name").send_keys("Test")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("reseller-name").clear()
        driver.find_element_by_id("reseller-name").send_keys("Test 12")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
