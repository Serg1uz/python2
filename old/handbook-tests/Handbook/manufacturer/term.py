# -*- coding: utf-8 -*-
import sys
import time
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Term(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_term(self):
        print("")
        print("Term")
        driver = self.driver
        driver.get(base_url + "/manufacturer/brand/newcon-optik/term")
        driver.find_element_by_id("term-name").send_keys("Test")

        self.dropDownList(
            "select2-payment-method-container",
            "//ul[@id='select2-payment-method-results']/li[@aria-selected='false']"
        )

        self.dropDownList(
            "select2-keep-credit-card-container",
            "//ul[@id='select2-keep-credit-card-results']/li[@aria-selected='false']"
        )

        self.dropDownList(
            "select2-accounting-creditCardAttachment-container",
            "//ul[@id='select2-accounting-creditCardAttachment-results']/li[@aria-selected='false']"
        )

        driver.find_element_by_name("Pricing[withFreightForwardInProfit]").click()
        driver.find_element_by_id("pricing-startDate").send_keys("2018-04-18")
        driver.find_element_by_id("select2-pricing-requestFrequency-container").click()
        driver.find_element_by_xpath("//ul[@id='select2-pricing-requestFrequency-results']/li").click()
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("term-name").clear()
        driver.find_element_by_id("term-name").send_keys("Test 1")
        self.clickSaveButton()
        driver.implicitly_wait(10)
        self.clickDeleteButton()
        self.clickSuccessButton()

    def test_term_all(self):
        print("")
        print("Term all")
        driver = self.driver
        driver.get(base_url + "/manufacturer/brand/newcon-optik/term")

        cke = [
            "cke_1_contents", "cke_2_contents", "cke_3_contents", "cke_4_contents",
            "cke_5_contents", "cke_6_contents", "cke_7_contents", "cke_8_contents",
            "cke_9_contents", "cke_10_contents", "cke_11_contents", "cke_12_contents",
            "cke_13_contents", "cke_14_contents", "cke_15_contents", "cke_16_contents",
            "cke_17_contents", "cke_18_contents", "cke_19_contents", "cke_20_contents",
            "cke_21_contents", "cke_22_contents", "cke_23_contents", "cke_24_contents",
            "cke_25_contents", "cke_26_contents", "cke_27_contents", "cke_28_contents",
            "cke_29_contents", "cke_30_contents", "cke_31_contents", "cke_32_contents",
            "cke_33_contents", "cke_34_contents", "cke_35_contents", "cke_36_contents",
            "cke_37_contents", "cke_38_contents", "cke_39_contents", "cke_40_contents",
            "cke_41_contents", "cke_42_contents", "cke_43_contents", "cke_44_contents",
            "cke_45_contents",
        ]
        for item in cke:
            self.cke_editor(item)

        inp = [
            "shipping-hasExpressService", "term-acceptCustomerSertificate", "shipping-isAvailableViaCustomer",
            "term-isEmailable", "term-distributorHasOrderOnline", "term-distributorOrderOnlineIsUsed",
            "shipping-hasPickUp",
        ]
        for item in inp:
            driver.find_element_by_id("%s" % (item)).click()

        text = [
            "term-name", "shipping-manufacturerAccount", "term-logo"
        ]
        for item in text:
            driver.find_element_by_id("%s" % (item)).send_keys("Test")

        numb = [
            "term-dropShipmentTariff", "term-handlingTariff", "term-accountNumber", "term-minOrderAmount",
            "term-lowOrderFee", "term-insuranceMinOrderAmount", "term-distributorOrderOnlineAmountValue",
        ]
        for item in numb:
            driver.find_element_by_id("%s" % (item)).send_keys("1")

        driver.find_element_by_id("term-brandBaseUrl").send_keys(
            "https://handbook2-stage.uttc-usa.com/manufacturer/brand/newcon-optik")
        driver.find_element_by_id("term-catalogUrl").send_keys(
            "https://handbook2-stage.uttc-usa.com/manufacturer/brand/brady")

        self.dropDownListCss("ul.select2-selection__rendered", "//ul[@id='select2-term-categories-uri-results']/li[2]")
        self.dropDownListCss(".field-term-workingTimeFrom i.glyphicon.glyphicon-time", "//td/a")
        self.dropDownListCss(".field-term-workingTimeTill i.glyphicon.glyphicon-time", "//form/div[2]")

        driver.find_element_by_id("term-distributorUrl").send_keys("http://google.com")
        driver.find_element_by_id("term-cageNumber").send_keys("11111")
        driver.find_element_by_name("Pricing[withFreightForwardInProfit]").click()

        driver.find_element_by_id("pricing-startDate").send_keys("2018-04-18")
        driver.find_element_by_id("select2-pricing-requestFrequency-container").click()
        driver.find_element_by_xpath("//ul[@id='select2-pricing-requestFrequency-results']/li").click()
        driver.find_element_by_id("pricing-requestDateManually").send_keys("2019-12-01")
        driver.find_element_by_id("pricing-warningPeriod").send_keys("5")
        driver.find_element_by_id("accounting-canDownloadInvoices").click()
        driver.find_element_by_id("accounting-paymentDiscountTariff").send_keys("1%, 20:2%")

        driver.find_element_by_id("select2-term-mpoFormat-container").click()
        driver.find_element_by_xpath("//ul[@id='select2-term-mpoFormat-results']/li[@aria-selected='false']").click()

        self.dropDownList(
            "select2-payment-method-container",
            "//ul[@id='select2-payment-method-results']/li[@aria-selected='false']"
        )

        self.dropDownList(
            "select2-keep-credit-card-container",
            "//ul[@id='select2-keep-credit-card-results']/li[@aria-selected='false']"
                          )

        self.dropDownList(
            "select2-accounting-creditCardAttachment-container",
            "//ul[@id='select2-accounting-creditCardAttachment-results']/li[@aria-selected='false']"
        )

        driver.find_element_by_id("domesticRule-isEnabled").click()
        driver.find_element_by_id("exportRule-isEnabled").click()

        driver.find_element_by_id("select2-accounting-contact-uri-container").click()
        driver.find_element_by_xpath("//span[2]/ul/li").click()

        self.dropDownList(
            "select2-accounting-paymentTerm-container",
            "//ul[@id='select2-accounting-paymentTerm-results']/li[@aria-selected='false']"
        )

        self.dropDownListCss(
            "#select2-shipping-viaAccountee-container > span.select2-selection__placeholder",
            "//ul[@id='select2-shipping-viaAccountee-results']/li"
        )

        self.dropDownList(
            "select2-shipping-exportToCanadaMexico-container",
            "//ul[@id='select2-shipping-exportToCanadaMexico-results']/li[@aria-selected='false']"
        )

        self.dropDownList(
            "select2-shipping-exportWorldwide-container",
            "//ul[@id='select2-shipping-exportWorldwide-results']/li[@aria-selected='false']"
        )

        driver.find_element_by_id("shipping-freightPrepaidAmount").send_keys("123123")
        driver.find_element_by_id("shipping-shipperPostcode").send_keys("123123")

        driver.find_element_by_xpath("//div[2]/span/span/span/ul/li/input").click()
        time.sleep(2)
        driver.find_element_by_xpath("//ul[@class='select2-results__options']/li[2]").click()
        driver.find_element_by_id("select2-term-supportContact-uri-container").click()
        time.sleep(0.5)
        driver.find_element_by_xpath("//span[2]/ul/li").click()
        driver.find_element_by_css_selector(
            "#select2-term-poContact-uri-container > span.select2-selection__placeholder").click()
        time.sleep(0.5)
        driver.find_element_by_xpath("//span[2]/ul/li").click()
        driver.find_element_by_id("select2-term-techContact-uri-container").click()
        time.sleep(0.5)
        driver.find_element_by_xpath("//span[2]/ul/li").click()
        driver.find_element_by_css_selector(
            "#select2-term-rmaContact-uri-container > span.select2-selection__placeholder").click()
        time.sleep(0.5)
        driver.find_element_by_xpath("//span[2]/ul/li").click()
        self.upload("term-attachments-upload")
        self.dropDownList("select2-term-isProblemBrand-container",
                          "//ul[@id='select2-term-isProblemBrand-results']/li[2]")
        driver.find_element_by_id("term-problemBrandAnalysisDate").clear()
        driver.find_element_by_id("term-problemBrandAnalysisDate").send_keys("2020-10-15")

        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("term-name").clear()
        driver.find_element_by_id("term-name").send_keys("Test 1")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()
        driver.get(base_url + "/manufacturer/terms/check")
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
