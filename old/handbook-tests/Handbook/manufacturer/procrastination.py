# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Procrastination(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_procrastination(self):
        print("")
        print("Procrastination")
        driver = self.driver
        driver.get(base_url + "/manufacturer/procrastinations")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Procrastination").click()
        self.dropDownList("select2-procrastination-statusCode-container",
                          "//ul[@id='select2-procrastination-statusCode-results']/li")
        driver.find_element_by_id("procrastination-warningPeriod").send_keys("1")
        driver.find_element_by_id("procrastination-alertPeriod").send_keys("2")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("procrastination-warningPeriod").clear()
        driver.find_element_by_id("procrastination-warningPeriod").send_keys("3")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
