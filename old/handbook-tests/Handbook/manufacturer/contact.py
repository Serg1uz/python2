# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Contact(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_contact(self):
        print("")
        print("Contact")
        driver = self.driver
        driver.get(base_url + "/manufacturer/brand/newcon-optik/contact")
        driver.find_element_by_id("contact-fullName").send_keys("Test")
        # Add rating
        driver.find_element_by_xpath("//label[@for='Contact[rating]-5']").click()
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("contact-fullName").clear()
        driver.find_element_by_id("contact-fullName").send_keys("Test 1")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def test_contact_all(self):
        print("")
        print("Contact all")
        driver = self.driver
        driver.get(base_url + "/manufacturer/brand/newcon-optik/contact")

        inp = ["contact-fullName", "contact-department", "contact-position", "contact-address"]
        for item in inp:
            driver.find_element_by_id("%s" % (item)).send_keys("Test")

        inp = ["contact-mainPhone", "contact-directPhone", "contact-mobilePhone", "contact-fax"]
        i = 1
        for item in inp:
            driver.find_element_by_id("%s" % (item)).send_keys("11111111%s" % (i))
            i = i + 1

        driver.find_element_by_id("contact-email").send_keys("test@test.com")

        self.cke_editor("cke_1_contents")

        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("contact-fullName").clear()
        driver.find_element_by_id("contact-fullName").send_keys("Test 1")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
