# -*- coding: utf-8 -*-
# Одноразовый скрипт для апрува всех часов за месяц
# в ссылке указываем нужный период, а также переход под пользователя у котороге есть права видеть все записи
import unittest

from selenium import webdriver
from login import LoginPage
from base import base_url, PageCUD


class ApproveEmployeeLog(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_approve_employee_log(self):
        print("")
        print("Approve Employee Log")
        link = "/corporation/working/employees/logs?dateFrom=2018-06-01&dateTo=2018-06-30&sort=status%2C-date"
        driver = self.driver
        # driver.get(base_url + "/auth/substitute/emaliuchenkova")
        driver.get(base_url + link)
        while True:
            try:
                driver.find_element_by_xpath("//tbody/tr[1]/td[@class='status' and contains(text(), 'open')]")
            except:
                print ("Done")
                break
            driver.find_element_by_xpath("//tbody/tr[1]/td[@class='date']/a").click()
            driver.find_element_by_link_text("Revise").click()
            driver.find_element_by_id("select2-employeeLog-status-container").click()
            driver.find_element_by_xpath("//ul[@id='select2-employeeLog-status-results']/li[2]").click()
            self.clickSaveButton()
            driver.get(base_url + link)

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
