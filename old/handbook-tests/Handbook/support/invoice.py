# -*- coding: utf-8 -*-
import unittest
import sys

from random import randint
from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Invoice (unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_invoice(self):
        print ("")
        print ("Invoice")
        driver = self.driver
        driver.get(base_url + "/support/report/2018-03-01")
        driver.find_element_by_xpath("(//div[@class='panel-heading'])[2]/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Invoice").click()
        driver.find_element_by_id("select2-invoice-contract-id-container").click()
        driver.find_element_by_xpath("//input[@class='select2-search__field']").send_keys("011414")
        driver.find_element_by_xpath("//span[@class='select2-results']/ul/li").click()
        driver.find_element_by_id("invoice-number").send_keys(randint(100, 9999))
        self.cke_editor("cke_1_contents")
        self.cke_editor("cke_2_contents")
        self.clickSaveButton()
        driver.find_element_by_link_text("Refresh").click()
        driver.find_element_by_css_selector("button.refresh").click()
        driver.find_element_by_link_text("Print").click()
        driver.back()
        self.clickUpdateButton()
        driver.find_element_by_id("invoice-number").send_keys("1")
        driver.find_element_by_id("invoice-date").click()
        driver.find_element_by_xpath("//td[@class='day'][5]").click()
        driver.find_element_by_id("invoice-contractTitle").send_keys("test ")
        driver.find_element_by_id("invoice-contractTitle2").send_keys("test2 ")
        driver.find_element_by_id("invoice-contractorName").send_keys("test ")
        driver.find_element_by_id("invoice-contractorName2").send_keys("test2 ")
        driver.find_element_by_id("invoice-customerName").send_keys("test ")
        self.cke_editor("cke_invoice-contractRequisite")
        self.cke_editor("cke_invoice-contractRequisite2")
        self.cke_editor("cke_invoice-customerAddress")
        self.cke_editor("cke_invoice-comment")
        self.cke_editor("cke_invoice-realWork")
        self.upload("invoice-attachments-upload")
        driver.find_element_by_xpath("//div[@id='w1-container']/table/tbody/tr/td/div/input").send_keys("test ")
        driver.find_element_by_xpath("//div[@id='w1-container']/table/tbody/tr/td[2]/div/input").send_keys("test ")
        self.cke_editor("cke_6_contents")
        self.cke_editor("cke_7_contents")
        driver.find_element_by_xpath("//div[@id='w1-container']/table/tbody/tr/td[5]/div/input").send_keys("1")
        self.clickSaveButton()
        driver.find_element_by_link_text("Issue").click()
        driver.find_element_by_css_selector("button.issue").click()
        self.clickDeleteButton()
        self.clickCancelButton()
        driver.find_element_by_link_text("Cancel").click()
        driver.find_element_by_css_selector("button.reverse").click()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
