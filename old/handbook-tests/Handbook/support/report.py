# -*- coding: utf-8 -*-
import unittest
import sys
from selenium import webdriver
sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Report(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_report(self):
        print ("")
        print ("Report")
        driver = self.driver
        driver.get(base_url + "/support/reports")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Report").click()
        driver.find_element_by_id("report-month").send_keys("2020-10")
        self.cke_editor("cke_1_contents")
        self.clickSaveButton()
        driver.find_element_by_link_text("Refresh").click()
        driver.find_element_by_css_selector("button.refresh").click()
        self.clickUpdateButton()
        self.cke_editor_new("cke_1_contents")
        driver.find_element_by_xpath("//input[@type='text']").clear()
        driver.find_element_by_xpath("//input[@type='text']").send_keys("test")
        self.clickSaveButton()
        driver.find_element_by_xpath("//input[@type='text']").clear()
        driver.find_element_by_xpath("//input[@type='text']").send_keys("100")
        driver.find_element_by_xpath("//td[@class='kv-align-top qty']/div/input[1]").send_keys("test")
        self.clickSaveButton()
        driver.find_element_by_xpath("//td[@class='kv-align-top qty']/div/input[1]").clear()
        driver.find_element_by_xpath("//td[@class='kv-align-top qty selected']/div/input[1]").send_keys("5")
        driver.find_element_by_xpath("//td[@class='kv-align-top unit']/div/input[1]").clear()
        driver.find_element_by_xpath("//td[@class='kv-align-top unit selected']/div/input[1]").send_keys("pack")
        self.clickSaveButton()
        driver.find_element_by_link_text("Close").click()
        self.clickCancelButton()
        self.clickDeleteButton()
        driver.find_element_by_xpath("//th[2]").click()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
