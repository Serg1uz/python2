# -*- coding: utf-8 -*-
import sys
import time
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Contractor(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_contractor(self):
        print("")
        print("Contractor")
        driver = self.driver
        driver.get(base_url + "/corporation/employee/vmisaev")
        driver.find_element_by_link_text("create").click()
        self.dropDownList("select2-contractor-workgroup-uri-container",
                          "//ul[@id='select2-contractor-workgroup-uri-results']/li")
        driver.find_element_by_id("contractor-name2").send_keys("Autotest")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("contractor-name2").clear()
        driver.find_element_by_id("contractor-name2").send_keys("Autotest 2")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def test_contractor_all(self):
        print("")
        print("Contractor all")
        driver = self.driver

        driver.get(base_url + "/corporation/employee/vmisaev")
        driver.find_element_by_link_text("create").click()
        self.dropDownList("select2-contractor-workgroup-uri-container",
                          "//ul[@id='select2-contractor-workgroup-uri-results']/li")
        driver.find_element_by_id("contractor-name2").send_keys("Autotest")
        time.sleep(1)
        self.dropDownListCss("input.select2-search__field", "//ul[@id='select2-worklist-id-results']/li")
        time.sleep(1)
        cke = ["cke_contractor-ncea", "cke_contractor-legend"]
        for item in cke:
            self.cke_editor(item)

        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("contractor-name2").clear()
        driver.find_element_by_id("contractor-name2").send_keys("Autotest 2")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
