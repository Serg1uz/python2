# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver
from time import sleep

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Workgroup(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_workgroup(self):
        print("")
        print("Workgroup")
        driver = self.driver
        driver.get(base_url + "/support/workgroups")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Workgroup").click()
        driver.find_element_by_id("workgroup-name").send_keys("Workg")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("workgroup-name").clear()
        driver.find_element_by_id("workgroup-name").send_keys("Workg 12")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def test_workgroup_all(self):
        print("")
        print("Workgroup all")
        driver = self.driver
        driver.get(base_url + "/support/workgroups")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Workgroup").click()
        driver.find_element_by_id("workgroup-name").send_keys("Workg")
        self.dropDownListCss("ul.select2-selection__rendered",
                             "//ul[@id='select2-workgroup-customers-uri-results']/li[2]")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("workgroup-name").clear()
        driver.find_element_by_id("workgroup-name").send_keys("Workg 12")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def test_workgroup_delete_worklist(self):
        print("")
        print("Workgroup delete worklist")
        driver = self.driver
        driver.get(base_url + "/support/workgroups")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Workgroup").click()
        driver.find_element_by_id("workgroup-name").send_keys("Test")
        self.clickSaveButton()
        driver.get(base_url + "/support/worklists")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Worklist").click()
        driver.find_element_by_id("worklist-name").send_keys("Test")
        driver.find_element_by_id("select2-worklist-workgroup-uri-container").click()
        driver.find_element_by_css_selector("input.select2-search__field").send_keys("T")
        sleep(2)
        driver.find_element_by_xpath("//ul[@class='select2-results__options']/li[contains(text(),'Test')]").click()
        self.clickSaveButton()
        driver.find_element_by_xpath("(//a[contains(text(),'Test')])").click()
        self.clickDeleteButton()
        self.clickCancelButton()
        driver.find_element_by_xpath("(//a[contains(text(),'Test')])").click()
        self.clickDeleteButton()
        driver.get(base_url + "/support/workgroups")
        driver.find_element_by_css_selector("tr.Test > td.name > a").click()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def test_workgroup_delete_contractor(self):
        print("")
        print("Workgroup delete contractor")
        driver = self.driver
        driver.get(base_url + "/support/workgroups")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Workgroup").click()
        driver.find_element_by_id("workgroup-name").send_keys("Test")
        self.clickSaveButton()
        driver.get(base_url + "/corporation/employee/vmisaev")
        driver.find_element_by_link_text("create").click()
        driver.find_element_by_id("select2-contractor-workgroup-uri-container").click()
        driver.find_element_by_css_selector(
            "span.select2-search.select2-search--dropdown > input.select2-search__field").send_keys("Test")
        driver.find_element_by_xpath("//span[2]/ul/li").click()
        driver.find_element_by_id("contractor-name2").send_keys("Test")
        self.clickSaveButton()
        driver.find_element_by_xpath("(//a[contains(text(),'Test')])").click()
        self.clickDeleteButton()
        self.clickCancelButton()
        driver.find_element_by_link_text("Владислав Мисаев").click()
        self.clickDeleteButton()
        driver.get(base_url + "/support/workgroups")
        driver.find_element_by_css_selector("tr.Test > td.name > a").click()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
