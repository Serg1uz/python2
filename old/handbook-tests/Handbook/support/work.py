# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Work(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_work(self):
        print("")
        print("Work")
        driver = self.driver
        driver.get(base_url + "/support/works")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Work").click()
        self.dropDownList("select2-work-status-container", "//ul[@id='select2-work-status-results']/li[2]")
        self.dropDownList("select2-work-worklist-uri-container", "//ul[@id='select2-work-worklist-uri-results']/li[2]")
        driver.find_element_by_id("work-name").send_keys("Test")
        driver.find_element_by_id("work-name2").send_keys("Test")
        driver.find_element_by_id("work-price").send_keys("12")
        driver.find_element_by_id("work-unit").send_keys("12")
        driver.find_element_by_id("work-balance").send_keys("12")
        self.dropDownListCss("span.select2-selection__placeholder", "//ul[@id='select2-work-balanceRule-results']/li")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("work-name").clear()
        driver.find_element_by_id("work-name").send_keys("Test 12")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def test_work_all(self):
        print("")
        print("Work all")
        driver = self.driver
        driver.get(base_url + "/support/works")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Work").click()
        self.dropDownList("select2-work-status-container", "//ul[@id='select2-work-status-results']/li[2]")
        self.dropDownList("select2-work-worklist-uri-container", "//ul[@id='select2-work-worklist-uri-results']/li[2]")
        driver.find_element_by_id("work-name").send_keys("Test")
        driver.find_element_by_id("work-name2").send_keys("Test")
        driver.find_element_by_id("work-price").send_keys("12")
        driver.find_element_by_id("work-unit").send_keys("12")
        driver.find_element_by_id("work-balance").send_keys("12")
        self.dropDownListCss("span.select2-selection__placeholder", "//ul[@id='select2-work-balanceRule-results']/li")
        self.cke_editor("cke_work-description")
        self.cke_editor("cke_work-description2")
        driver.find_element_by_id("work-agent").send_keys("Test")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("work-name").clear()
        driver.find_element_by_id("work-name").send_keys("Test 12")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
