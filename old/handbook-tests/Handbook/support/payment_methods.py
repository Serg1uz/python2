# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class PaymentMethod(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_payment_method(self):
        print ("")
        print ("Payment Methods")
        driver = self.driver
        driver.get(base_url + "/support/payment-methods")
        driver.find_element_by_link_text("Create Payment Method").click()
        driver.find_element_by_id("paymentMethod-requisite").send_keys("Autotest")
        driver.find_element_by_id("paymentMethod-requisite2").send_keys("Autotest 2")
        self.dropDownList(
            "select2-paymentMethod-customer-uri-container",
            "//ul[@id='select2-paymentMethod-customer-uri-results']/li[@aria-selected='false']"
        )
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("paymentMethod-requisite").send_keys(" New")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
