# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class CatalogBrand(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_catalog_brand(self):
        print ("")
        print ("Catalog Brands")
        driver = self.driver
        driver.get(base_url + "/manufacturer/brand/autotest")

        driver.find_element_by_xpath("//a[@href='/catalog/brand/autotest/create']").click()
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_xpath("//div[@class='form-group right field-brand-markets-uri']/span/span/span/ul").click()
        driver.find_element_by_xpath("//ul[@class='select2-results__options']/li[2]").click()
        self.clickUpdateButton()
        self.upload("brand-logoUpload")
        driver.find_element_by_id("brand-isEnabled").click()
        driver.find_element_by_id("brand-isFeatured").click()
        driver.find_element_by_id("brand-url").send_keys("https://www.google.com.ua")
        driver.find_element_by_id("brand-mapPolicy").send_keys("MAP Test")
        driver.find_element_by_xpath("//div[@class='form-group left field-brand-sellerStatus']/span/span/span").click()
        driver.find_element_by_xpath("//ul[@class='select2-results__options']/li[2]").click()
        self.cke_editor("cke_1_contents")
        driver.find_element_by_id("brand-hasDisclosablePrice").click()
        driver.find_element_by_id("brand-hasVisibleListPrice").click()
        driver.find_element_by_id("brand-hasVisibleManufacturerPrice").click()
        driver.find_element_by_id("brand-minOrderAmount").clear()
        driver.find_element_by_id("brand-minOrderAmount").send_keys("150.25")
        driver.find_element_by_id("brand-lowOrderFee").send_keys("2")
        driver.find_element_by_id("brand-productsCount").send_keys("test")
        driver.find_element_by_xpath("//div[@class='form-group left field-brand-shippingServices has-success']/span/span/span/ul").click()
        driver.find_element_by_xpath("//ul[@class='select2-results__options']/li[2]").click()
        self.clickSaveButton()
        driver.find_element_by_id("brand-productsCount").clear()
        driver.find_element_by_id("brand-productsCount").send_keys("150")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
