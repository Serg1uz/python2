# -*- coding: utf-8 -*-
import sys
import unittest
import random

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from  login import LoginPage


class Product(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_product(self):
        print ("")
        print ("Product")
        driver = self.driver
        driver.get(base_url + "/catalog/brand/for-autotest")
        driver.find_element_by_link_text("Products").click()
        driver.find_element_by_link_text("Create Product").click()
        driver.find_element_by_id("product-name").send_keys("Autotest")
        driver.find_element_by_id("select2-product-stockStatus-container").click()
        driver.find_element_by_xpath("//ul[@class='select2-results__options']/li").click()
        driver.find_element_by_id("product-partNumber").send_keys("Autotest" + str(random.randint(10000, 99999)))
        driver.find_element_by_id("product-packageQty").send_keys("2")
        driver.find_element_by_xpath("id('cke_3_contents')/div[1]/p[1]").click()
        driver.find_element_by_xpath("id('cke_3_contents')/div[1]/p[1]").send_keys("Autotest")
        driver.find_element_by_id("product-manufacturerListPrice").send_keys("150")
        driver.find_element_by_id("product-manufacturerSalePrice").send_keys("500")
        self.clickSaveButton()
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("product-name").clear()
        driver.find_element_by_id("product-name").send_keys("New Autotest")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def test_product_all(self):
        print ("")
        print ("Product All")
        driver = self.driver
        driver.get(base_url + "/catalog/brand/for-autotest")
        driver.find_element_by_link_text("Products").click()
        driver.find_element_by_link_text("Create Product").click()
        driver.find_element_by_id("product-name").send_keys("Autotest")
        driver.find_element_by_id("select2-product-stockStatus-container").click()
        driver.find_element_by_xpath("//ul[@class='select2-results__options']/li").click()
        driver.find_element_by_id("product-watermark").send_keys("Autotest")
        driver.find_element_by_id("product-isEnabled").click()
        driver.find_element_by_xpath("//div[@class='form-group left field-product-categories-uri']/span/span/span/ul").click()
        driver.find_element_by_xpath("//ul[@class='select2-results__options']/li").click()
        driver.find_element_by_id("product-partNumber").send_keys("Autotest" + str(random.randint(10000, 99999)))
        driver.find_element_by_id("product-manufacturerShippingIsFree").click()
        driver.find_element_by_id("product-manufacturerPackageQty").send_keys("5")
        # TODO добавить выпадающие списки main, variants... когда будут работать
        driver.find_element_by_id("product-modelNumber").send_keys("autotest")
        driver.find_element_by_id("product-upc").send_keys("34234234")
        driver.find_element_by_id("product-packageQty").send_keys("2")
        driver.find_element_by_id("product-minOrderQty").send_keys("255")
        self.cke_editor("cke_1_contents")
        self.cke_editor("cke_2_contents")
        driver.find_element_by_xpath("id('cke_3_contents')/div[1]/p[1]").click()
        driver.find_element_by_xpath("id('cke_3_contents')/div[1]/p[1]").send_keys("Autotest")
        driver.find_element_by_xpath("id('cke_4_contents')/div[1]/p[1]").click()
        driver.find_element_by_xpath("id('cke_4_contents')/div[1]/p[1]").send_keys("Autotest")
        driver.find_element_by_id("product-manufacturerListPrice").send_keys("150")
        driver.find_element_by_id("product-manufacturerSalePrice").send_keys("500")
        driver.find_element_by_id("product-minAdvertisedPrice").send_keys("100")
        self.upload("product-imageUpload")
        self.clickSaveButton()
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("product-name").clear()
        driver.find_element_by_id("product-name").send_keys("New Autotest")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
