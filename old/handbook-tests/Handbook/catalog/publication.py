# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Publication(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_publication(self):
        print ("")
        print ("Publication")
        driver = self.driver
        driver.get(base_url + "/catalog/brand/for-autotest")
        driver.find_element_by_xpath("//section[@id='publications']/p/a").click()
        driver.find_element_by_id("select2-brandPublication-showcase-uri-container").click()
        driver.find_element_by_xpath("//ul[@class='select2-results__options']/li").click()
        driver.find_element_by_id("brandPublication-name").send_keys("Autotest")
        self.clickSaveButton()
        driver.find_element_by_xpath("//td[@class='action']/a[@class='update']").click()
        driver.find_element_by_id("brandPublication-name").clear()
        driver.find_element_by_id("brandPublication-name").send_keys("New Autotest")
        self.clickSaveButton()
        driver.find_element_by_xpath("//td[@class='action']/a[@class='delete']").click()
        driver.find_element_by_css_selector("button.delete").click()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
