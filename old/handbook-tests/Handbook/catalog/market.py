# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Market(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_market(self):
        print ("")
        print ("Market")
        driver = self.driver
        driver.get(base_url + "/catalog/markets")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Market").click()
        driver.find_element_by_id("market-name").send_keys("Autotest")
        driver.find_element_by_xpath("//div[@class='form-group left field-market-brands-uri']/span/span/span/ul").click()
        driver.find_element_by_xpath("//ul[@class='select2-results__options']/li[2]").click()
        self.cke_editor("cke_1_contents")
        self.upload("market-imageUpload")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("market-name").clear()
        driver.find_element_by_id("market-name").send_keys("New Autotest")
        self.cke_editor_new("cke_1_contents")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccesButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
