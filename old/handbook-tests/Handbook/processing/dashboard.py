# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Dashboard(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_dashboard(self):
        print("")
        print("Dashboard")
        driver = self.driver
        driver.get(base_url + "/processing")
        driver.find_element_by_xpath("//th[2]/a").click()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
