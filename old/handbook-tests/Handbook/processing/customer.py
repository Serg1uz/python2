# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Customer(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_customer(self):
        print("")
        print("Customer")
        driver = self.driver
        driver.get(base_url + "/processing/customers")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Customer").click()
        driver.find_element_by_id("select2-customerAccount-showcaseId-container").click()
        driver.find_element_by_xpath("//ul[@id='select2-customerAccount-showcaseId-results']/li").click()
        driver.find_element_by_id("customerAccount-username").send_keys("Autotest 2r")
        text = ["customerContact-firstName", "customerContact-lastName", "customerContact-company",
                "customerContact-job"]
        for item in text:
            driver.find_element_by_id("%s" % (item)).send_keys("Test")

        driver.find_element_by_id("customerContact-phone").send_keys("1112221113")

        driver.find_element_by_id("customerContact-email").send_keys("test@test.co")
        self.dropDownList("select2-customer-source-container", "//ul[@class='select2-results__options']/li[2]")
        self.dropDownListCss("#select2-customer-type-container > span.select2-selection__placeholder",
                             "//ul[@class='select2-results__options']/li[2]")
        self.dropDownList("select2-customer-status-container", "//ul[@class='select2-results__options']/li[2]")
        self.dropDownList("select2-customer-approval-container", "//ul[@class='select2-results__options']/li[2]")
        self.dropDownList("select2-customer-destination-container", "//ul[@class='select2-results__options']/li[2]")
        self.dropDownListCss("span.select2-selection__placeholder", "//ul[@class='select2-results__options']/li[2]")
        self.dropDownList("select2-customer-timeZone-container", "//ul[@class='select2-results__options']/li[2]")
        self.cke_editor("cke_1_contents")
        self.cke_editor("cke_2_contents")
        driver.find_element_by_id("customer-balance").send_keys("12")
        self.clickSaveButton()
        self.dropDownList("select2-customer-prospectivity-container", "//ul[@class='select2-results__options']/li[2]")
        self.clickSaveButton()
        self.clickUpdateButton()
        self.dropDownList("select2-customer-status-container", "//ul[@class='select2-results__options']/li[3]")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickCancelButton()
        driver.find_element_by_xpath("//dd[14]/ul/li/a").click()
        self.clickUpdateButton()
        driver.find_element_by_id("customerAccount-username").clear()
        driver.find_element_by_id("customerAccount-username").send_keys("Autotest 87")
        self.clickSaveButton()
        driver.find_element_by_xpath("(//a[contains(text(),'Test Test')])[2]").click()
        self.clickDeleteButton()
        self.dropDownListXpath("//div[@id='breadcrumbs-pjax-container']/ul/li[4]/a", "//dd[14]/ul/li/a")
        self.clickDeleteButton()
        self.clickSuccessButton()
        driver.back()
        driver.back()
        driver.back()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def test_customer_all(self):
        print("")
        print("Customer all")
        driver = self.driver
        driver.get(base_url + "/processing/customers")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Customer").click()
        driver.find_element_by_id("select2-customerAccount-showcaseId-container").click()
        driver.find_element_by_xpath("//ul[@id='select2-customerAccount-showcaseId-results']/li").click()
        driver.find_element_by_id("customerAccount-username").send_keys("Autotest 6m")
        text = ["customerContact-firstName", "customerContact-lastName", "customerContact-company",
                "customerContact-job"]
        for item in text:
            driver.find_element_by_id("%s" % (item)).send_keys("Test")

        driver.find_element_by_id("customerContact-phone").send_keys("1112221113")
        driver.find_element_by_id("customerContact-email").send_keys("test@test.co")
        self.dropDownList("select2-customer-source-container", "//ul[@class='select2-results__options']/li[2]")
        self.dropDownListCss("#select2-customer-type-container > span.select2-selection__placeholder",
                             "//ul[@class='select2-results__options']/li[2]")
        self.dropDownList("select2-customer-status-container", "//ul[@class='select2-results__options']/li[2]")
        self.dropDownList("select2-customer-approval-container", "//ul[@id='select2-customer-approval-results']/li[2]")
        self.dropDownList("select2-customer-destination-container", "//ul[@class='select2-results__options']/li[2]")
        self.dropDownList("select2-customer-timeZone-container", "//ul[@id='select2-customer-timeZone-results']/li[2]")
        self.cke_editor("cke_1_contents")
        self.cke_editor("cke_2_contents")
        driver.find_element_by_id("customer-balance").send_keys("12")
        driver.find_element_by_id("customer-url").send_keys("http://google.com")
        driver.find_element_by_id("customer-hasSpecialRequirements").click()
        self.clickSaveButton()
        self.dropDownList("select2-customer-prospectivity-container", "//ul[@class='select2-results__options']/li[2]")
        self.dropDownList("select2-customer-timeZone-container", "//span[2]/ul/li[2]")
        self.clickSaveButton()
        self.clickUpdateButton()
        self.dropDownList("select2-customer-status-container", "//ul[@class='select2-results__options']/li[3]")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickCancelButton()
        driver.find_element_by_xpath("//dd[14]/ul/li/a").click()
        driver.find_element_by_link_text("Update").click()
        driver.find_element_by_id("customerAccount-username").send_keys("Autotest 6")
        self.clickSaveButton()
        driver.find_element_by_xpath("(//a[contains(text(),'Test Test')])[2]").click()
        self.clickDeleteButton()
        self.dropDownListXpath("//div[@id='breadcrumbs-pjax-container']/ul/li[4]/a", "//dd[14]/ul/li/a")
        self.clickDeleteButton()
        self.clickSuccessButton()
        driver.back()
        driver.back()
        driver.back()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
