# -*- coding: utf-8 -*-
from activity import *
from address import *
from blacklist import *
from contact import *
from customer import *
from dashboard import *
from shipping_provider import *
from shipping_service import *
from stock_status import *
from taxing import *


if __name__ == "__main__":
    unittest.main()
