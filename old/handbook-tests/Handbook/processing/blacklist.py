# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Blacklist(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_blacklist(self):
        print("")
        print("Blacklist")
        driver = self.driver
        driver.get(base_url + "/processing/blacklists")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Blacklist").click()
        driver.find_element_by_id("blacklist-name").send_keys("Autotest")
        driver.find_element_by_id("blacklist-pattern").send_keys("Autotest")
        self.cke_editor("cke_blacklist-comment")
        self.clickSaveButton()
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("blacklist-name").clear()
        driver.find_element_by_id("blacklist-name").send_keys("Autotest 2")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
