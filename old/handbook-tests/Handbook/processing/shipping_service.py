# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class ShippingService(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_shipping_service(self):
        print("")
        print("Shipping Service")
        driver = self.driver
        driver.get(base_url + "/processing/shipping-services")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Shipping Service").click()
        driver.find_element_by_id("shippingService-name").send_keys("Autotest")
        self.dropDownListCss("span.select2-selection__placeholder",
                             "//ul[@id='select2-shippingService-provider-uri-results']/li[2]")
        driver.find_element_by_id("shippingService-code").send_keys("12AS12DF")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("shippingService-name").clear()
        driver.find_element_by_id("shippingService-name").send_keys("Autotest 2")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
