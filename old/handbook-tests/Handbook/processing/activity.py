# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Activity(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_activity(self):
        print("")
        print("Activity")
        driver = self.driver
        driver.get(base_url + "/processing/activities")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Activity").click()
        driver.find_element_by_id("select2-activity-caller-uri-container").click()
        driver.find_element_by_css_selector("input.select2-search__field").send_keys("tes")
        driver.find_element_by_css_selector("li.select2-results__option.select2-results__option--highlighted").click()
        self.cke_editor("cke_activity-message")
        self.clickSaveButton()
        self.clickSaveButton()
        driver.find_element_by_css_selector("header").click()
        self.clickUpdateButton()
        self.cke_editor_new("cke_activity-message")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
