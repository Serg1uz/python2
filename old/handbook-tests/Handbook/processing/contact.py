# -*- coding: utf-8 -*-
import sys
import time
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Contact(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_contact(self):
        print("")
        print("Contact")
        driver = self.driver
        driver.get(base_url + "/processing/account/A-MD042417-2")
        driver.find_element_by_css_selector("dd.contacts > p.action > a.create").click()
        text = ["customerContact-firstName", "customerContact-lastName", "customerContact-company",
                "customerContact-job"]
        for item in text:
            driver.find_element_by_id("%s" % (item)).send_keys("Test")

        driver.find_element_by_id("customerContact-phone").send_keys("1112223343")
        driver.find_element_by_id("customerContact-email").send_keys("fgdfgdf@dfsd.sd")
        self.clickSaveButton()
        driver.implicitly_wait(15)
        self.clickUpdateButton()
        driver.find_element_by_id("customerContact-firstName").clear()
        driver.find_element_by_id("customerContact-firstName").send_keys("Test 2")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def test_contact_all(self):
        print("")
        print("Contact all")
        driver = self.driver
        driver.get(base_url + "/processing/account/A-MD042417-2")
        driver.find_element_by_css_selector("dd.contacts > p.action > a.create").click()
        text = ["customerContact-firstName", "customerContact-lastName", "customerContact-company",
                "customerContact-job"]
        for item in text:
            driver.find_element_by_id("%s" % (item)).send_keys("Test")

        driver.find_element_by_id("customerContact-phone").send_keys("1112223343")
        driver.find_element_by_id("customerContact-email").send_keys("fgdfgdf@dfsd.sd")
        driver.find_element_by_id("customerContact-taxpayerIdNumber").send_keys("123123123")
        driver.find_element_by_id("select2-customerContact-country-container").click()
        time.sleep(1)
        driver.find_element_by_xpath("//span[2]/ul/li").click()
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("customerContact-firstName").clear()
        driver.find_element_by_id("customerContact-firstName").send_keys("Test 2")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
