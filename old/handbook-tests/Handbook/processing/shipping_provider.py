# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class ShippingProvider(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_shipping_provider(self):
        print("")
        print("Shipping Provider")
        driver = self.driver
        driver.get(base_url + "/processing/shipping-providers")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Shipping Provider").click()
        driver.find_element_by_id("shippingProvider-name").send_keys("Autotest")
        driver.find_element_by_id("shippingProvider-url").send_keys("http://google.com")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("shippingProvider-name").clear()
        driver.find_element_by_id("shippingProvider-name").send_keys("Autotest 2")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def test_shipping_provider_with_shipping_service(self):
        print("")
        print("Shipping Provider with Shipping Service")
        driver = self.driver
        driver.get(base_url + "/processing/shipping-providers")
        driver.find_element_by_id("main").click()
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Shipping Provider").click()
        driver.find_element_by_id("shippingProvider-name").send_keys("Autotest")
        driver.find_element_by_id("shippingProvider-url").send_keys("http://google.com")
        self.clickSaveButton()
        driver.get(base_url + "/processing/shipping-services")
        driver.find_element_by_link_text("Create Shipping Service").click()
        driver.find_element_by_id("shippingService-name").send_keys("Autotest")
        driver.find_element_by_css_selector("span.select2-selection__placeholder").click()
        driver.find_element_by_css_selector("input.select2-search__field").send_keys("auto")
        driver.find_element_by_css_selector("li.select2-results__option.select2-results__option--highlighted").click()
        driver.find_element_by_id("shippingService-code").send_keys("123qwe")
        self.clickSaveButton()
        driver.find_element_by_link_text("Autotest").click()
        self.clickDeleteButton()
        self.clickCancelButton()
        driver.find_element_by_link_text("Autotest").click()
        self.clickDeleteButton()
        self.clickSuccessButton()
        driver.back()
        driver.back()
        driver.back()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
