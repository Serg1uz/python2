# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Address(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_address(self):
        print("")
        print("Address")
        driver = self.driver
        driver.get(base_url + "/processing/account/A-MD042417-2")
        driver.find_element_by_css_selector("a.create").click()
        text = ["customerAddress-firstName", "customerAddress-lastName", "customerAddress-street",
                "customerAddress-attention", "customerAddress-company", "customerAddress-city"]
        for item in text:
            driver.find_element_by_id("%s" % (item)).send_keys("Test")

        inp = ["customerAddress-postcode", "customerAddress-phone", "customerAddress-fax"]
        i = 1
        for item in inp:
            driver.find_element_by_id("%s" % (item)).send_keys("11111111%s" % (i))
            i = i + 1

        self.dropDownList("select2-customerAddress-region-code-container",
                          "//ul[@id='select2-customerAddress-region-code-results']/li[2]")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("customerAddress-firstName").clear()
        driver.find_element_by_id("customerAddress-firstName").send_keys("Test 2")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def test_address_all(self):
        print("")
        print("Address all")
        driver = self.driver
        driver.get(base_url + "/processing/account/A-MD042417-2")
        driver.find_element_by_css_selector("a.create").click()
        text = ["customerAddress-firstName", "customerAddress-lastName", "customerAddress-street",
                "customerAddress-attention", "customerAddress-company", "customerAddress-city", "customerAddress-dock"]
        for item in text:
            driver.find_element_by_id("%s" % (item)).send_keys("Test")

        self.dropDownList("customerAddress-isSignatureRequired", "//form[@id='w0']/div[6]/div/label")
        self.dropDownList("select2-customerAddress-region-code-container",
                          "//ul[@id='select2-customerAddress-region-code-results']/li")
        self.dropDownListCss("span.select2-selection.select2-selection--single", "//span[2]/ul/li")

        inp = ["customerAddress-postcode", "customerAddress-phone", "customerAddress-phone2", "customerAddress-fax"]
        i = 1
        for item in inp:
            driver.find_element_by_id("%s" % (item)).send_keys("11111111%s" % (i))
            i = i + 1

        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("customerAddress-firstName").clear()
        driver.find_element_by_id("customerAddress-firstName").send_keys("Test 2")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
