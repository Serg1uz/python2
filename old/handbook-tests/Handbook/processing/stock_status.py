# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class StockStatus(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_stock_status(self):
        print("")
        print("StockStatus")
        driver = self.driver
        driver.get(base_url + "/processing/stock-statuses")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Stock Status").click()
        driver.find_element_by_id("stockStatus-name").send_keys("Autotest")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("stockStatus-name").clear()
        driver.find_element_by_id("stockStatus-name").send_keys("Autotest 2")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
