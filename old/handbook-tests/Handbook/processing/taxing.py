# -*- coding: utf-8 -*-
import sys
import time
import unittest

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Taxing(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_taxing(self):
        print("")
        print("Taxing")
        driver = self.driver
        driver.get(base_url + "/processing/taxing")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Tax").click()
        driver.find_element_by_id("select2-tax-country-code-container").click()
        driver.find_element_by_id("select2-tax-country-code-container").send_keys("United States")
        time.sleep(1)
        driver.find_element_by_css_selector("input.select2-search__field").send_keys(Keys.ENTER)
        time.sleep(1)
        driver.find_element_by_id("select2-tax-region-code-container").click()
        driver.find_element_by_id("select2-tax-region-code-container").send_keys("APO")
        time.sleep(1)
        driver.find_element_by_css_selector("input.select2-search__field").send_keys(Keys.ENTER)
        driver.find_element_by_id("tax-rate").send_keys("0.2")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("tax-rate").clear()
        driver.find_element_by_id("tax-rate").send_keys("0.25")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def test_taxing_all(self):
        print("")
        print("Taxing all")
        driver = self.driver
        driver.get(base_url + "/processing/taxing")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Tax").click()
        driver.find_element_by_id("select2-tax-country-code-container").click()
        driver.find_element_by_id("select2-tax-country-code-container").send_keys("United States")
        time.sleep(1)
        driver.find_element_by_css_selector("input.select2-search__field").send_keys(Keys.ENTER)
        time.sleep(1)
        driver.find_element_by_id("select2-tax-region-code-container").click()
        driver.find_element_by_id("select2-tax-region-code-container").send_keys("Ohio")
        time.sleep(1)
        driver.find_element_by_css_selector("input.select2-search__field").send_keys(Keys.ENTER)
        driver.find_element_by_id("tax-rate").send_keys("0.3")
        driver.find_element_by_id("tax-district").send_keys("Tests")
        driver.find_element_by_id("tax-code").send_keys("1117779")
        driver.find_element_by_id("tax-city").send_keys("Tests")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("tax-rate").clear()
        driver.find_element_by_id("tax-rate").send_keys("0.25")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
