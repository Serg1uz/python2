import glob
import random
import string
import sys
import unittest
import datetime
import os
import requests

sys.path.append("..")

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from base import base_url, PageCUD
from login import LoginPage

class TestClass (unittest.TestCase, PageCUD):
    @classmethod
    def setUpClass(cls):

        #Добавление  профиля для Firefox, что бы не было окна выбора загрузки
        cls.profile = webdriver.FirefoxProfile()
        cls.profile.set_preference("browser.download.folderList", 2)
        cls.profile.set_preference("browser.download.manager.showWhenStarting", False)
        cls.profile.set_preference("browser.download.dir", '/home/sergeych/Downloads') # !!! Установка каталога для Загрузки
        cls.profile.set_preference("browser.helperApps.neverAsk.saveToDisk", "text/csv;application/vnd.ms-excel") # !!! Перечисление типов файлов для которых не будет выпадать окно загрузки

        cls.driver=webdriver.Firefox(log_path="../Webdriver.log", firefox_profile=cls.profile)
        cls.driver.implicitly_wait(10)
        cls.driver.maximize_window()
        cls.driver.title


        login = LoginPage(cls.driver)
        login.loginform()

        # Открытие "Advanced info panel"
        cls.driver.get(base_url + "/access/accounts")
        cls.driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()

        # ADVANCED INFO
        # Проверка открытие модального окна Hot Key

    def test_1_advanced_info_hbtn(self):
        print("")
        print("Accounts advanced info: hot key btn")
        driver = self.driver
        driver.find_element_by_xpath("//a[@class='hotkey btn']").click()
        hdt = driver.find_element_by_xpath("//*[@id='modalHeader']/h1")
        # print (hdt.text)
        assert hdt.text == "Hotkey"
        driver.find_element_by_xpath("//div[@id='modalHeader']/button[@class='close']").click()
        pass

    # Проверка работоспособности кнопки конфигурации
    def test_2_advanced_info_confbtn(self):
        print ("")
        print ("Accounts advanced info: configuration columns btn")
        driver = self.driver
        WebDriverWait(driver, 5).until_not(EC.visibility_of_element_located((By.XPATH,"//div[@class='modal-backdrop fade']")))
        driver.find_element_by_xpath(" //button[@id='personalize-grid-edit']").click()
        driver.implicitly_wait(15)
        btnapply=driver.find_element_by_xpath("//button[@id='personalize-grid-apply']")
        btnapplyD=btnapply.is_displayed()
        btnapply.click()
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        assert btnapplyD == True
        pass

    # Проверка работоспособности кнопки персонализации конфигурации
    def test_3_advanced_info_personalizebtn(self):
        print("")
        print("Accounts advanced info: personalize columns btn")
        driver = self.driver
        driver.find_element_by_xpath("//button[@data-original-title='Personalize grid settings']").click()
        driver.implicitly_wait(10)
        txt=driver.find_element_by_xpath("//label[@for ='theme-grid-access-account']")
        print(txt.text)
        driver.find_element_by_xpath("//button[@data-original-title='Save grid settings']").click()
        try:
            assert txt.text == "Grid Theme"
        except AssertionError as error:
            print (error)
        driver.implicitly_wait(15)
        pass

    #Проверка експорта в csv
    def test_4_advanced_info_expcsv(self):
        print("")
        print("Accounts advanced info: export csv btn")
        driver = self.driver
        WebDriverWait(driver, 5).until_not(
            EC.visibility_of_element_located((By.XPATH, "//div[@class='fade modal in']")))
        if not driver.find_element_by_xpath("//a[@id='mysql-export-csv']").is_displayed():
            driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_xpath("//a[@id='mysql-export-csv']").click()
        driver.implicitly_wait(120)
        downloaddir="/home/sergeych/Downloads/"
        td=datetime.date.today()
        filename="Access Account " + td.strftime("%Y-%m-%d") + " *.csv"
        ff=downloaddir+filename
        files=glob.glob(ff)
        chfile=False
        if len(files)>0: chfile=True

        if chfile:
            for f in files: os.remove(f)

        assert chfile == True
        pass

    #Проверка експорта в xls
    def test_5_advanced_info_expxls(self):
        print("")
        print("Accounts advanced info: export xls btn")
        driver = self.driver
        if not driver.find_element_by_xpath("//a[@id='mysql-export-csv']").is_displayed():
            driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_xpath("//a[@id='mysql-export-xls']").click()
        driver.implicitly_wait(120)
        downloaddir="/home/sergeych/Downloads/"
        td=datetime.date.today()
        filename="Access Account " + td.strftime("%Y-%m-%d") + " *.xls"
        ff=downloaddir+filename
        files=glob.glob(ff)
        chfile=False
        if len(files)>0: chfile=True
        print (files)
        if chfile:
            for f in files: os.remove(f)

        assert chfile == True
        pass

    # Проверка сохранение конфигурации
    def test_6_advanced_info_saveconfig(self):
        print("")
        print("Accounts advanced info: save config btn")
        driver = self.driver
        driver.find_element_by_xpath(" //button[@class='btn btn-default save-configuration']").click()
        ch=driver.find_element_by_xpath(" //input[@id='dynaGridConfigFilter-isShared']").is_displayed()
        driver.find_elements_by_xpath("//div[@class ='modal-header']/button[@ class ='close']")[1].click()
        assert ch == True
        pass
    #END ADVANCED INFO

    #SEARCH && FILTERS
    #Полное текстовый поиск
    def test_7_fulltextsearch(self):
        print("")
        print("Full text search: check status code")
        driver = self.driver
        WebDriverWait(driver, 5).until_not(
            EC.visibility_of_element_located((By.XPATH, "//div[@class='modal-backdrop fade']")))
        driver.find_element_by_xpath("//input[@id='accountSearch-query']").send_keys("test")
        driver.find_element_by_xpath("//button[@class='search']").click()
        driver.implicitly_wait(10)
        req=requests.get(driver.current_url).status_code
        print (req)
        assert (req == 200) or (req == 302)
        #Reset filters / Сброс фильтров(поиска)
        print("Reset filter")
        driver.find_element_by_xpath("//a[@class='refresh']").click()
        txt = driver.find_element_by_xpath("//input[@id='accountSearch-query']").text
        req = requests.get(driver.current_url).status_code
        assert (txt == '') and ((req == 200) or (req == 302))
        pass

    #Проверка фильтрации
    def test_8_advancedsearch(self):
        print("")
        print("Advanced search ")
        driver = self.driver
        driver.find_element_by_xpath("//a[@class='show-advanced']").click()
        #Full Names
        driver.find_element_by_xpath("//input[@class='select2-search__field']").click()
        driver.find_element_by_xpath("//ul[@id='select2-accountSearch-fullNames-results']/li[@aria-selected='false']").click()
        driver.find_elements_by_xpath("//button[@class='search']")[1].click()
        txtsearch = driver.find_element_by_xpath("//li[@class='select2-selection__choice']").text
        txtsearch = txtsearch[2:].lstrip()
        print ("search text = " + txtsearch)
        resultsearch=driver.find_element_by_xpath("//td[@class='displayName']").text
        print("result text = " + resultsearch)
        assert txtsearch == resultsearch, 'Filter is not work'

        #test export/ експорт таблицы при использовании фильтра
        print ("export with filter")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_xpath("//a[@id='mysql-export-csv']").click()
        driver.implicitly_wait(120)
        downloaddir="/home/sergeych/Downloads/"
        td=datetime.date.today()
        filename="Access Account " + td.strftime("%Y-%m-%d") + " *.csv"
        ff=downloaddir+filename
        files=glob.glob(ff)
        chfile=False
        if len(files)>0: chfile=True

        if chfile:
            for f in files: os.remove(f)

        assert chfile == True, 'Export with filter is not work'

        #Reset filters/сброс фильтров
        print("Reset filter")
        driver.find_element_by_xpath("//a[@class='refresh']").click()
        txt = driver.find_element_by_xpath("//input[@id='accountSearch-query']").text
        req = requests.get(driver.current_url).status_code
        assert (txt == '') and ((req == 200) or (req == 302))
        pass

    #Сброс фильтра / поиска
    def test_9_resetfilters(self):
        print("")
        print("Reset filter / search ")
        driver = self.driver
        driver.find_element_by_xpath("//a[@class='refresh']").click()
        txt= driver.find_element_by_xpath("//input[@id='accountSearch-query']").text
        req= requests.get(driver.current_url).status_code
        assert (txt == '') and ((req == 200) or (req == 302))
        pass
    #End SEARCH

    # Проверка функционала
    def test_a1_createaccount(self):
        print ("")
        print ("Create Account")

        # Создание аккаунта
        driver = self.driver
        driver.find_element_by_link_text("Create Account").click()
        txt=driver.find_element_by_xpath("//header/h1").text
        assert txt == 'Create Account', 'Not open page for create account'

        #проверка обязательных для заполнения полей
        driver.find_element_by_xpath("//input[@id='account-username']").click()
        driver.find_element_by_xpath("//input[@id='account-email']").click()
        driver.find_element_by_xpath("//input[@id='account-username']").click()
        ch1=driver.find_element_by_xpath("//div[contains(text(),'Username cannot be blank.')]").is_displayed()
        ch2=driver.find_element_by_xpath("//div[contains(text(),'Username cannot be blank.')]").is_displayed()
        assert (ch1 or ch2), 'Failed empty required fields'

        #Проверка валидации email
        driver.find_element_by_xpath("//input[@id='account-email']").send_keys('test')
        driver.find_element_by_xpath("//input[@id='account-username']").click()
        ch2 = driver.find_element_by_xpath("//div[contains(text(),'Email is not a valid email address.')]").is_displayed()
        assert ch2, 'Failed valid e-mail'

        #Заполнение всех полей
        td = datetime.date.today()
        driver.find_element_by_xpath("//input[@id='account-username']").send_keys('autotest'+td.strftime("%Y-%m-%d"))
        driver.find_element_by_xpath("//input[@id='account-email']").send_keys('autotest'+td.strftime("%Y-%m-%d")+'@test.com')
        driver.find_element_by_xpath("//span[@id='select2-account-corporationEmployee-uri-container']").click()
        driver.find_element_by_xpath("//input[@class='select2-search__field']").send_keys("testN")
        driver.find_element_by_xpath("//ul[@class='select2-results__options']/li[@aria-selected='false']").click()
        driver.find_element_by_xpath("//button[@class='save']").click()
        #Проверка на уникальность полей
        try:
            ch1=driver.find_element_by_xpath("//ul[@class='error-summary']").is_displayed()
            if ch1:
                rd_string = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(3))
                driver.find_element_by_xpath("//input[@id='account-username']").send_keys(rd_string)
                driver.find_element_by_xpath("//input[@id='account-email']").send_keys(rd_string)
            driver.find_element_by_xpath("//button[@class='save']").click()
        except Exception:
            ch1=False

        ch1=driver.find_element_by_xpath("//div[@id='w15-success-0']").is_displayed()
        assert ch1, 'Failed create account'

        #Редактирование аккаунта
        self.clickUpdateButton()
        driver.implicitly_wait(20)
        tmp_name=driver.find_element_by_id("account-username").text+"-updated"
        driver.find_element_by_id("account-username").clear()
        driver.find_element_by_id("account-username").send_keys(tmp_name)
        self.clickSaveButton()
        ch1 = driver.find_element_by_xpath("//div[@id='w15-success-0']").is_displayed()
        assert ch1, 'Failed update account'

        #Удаление аккаунта
        self.clickDeleteButton()
        self.clickSuccessButton()
        pass

    #Проверка остальных линков на открытие по статус коду ответа
    def test_zzz_otherlinks(self):
        print ("")
        print("Check other links")
        driver=self.driver
        driver.get(base_url + "/access/check-statuses")
        req= requests.get(driver.current_url).status_code
        assert req == 200 or req == 300, "Can't open url - " + driver.current_url

        driver.get(base_url + "/audit")
        links = ["Entries", "Mails", "Javascripts", "Errors"]
        for link in links:
            try:
                driver.find_element_by_link_text(link).click()
                driver.find_element_by_xpath("//h1[contains(text(), '" + link + "')]")
            except:
                print ("Error in " + driver.current_url)

        driver.find_element_by_link_text("Trails").click()
        req = requests.get(driver.current_url).status_code
        assert req == 200 or req == 300, "Can't open url - " + driver.current_url

        driver.find_element_by_css_selector("button.search").click()
        req = requests.get(driver.current_url).status_code
        assert req == 200 or req == 300, "Can't open url - " + driver.current_url

        driver.get(base_url + "/access")
        req = requests.get(driver.current_url).status_code
        assert req == 200 or req == 300, "Can't open url - " + driver.current_url

        driver.find_element_by_link_text("Assignment").click()
        req = requests.get(driver.current_url).status_code
        assert req == 200 or req == 300, "Can't open url - " + driver.current_url

        driver.find_element_by_link_text("Role").click()
        req = requests.get(driver.current_url).status_code
        assert req == 200 or req == 300, "Can't open url - " + driver.current_url

        driver.find_element_by_link_text("Permission").click()
        req = requests.get(driver.current_url).status_code
        assert req == 200 or req == 300, "Can't open url - " + driver.current_url

        driver.find_element_by_link_text("Route").click()
        req = requests.get(driver.current_url).status_code
        assert req == 200 or req == 300, "Can't open url - " + driver.current_url

        driver.find_element_by_link_text("Rule").click()
        req = requests.get(driver.current_url).status_code
        assert req == 200 or req == 300, "Can't open url - " + driver.current_url

        driver.find_element_by_link_text("Menus").click()
        req = requests.get(driver.current_url).status_code
        assert req == 200 or req == 300, "Can't open url - " + driver.current_url
        pass

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()


if __name__ == '__main__':
    unittest.main()
