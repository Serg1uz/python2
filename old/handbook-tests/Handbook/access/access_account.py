# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class AcessAccount(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_acess_account(self):
        print("")
        print("Accounts")
        driver = self.driver
        driver.get(base_url + "/access/accounts")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Account").click()
        driver.find_element_by_id("account-username").send_keys("autotest2")
        driver.find_element_by_id("account-email").send_keys("autotest2@test.com")
        driver.find_element_by_id("select2-account-corporationEmployee-uri-container").click()
        driver.find_element_by_xpath("//span[2]/ul/li[2]").click()
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("account-username").clear()
        driver.find_element_by_id("account-username").send_keys("autotest-2")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()
        print(".")
        print("Access")
        driver.get(base_url + "/access/check-statuses")
        driver.find_element_by_xpath("//h3[contains(text(), 'Check Statuses')]")
        driver.get(base_url + "/audit")
        links = ["Entries", "Mails", "Javascripts", "Errors"]
        for link in links:
            try:
                driver.find_element_by_link_text(link).click()
                driver.find_element_by_xpath("//h1[contains(text(), '" + link + "')]")
            except:
                print ("Error in " + driver.current_url)
        driver.find_element_by_link_text("Trails").click()
        driver.find_element_by_css_selector("button.search").click()
        driver.get(base_url + "/access")
        driver.find_element_by_link_text("Assignment").click()
        driver.find_element_by_link_text("Role").click()
        driver.find_element_by_link_text("Permission").click()
        driver.find_element_by_link_text("Route").click()
        driver.find_element_by_link_text("Rule").click()
        driver.find_element_by_link_text("Menu").click()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
