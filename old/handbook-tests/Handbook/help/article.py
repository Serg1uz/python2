# -*- coding: utf-8 -*-
import sys
import time
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Article(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_article(self):
        print("")
        print("Article")
        driver = self.driver
        driver.get(base_url + "/help/article")
        time.sleep(2)
        driver.find_element_by_id("articleRevision-title").send_keys("Autotest")
        self.dropDownList("select2-articleRevision-status-container",
                          "//ul[@id='select2-articleRevision-status-results']/li[3]")
        driver.find_element_by_xpath("//div[@id='cke_1_contents']/div").send_keys("Autotest")
        driver.find_element_by_name("ArticleRevision[isPublished]").click()
        self.clickUpdateButton()
        driver.find_element_by_id("articleRevision-title").clear()
        driver.find_element_by_id("articleRevision-title").send_keys("Autotest 2")
        driver.find_element_by_name("ArticleRevision[isPublished]").click()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def test_article_all(self):
        print("")
        print("Article all")
        driver = self.driver
        driver.get(base_url + "/help/article")
        driver.find_element_by_id("articleRevision-title").send_keys("Autotest")
        self.upload("articleRevision-iconUpload")
        self.dropDownList("select2-articleRevision-status-container",
                          "//ul[@id='select2-articleRevision-status-results']/li[3]")
        driver.find_element_by_xpath("//div[@id='cke_1_contents']/div").send_keys("Autotest")
        self.dropDownListThree("articleRevision-expirationTime", "//tr[5]/td[4]", "//span[19]")
        self.dropDownListCss("ul.select2-selection__rendered", "//ul[@id='select2-articleRevision-ranks-results']/li")
        self.dropDownListXpath("(//input[@type='search'])[2]", "//ul[@id='select2-articleRevision-units-uri-results']/li[2]")
        self.dropDownListXpath("(//input[@type='search'])[3]", "//ul[@id='select2-articleRevision-learners-uri-results']/li[2]")
        driver.find_element_by_id("articleRevision-readingPeriod").send_keys("2")
        driver.find_element_by_name("ArticleRevision[isPublished]").click()
        self.clickUpdateButton()
        driver.find_element_by_id("articleRevision-title").clear()
        driver.find_element_by_id("articleRevision-title").send_keys("Autotest 2")
        driver.find_element_by_name("ArticleRevision[isPublished]").click()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
