# -*- coding: utf-8 -*-
import sys
import time
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Courses(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_courses(self):
        print("")
        print("Courses")
        driver = self.driver
        driver.get(base_url + "/help/courses/revisions")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Course").click()
        driver.find_element_by_id("courseRevision-name").send_keys("Autotest")
        self.cke_editor("cke_courseRevision-description")
        driver.find_element_by_name("scenario").click()
        self.clickUpdateButton()
        driver.find_element_by_id("courseRevision-name").clear()
        driver.find_element_by_id("courseRevision-name").send_keys("Autotest 2")
        driver.find_element_by_name("scenario").click()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def test_courses_all(self):
        print("")
        print("Courses all")
        driver = self.driver
        driver.get(base_url + "/help/courses/revisions")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Course").click()
        driver.find_element_by_id("courseRevision-name").send_keys("Autotest")
        self.cke_editor("cke_courseRevision-description")
        self.dropDownListCss("input.select2-search__field",
                             "//ul[@id='select2-courseRevision-ranks-results']/li")
        self.dropDownListXpath("(//input[@type='search'])[2]",
                               "//ul[@id='select2-courseRevision-units-uri-results']/li[2]")
        time.sleep(2)
        driver.find_element_by_name("scenario").click()
        self.clickUpdateButton()
        driver.find_element_by_id("courseRevision-name").clear()
        driver.find_element_by_id("courseRevision-name").send_keys("Autotest 2")
        driver.find_element_by_name("scenario").click()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
