# -*- coding: utf-8 -*-
import sys
import time
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Comment(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_comment(self):
        print("")
        print("Comment")
        driver = self.driver
        driver.get(base_url + "/help/article/testers")
        driver.find_element_by_link_text("Create").click()
        time.sleep(2)
        self.cke_editor("cke_comment-body")
        self.clickSaveButton()
        driver.find_element_by_xpath("(//a[contains(text(),'Update')])[2]").click()
        self.cke_editor_new("cke_comment-body")
        self.clickSaveButton()
        driver.find_element_by_xpath("(//a[contains(text(),'Delete')])[2]").click()
        driver.find_element_by_css_selector("button.delete").click()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
