# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class ArticleLabel(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_article_label(self):
        print ("")
        print ("Article Label")
        driver = self.driver
        driver.get(base_url + "/help/articles/labels")
        driver.find_element_by_link_text("Create Article Label").click()
        driver.find_element_by_id("articleLabel-name").send_keys("Autotest")
        self.cke_editor("cke_articleLabel-description")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("articleLabel-name").send_keys(" new")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
