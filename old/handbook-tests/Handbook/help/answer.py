# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Answer(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_answer(self):
        print("")
        print("Answer")
        driver = self.driver
        driver.get(base_url + "/help/article/testers")
        driver.find_element_by_css_selector("a.test").click()
        driver.find_element_by_link_text("Create").click()
        self.cke_editor("cke_question-body")
        self.clickSaveButton()
        driver.find_element_by_link_text("Create").click()
        self.cke_editor("cke_answer-body")
        self.clickSaveButton()
        driver.find_element_by_link_text("Answers").click()
        self.clickUpdateButton()
        self.dropDownList("select2-question-answerType-container",
                          "//ul[@id='select2-question-answerType-results']/li[2]")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_xpath("//div[@id='question-answers-uri']//input[@id='question-answers-uri--0']").click()
        self.clickSaveButton()
        self.clickUpdateButton()
        self.dropDownList("select2-question-answerType-container",
                          "//ul[@id='select2-question-answerType-results']/li[3]")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
