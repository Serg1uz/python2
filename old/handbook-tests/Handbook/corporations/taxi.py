# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Taxi (unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_taxi(self):
        print ("")
        print ("Taxi")
        driver = self.driver
        driver.get(base_url + "/corporation/transportation/taxies")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Taxi").click()
        driver.find_element_by_id("taxi-name").send_keys("Autotest")
        driver.find_element_by_id("select2-taxi-mode-container").click()
        driver.find_element_by_xpath("//ul[@id='select2-taxi-mode-results']/li").click()
        self.cke_editor("cke_taxi-description")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("taxi-name").send_keys("-2")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()

if __name__ == "__main__":
    unittest.main()
