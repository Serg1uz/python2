# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Membership(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_membership(self):
        print("")
        print("Membership")
        driver = self.driver
        driver.get(base_url + "/corporation/employee/vmisaev")
        try:
            driver.find_element_by_css_selector("a.create").click()
        except:
            driver.find_element_by_link_text("create a new one").click()
        self.dropDownList("select2-employeeMembership-company-uri-container",
                          "//ul[@id='select2-employeeMembership-company-uri-results']/li[2]")
        driver.find_element_by_id("employeeMembership-nickname").send_keys("test")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("employeeMembership-nickname").clear()
        driver.find_element_by_id("employeeMembership-nickname").send_keys("test-2")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
