# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Position(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_position(self):
        print("")
        print("Position")
        driver = self.driver
        driver.get(base_url + "/corporation/positions")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Position").click()
        driver.find_element_by_id("position-title").send_keys("test")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("position-title").clear()
        driver.find_element_by_id("position-title").send_keys("test-2")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def test_position_all(self):
        print("")
        print("Position all")
        driver = self.driver
        driver.get(base_url + "/corporation/positions")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Position").click()
        driver.find_element_by_id("position-title").send_keys("test")
        self.cke_editor("cke_position-description")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("position-title").clear()
        driver.find_element_by_id("position-title").send_keys("test-2")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def test_position_delete_validator(self):
        print("")
        print("Position Delete Validator")
        driver = self.driver
        driver.get(base_url + "/corporation/position/tester")
        self.clickDeleteButton()
        self.clickCancelButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
