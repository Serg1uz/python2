# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Location(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_location(self):
        print("")
        print("Location")
        driver = self.driver
        driver.get(base_url + "/corporation/locations")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Location").click()
        driver.find_element_by_id("location-name").send_keys("test")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("location-name").clear()
        driver.find_element_by_id("location-name").send_keys("test-2")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def test_location_all(self):
        print("")
        print("Location all")
        driver = self.driver
        driver.get(base_url + "/corporation/locations")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Location").click()
        driver.find_element_by_id("location-name").send_keys("test")
        self.cke_editor("cke_location-description")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("location-name").clear()
        driver.find_element_by_id("location-name").send_keys("test-2")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
