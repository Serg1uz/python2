# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Holiday(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_holiday(self):
        print("")
        print("Holiday")
        driver = self.driver
        driver.get(base_url + "/corporation/holidays")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Holiday").click()
        driver.find_element_by_id("holiday-name").send_keys("test")
        self.dropDownList("select2-holiday-rule-container", "//span[2]/ul/li")
        driver.find_element_by_id("holiday-month").send_keys("3")
        driver.find_element_by_id("holiday-day").send_keys("23")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("holiday-month").clear()
        driver.find_element_by_id("holiday-month").send_keys("5")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def test_holiday_all(self):
        print("")
        print("Holiday all")
        driver = self.driver
        driver.get(base_url + "/corporation/holidays")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Holiday").click()
        driver.find_element_by_id("holiday-name").send_keys("test")
        driver.find_element_by_id("select2-holiday-rule-container").click()
        driver.find_element_by_xpath("//span[2]/ul/li").click()
        driver.find_element_by_id("holiday-month").send_keys("3")
        driver.find_element_by_id("holiday-day").send_keys("23")
        self.cke_editor("cke_holiday-description")
        driver.find_element_by_id("holiday-startYear").send_keys("2010")
        driver.find_element_by_id("holiday-endYear").send_keys("2020")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("holiday-month").clear()
        driver.find_element_by_id("holiday-month").send_keys("5")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
