# -*- coding: utf-8 -*-
from calendar_turn import *
from company import *
from employee import *
from holiday import *
from issue import *
from issue_label import *
from location import *
from membership import *
from outbox import *
from pages import *
from position import *
from showcase import *
from unit import *
from sanctions import *
from absence_reason import *
from shifts import *
from absences import *
from employee_taxi_ride import *
from report_transportation import *
from routine import *
from taxi import *
from transportation_employee import *
from schedule import *
from working_view import *
from timesheet import *


if __name__ == "__main__":
    unittest.main()
