# -*- coding: utf-8 -*-
import sys
import unittest
from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class EmployeeTaxiRide(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_employee_taxi_ride(self):
        # TODO check month
        print ("")
        print ("Employee Taxi Ride")
        driver = self.driver
        driver.get(base_url + "/corporation/transportation/employee/aokunev/taxi/ride")
        driver.find_element_by_id("select2-employeeTaxiRide-taxi-uri-container").click()
        driver.find_element_by_xpath("//ul[@id='select2-employeeTaxiRide-taxi-uri-results']/li[@aria-selected='false']").click()
        driver.find_element_by_id("employeeTaxiRide-amount2").send_keys("wrong")
        self.clickSaveButton()
        driver.find_element_by_id("employeeTaxiRide-amount2").clear()
        driver.find_element_by_id("employeeTaxiRide-amount2").send_keys("100.25")
        self.cke_editor("cke_employeeTaxiRide-comment")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("employeeTaxiRide-amount2").clear()
        driver.find_element_by_id("employeeTaxiRide-amount2").send_keys("56.00")
        self.cke_editor_new("cke_employeeTaxiRide-comment")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()
        driver.find_element_by_css_selector("button.search").click()

    def tearDown(self):
        self.driver.quit()
        # self.driver.title


if __name__ == "__main__":
    unittest.main()
