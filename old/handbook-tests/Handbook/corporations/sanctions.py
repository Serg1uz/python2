# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Sanctions(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_sanctions(self):
        print("")
        print("Sanctions")
        driver = self.driver
        driver.get(base_url + "/corporation/sanctions")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Sanction").click()
        driver.find_element_by_id("sanction-name").send_keys("autotest")
        driver.find_element_by_id("sanction-mark").send_keys("-0.02")
        self.cke_editor("cke_sanction-description")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("sanction-name").clear()
        driver.find_element_by_id("sanction-name").send_keys("change")
        driver.find_element_by_id("sanction-mark").clear()
        driver.find_element_by_id("sanction-mark").send_keys("1")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()


    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
