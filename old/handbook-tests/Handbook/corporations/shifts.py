# -*- coding: utf-8 -*-
import sys
import unittest

from random import randint
from selenium import webdriver
sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Shifts(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_shifts(self):
        print ("")
        print ("Working Shifts Required")
        driver = self.driver
        driver.get(base_url + "/corporation/working/shifts")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Shift").click()
        driver.find_element_by_id("shift-workingTimeFrom").send_keys(randint(3, 10))
        driver.find_element_by_id("shift-workingTimeTill").send_keys(randint(11, 23))
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("shift-workingTimeFrom").clear()
        driver.find_element_by_id("shift-workingTimeFrom").send_keys(randint(3, 10))
        driver.find_element_by_id("shift-workingTimeTill").clear()
        driver.find_element_by_id("shift-workingTimeTill").send_keys(randint(11, 23))
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def test_shifts_all(self):
        print ("")
        print ("Working Shifts All")
        driver = self.driver
        driver.get(base_url + "/corporation/working/shifts")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Shift").click()
        driver.find_element_by_id("shift-workingTimeFrom").send_keys(randint(3, 10))
        driver.find_element_by_id("shift-workingTimeTill").send_keys(randint(16, 23))
        driver.find_element_by_id("shift-lunchTimeFrom").send_keys(randint(11, 12))
        driver.find_element_by_id("shift-lunchTimeTill").send_keys(randint(13, 14))
        driver.find_element_by_id("shift-isFlexitime").click()
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("shift-workingTimeFrom").clear()
        driver.find_element_by_id("shift-workingTimeFrom").send_keys(randint(3, 10))
        driver.find_element_by_id("shift-workingTimeTill").clear()
        driver.find_element_by_id("shift-workingTimeTill").send_keys(randint(11, 23))
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
