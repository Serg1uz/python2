# -*- coding: utf-8 -*-
import sys
import time
import unittest

from selenium import webdriver
import glob
import random
import string
import sys
import unittest
import datetime
import os
import requests

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Employee(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(15)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_employee(self):
        print("")
        print("Employee")
        driver = self.driver
        driver.get(base_url + "/corporation/employees/index")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Employee").click()
        driver.find_element_by_id("employee-firstName").send_keys("test")
        driver.find_element_by_id("employee-lastName").send_keys("test")
        self.dropDownList("employee-hiringDate", "//td[4]")
        self.cke_editor("cke_employee-hiringNote")
        self.dropDownList("select2-employee-unit-uri-container", "//span[2]/ul/li[4]")
        self.dropDownListCss("#select2-employee-position-uri-container > span.select2-selection__placeholder",
                             "//span[2]/ul/li[3]")
        rd_string = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8))
        driver.find_element_by_id("employeeContact-phone").send_keys(rd_string)
        self.clickSaveButton()
        # self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("employee-firstName").clear()
        driver.find_element_by_id("employee-firstName").send_keys("test")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()
        driver.get(base_url + "/corporation/working/employees/schedules/scheme")
        driver.find_element_by_xpath("//div[contains(@class, 'worker')]")

    def test_employee_all(self):
        print("")
        print("Employee all")
        driver = self.driver
        driver.get(base_url + "/corporation/employees/index")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Employee").click()
        driver.find_element_by_id("employee-isStaffer").click()
        self.dropDownList("employee-hiringDate", "//td[4]")

        cke = ["cke_employee-hiringNote", "cke_employee-responsibility", "cke_employee-examResult",
               "cke_employee-workResult", "cke_employee-education", "cke_employee-relatives",
               "cke_employeeEmployment-experience", "cke_employeeEmployment-recommendations",
               "cke_employeeQuestionnaire-addition"]
                # "cke_employee-firingNote", Временно убрали при создании
        for item in cke:
            self.cke_editor(item)

        text = ["employee-firstName", "employee-lastName", "employee-middleName", "employee-middleName2",
                "employee-firstName2", "employee-lastName2", "employee-languages", "employeeRegistry-birthPlace",
                "employeeRegistry-names", "employeeRegistry-military", "employeeRegistry-passportNumber",
                "employeeRegistry-address", "employeeRegistry-passportIssuer", "employeeRegistry-identificationNumber",
                "employeeContact-address", "employeeQuestionnaire-detention",
                "employeeQuestionnaire-criminalInvestigation", "employeeQuestionnaire-criminalLiability",
                "employeeQuestionnaire-contraindication", "employeeQuestionnaire-medication",
                "employeeQuestionnaire-dispensary", "employeeQuestionnaire-abroad", "employeeQuestionnaire-vehicles",
                "employeeQuestionnaire-interests", "employeeQuestionnaire-expectations",
                "employeeEmployment-contractualObligation", "employee-firstName", "employeeEmployment-referrer",
                "employeeContact-skype", "employeeContact-phone", "employeeContact-phone2", "employeeContact-phone3"]
        for item in text:
            driver.find_element_by_id("%s" % (item)).send_keys("Autotest")

        # Временно убрали firing date при создании
        # self.dropDownListCss(
        #     "#employee-firingDate-kvdate > span.input-group-addon.kv-date-calendar > i.glyphicon.glyphicon-calendar",
        #     "//tr[5]/td[6]")
        self.dropDownListCss("#select2-employee-position-uri-container > span.select2-selection__placeholder",
                             "//span[2]/ul/li[3]")
        self.dropDownList("select2-employee-rank-container", "//span[2]/ul/li[3]")
        self.dropDownList("select2-employee-unit-uri-container", "//span[2]/ul/li[4]")
        driver.find_element_by_id("employeeRegistry-birthDate").click()
        time.sleep(1)
        driver.find_element_by_xpath("//tr[2]/td[7]").click()
        self.dropDownList("select2-employeeRegistry-gender-container", "//span[2]/ul/li[2]")
        self.dropDownList("select2-employeeRegistry-maritalStatus-container", "//span[2]/ul/li[2]")

        driver.find_element_by_id("employeeRegistry-children").send_keys("2")
        rd_string = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(6))
        driver.find_element_by_id("employeeContact-email").send_keys(rd_string+"@ttesttees.wwt")
        driver.find_element_by_id("employeeEmployment-certificate").send_keys("1234")
        self.clickSaveButton()
        # self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("employee-firstName").clear()
        driver.find_element_by_id("employee-firstName").send_keys("Autotestss")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def test_employee_validate(self):
        print("")
        print("Employee Validate")
        driver = self.driver
        driver.get(base_url + "/corporation/employee/vmisaev")
        self.clickDeleteButton()
        self.clickCancelButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
