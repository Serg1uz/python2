# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class AbcenceReason(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_absence_reason(self):
        print ("")
        print ("Absence Reason")
        driver = self.driver
        driver.get(base_url + "/corporation/working/absences/reasons")
        driver.find_element_by_link_text("Create Absence Reason").click()
        self.clickSaveButton()
        driver.find_element_by_id("absenceReason-name").send_keys("Autotest")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("absenceReason-name").send_keys(" New")
        self.cke_editor("cke_absenceReason-description")
        driver.find_element_by_id("absenceReason-logIsWorkingPeriodReduction").click()
        self.cke_editor("cke_absenceReason-logComment")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
