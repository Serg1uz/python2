# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class CalendarTurn(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_calendar_turn(self):
        print("")
        print("CalendarTurn")
        driver = self.driver
        driver.get(base_url + "/corporation/calendar-turns")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Calendar Turn").click()
        self.dropDownList("calendarTurn-turnDay", "//tr[2]/td[4]")
        self.dropDownList("calendarTurn-dayOff", "//tr[5]/td[6]")
        self.cke_editor("cke_calendarTurn-reason")
        self.clickSaveButton()
        self.clickUpdateButton()
        self.cke_editor_new("cke_calendarTurn-reason")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
