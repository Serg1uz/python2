# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver
from random import randint
sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Absences(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_absences(self):
        print ("")
        print ("Absences One Day")
        driver = self.driver
        driver.get(base_url + "/corporation/working/absences")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Absence Log").click()
        driver.find_element_by_id("select2-absenceLog-reason-uri-container").click()
        driver.find_element_by_xpath("//li[@aria-selected='false']").click()
        driver.find_element_by_id("absenceLog-dateFrom").click()
        driver.find_element_by_xpath("//tbody/tr[2]/td[@class='day'][4]").click()
        driver.find_element_by_id("absenceLog-dateTill").click()
        driver.find_element_by_xpath("//tbody/tr[2]/td[@class='day'][4]").click()
        driver.find_element_by_id("absenceLog-timeFrom").send_keys("10:" + str(randint(0, 59)))
        driver.find_element_by_id("absenceLog-timeTill").send_keys("12:" + str(randint(0, 59)))
        self.cke_editor("cke_absenceLog-comment")
        self.driver.find_element_by_id("absenceLog-isWorkingPeriodReduction").click()
        self.clickSaveButton()
        self.clickUpdateButton()
        self.cke_editor_new("cke_absenceLog-comment")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def test_absences_some_day(self):
        print ("")
        print ("Absences Some Day")
        driver = self.driver
        driver.get(base_url + "/corporation/working/absences")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Absence Log").click()
        driver.find_element_by_id("select2-absenceLog-reason-uri-container").click()
        driver.find_element_by_xpath("//li[@aria-selected='false']").click()
        driver.find_element_by_id("absenceLog-dateFrom").click()
        driver.find_element_by_xpath("//tbody/tr[2]/td[@class='day'][4]").click()
        driver.find_element_by_id("absenceLog-dateTill").click()
        driver.find_element_by_xpath("//tbody/tr[3]/td[@class='day'][4]").click()
        driver.find_element_by_id("absenceLog-timeFrom").send_keys("10:" + str(randint(0, 59)))
        driver.find_element_by_id("absenceLog-timeTill").send_keys("12:" + str(randint(0, 59)))
        self.cke_editor("cke_absenceLog-comment")
        self.driver.find_element_by_id("absenceLog-isWorkingPeriodReduction").click()
        self.clickSaveButton()
        self.clickUpdateButton()
        self.cke_editor_new("cke_absenceLog-comment")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def test_absences_period(self):
        print ("")
        print ("Absences Period")
        driver = self.driver
        driver.get(base_url + "/corporation/working/absences")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Absence Log").click()
        driver.find_element_by_id("select2-absenceLog-reason-uri-container").click()
        driver.find_element_by_xpath("//li[@aria-selected='false']").click()
        driver.find_element_by_id("absenceLog-dateFrom").click()
        driver.find_element_by_xpath("//tbody/tr[2]/td[@class='day'][4]").click()
        driver.find_element_by_id("absenceLog-period").send_keys("04:00")
        self.cke_editor("cke_absenceLog-comment")
        self.driver.find_element_by_id("absenceLog-isWorkingPeriodReduction").click()
        self.clickSaveButton()
        driver.implicitly_wait(10)
        self.clickUpdateButton()
        self.cke_editor_new("cke_absenceLog-comment")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
