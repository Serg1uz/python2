# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver
from time import sleep

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Timesheet(unittest.TestCase, PageCUD):

    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(15)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_timesheet(self):
        print("")
        print("Timesheet")
        driver = self.driver
        self.create_timesheet(driver, base_url)
        timesheet_url = "/corporation/working/timesheet/2019-11-01"
        self.create_issue(driver, base_url)
        issue_url = driver.current_url
        self.create_issue_log(driver)
        issue_log = driver.current_url
        self.revise_employee_log(driver)
        employee_log = driver.current_url
        self.revise_timesheet(driver, timesheet_url, base_url)
        self.open_timesheet(driver)
        self.open_employee_log(driver, employee_log)
        self.delete_issue_log(driver, issue_log)
        self.delete_timesheet(driver, base_url, timesheet_url)
        self.delete_issue(driver, issue_url)

    def create_timesheet(self, driver, base_url):
        print ("Create Timesheet")
        driver.get(base_url + "/corporation/working/timesheets")
        self.clickShowButton()
        driver.find_element_by_link_text("Create Timesheet").click()
        driver.find_element_by_id("timesheet-month").clear()
        driver.find_element_by_id("timesheet-month").send_keys("2019-11")
        self.cke_editor("cke_timesheet-comment")
        self.clickSaveButton()

    def create_issue(self, driver, base_url):
        print ("Create Issue")
        driver.get(base_url + "/corporation/working/issues")
        driver.find_element_by_css_selector(".quick-create").click()
        self.cke_editor("cke_issue-description")
        sleep(1)
        driver.find_element_by_css_selector("p.action > button.save").click()
        try:
            # иногда валидация не проходит и нужно еще раз нажать save
            sleep(1)
            driver.find_element_by_css_selector("p.action > button.save").click()
        except:
            print ("")
        sleep(5)
        self.clickSuccessButton()
        driver.find_element_by_link_text("Autotest").click()

    def create_issue_log(self, driver):
        print ("Create Issue Log")
        driver.find_element_by_link_text("Log Work").click()
        driver.find_element_by_id("employeeIssueLog-startTime").clear()
        driver.find_element_by_id("employeeIssueLog-startTime").send_keys("2019-11-11 14:00:00")
        driver.find_element_by_id("employeeIssueLog-workedPeriod").clear()
        driver.find_element_by_id("employeeIssueLog-workedPeriod").send_keys("03:21")
        driver.find_element_by_id("employeeIssueLog-qty").send_keys("453")
        self.cke_editor("cke_employeeIssueLog-comment")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("employeeIssueLog-workedPeriod").clear()
        driver.find_element_by_id("employeeIssueLog-workedPeriod").send_keys("04:23")
        self.clickSaveButton()
        self.clickSuccessButton()

    def revise_employee_log(self, driver):
        print ("Revise Employee Log")
        driver.find_element_by_xpath("//dd[@class='employee-log']/a").click()
        driver.find_element_by_link_text("Revise").click()
        self.dropDownList("select2-employeeLog-status-container",
                          "//ul[@id='select2-employeeLog-status-results']/li[2]")
        self.cke_editor("cke_employeeLog-comment")
        self.clickSaveButton()
        self.clickSuccessButton()

    def revise_timesheet(self, driver, timesheet_url, base_url):
        print ("Revise Timesheet")
        driver.get(base_url + timesheet_url)
        driver.find_element_by_link_text("Revise").click()
        driver.find_element_by_xpath("//input[@class='checkbox-select-all']").click()
        self.clickSaveButton()
        self.clickSuccessButton()
        self.clickCancelButton()

    def open_timesheet(self, driver):
        driver.find_element_by_link_text("Open").click()
        driver.find_element_by_xpath("//button[@class='open']").click()
        driver.find_element_by_link_text("Revise").click()
        driver.find_element_by_xpath("//input[@class='checkbox-select-all']").click()
        driver.find_element_by_xpath("//input[@class='checkbox-select-all']").click()
        self.clickSaveButton()
        self.clickSuccessButton()

    def open_employee_log(self, driver, employee_log):
        print ("Open Employee Log")
        driver.get(employee_log)
        driver.find_element_by_link_text("Revise").click()
        self.dropDownList("select2-employeeLog-status-container",
                          "//ul[@id='select2-employeeLog-status-results']/li[1]")
        self.clickSaveButton()
        self.clickSuccessButton()

    def delete_issue_log(self, driver, issue_log):
        print ("Delete Issue Log")
        driver.get(issue_log)
        self.clickDeleteButton()
        self.clickSuccessButton()

    def delete_timesheet(self, driver, base_url, timesheet_url):
        print ("Delete Timesheet")
        driver.get(base_url + timesheet_url)
        self.clickDeleteButton()
        self.clickSuccessButton()

    def delete_issue(self, driver, issue_url):
        print ("Delete Issue")
        driver.get(issue_url)
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
