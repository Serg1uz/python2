# -*- coding: utf-8 -*-
import sys
import unittest
from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class ReportTransportation(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_report_transportation(self):
        print ("")
        print ("Report (Transportation)")
        driver = self.driver
        driver.get(base_url + "/corporation/transportation/reports")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Report").click()
        driver.find_element_by_id("report-month").clear()
        driver.find_element_by_id("report-month").send_keys("2020-12")
        self.cke_editor("cke_report-comment")
        self.clickSaveButton()
        report_url = base_url + "/corporation/transportation/report/2020-12-01"

        driver.get(base_url + "/corporation/transportation/employee/aokunev/taxi/ride")
        self.dropDownList("select2-employeeTaxiRide-taxi-uri-container", "//ul[@id='select2-employeeTaxiRide-taxi-uri-results']/li[@aria-selected='false']")
        driver.find_element_by_id("employeeTaxiRide-date").clear()
        driver.find_element_by_id("employeeTaxiRide-date").send_keys("2020-12-16")
        driver.find_element_by_id("employeeTaxiRide-amount2").send_keys("100.25")
        self.clickSaveButton()
        self.clickSuccessButton()
        taxi_ride_url = driver.current_url

        driver.get(report_url)
        driver.find_element_by_link_text("Revise").click()
        driver.find_element_by_xpath("//input[@name='isClosed']").click()
        self.clickSaveButton()
        driver.find_element_by_xpath("//dd[contains(text(),'closed')]")
        self.clickCancelButton()
        driver.find_element_by_link_text("Open").click()
        driver.find_element_by_xpath("//button[contains(text(), 'Open')]").click()
        driver.find_element_by_link_text("Revise").click()
        driver.find_element_by_xpath("//input[@name='isClosed']").click()
        driver.find_element_by_xpath("//input[@name='isClosed']").click()
        self.clickSaveButton()
        self.clickSuccessButton()

        driver.get(taxi_ride_url)
        self.clickDeleteButton()
        self.clickSuccessButton()

        driver.get(report_url)
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
