# -*- coding: utf-8 -*-
import sys
import time
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Outbox(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_outbox(self):
        print("")
        print("Outbox")
        driver = self.driver
        driver.get(base_url + "/corporation/outbox")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Outbox Message").click()
        time.sleep(1)
        driver.find_element_by_id("outboxMessage-subject").send_keys("test")
        driver.find_element_by_id("outboxMessage-readingPeriod").send_keys("0")
        self.cke_editor("cke_1_contents")
        driver.find_element_by_css_selector("ul.select2-selection__rendered").click()
        time.sleep(1)
        driver.find_element_by_xpath("//body/span/span/span/ul/li").click()
        driver.find_element_by_xpath("//button[@type='submit']").click()
        driver.find_element_by_link_text("Delete").click()
        driver.find_element_by_css_selector("button.delete").click()
        self.clickSuccessButton()
        driver.get(base_url + "/corporation/inbox/messages/check")
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
