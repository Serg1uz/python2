# -*- coding: utf-8 -*-
import sys
import unittest
from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class TransportationEmployee (unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_transportation_employee(self):
        print ("")
        print ("Employee (Transpotation)")
        driver = self.driver
        driver.get(base_url + "/corporation/transportation/employee/aokunev")
        self.clickUpdateButton()
        driver.find_element_by_id("select2-employee-taxi-uri-container").click()
        driver.find_element_by_xpath("//ul[@id='select2-employee-taxi-uri-results']/li[@aria-selected='false']").click()
        self.clickSaveButton()
        self.clickSuccessButton()
        print ("")
        print ("Report Employee (Transportation)")
        driver.get(base_url + "/corporation/transportation/reports/employees")
        driver.find_element_by_xpath("(//td[@class='month'])[1]/a").click()
        driver.find_element_by_xpath("//article[@class='report-employee']")

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
