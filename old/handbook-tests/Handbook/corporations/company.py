# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Company(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_company(self):
        print("")
        print("Company")
        driver = self.driver
        driver.get(base_url + "/corporation/companies")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Company").click()
        driver.find_element_by_id("company-name").send_keys("test")
        driver.find_element_by_id("company-address").send_keys("test")
        driver.find_element_by_id("company-fax").send_keys("1234567890")
        driver.find_element_by_id("company-taxpayerIdNumber").send_keys("1212")
        driver.find_element_by_id("company-phone").send_keys("123456789")
        driver.find_element_by_id("company-url").send_keys("http://handbook2-stage.uttc-usa.com/corporation/companies")
        driver.find_element_by_id("company-email").send_keys("test@test.ru")
        self.upload("company-logoUpload")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("company-name").clear()
        driver.find_element_by_id("company-name").send_keys("test-2")
        self.clickSaveButton()
        company_uri = driver.current_url

        print("Company Delete Validator")
        driver.find_element_by_link_text("Create").click()
        driver.find_element_by_id("showcase-id").send_keys("CA")
        driver.find_element_by_id("showcase-phone").send_keys("1234567")
        driver.find_element_by_id("showcase-name").send_keys("test")
        driver.find_element_by_id("showcase-address").send_keys("test")
        driver.find_element_by_id("showcase-fax").send_keys("1234567")
        self.clickSaveButton()
        showcase_uri = driver.current_url
        driver.get(company_uri)
        self.clickDeleteButton()
        self.clickCancelButton()
        driver.get(showcase_uri)
        self.clickDeleteButton()
        self.clickSuccessButton()

        print ("Delete Company")
        driver.get(company_uri)
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
