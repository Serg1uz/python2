# -*- coding: utf-8 -*-
import os
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Unit(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_unit(self):
        print("")
        print("Unit")
        driver = self.driver
        driver.get(base_url + "/corporation/units")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Unit").click()
        driver.find_element_by_id("unit-name").send_keys("test")
        self.cke_editor("cke_unit-description")
        self.upload("unit-iconUpload")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("unit-name").clear()
        driver.find_element_by_id("unit-name").send_keys("test-2")
        driver.find_element_by_css_selector("#w0 > p.action > button.save").click()
        driver.find_element_by_link_text("Child").click()
        driver.find_element_by_id("unit-name").send_keys("test")
        self.cke_editor("cke_unit-description")
        self.clickSaveButton()
        driver.find_element_by_css_selector("a.view").click()
        self.clickDeleteButton()
        driver.find_element_by_link_text("Restore").click()
        driver.find_element_by_css_selector("button.restore").click()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
