# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Pages(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_pages(self):
        print("")
        print("Pages")
        driver = self.driver

        link = [
            "/corporation/employees/position-changes", "/corporation/inbox/messages",
            "/corporation/inbox", "/corporation/outbox/messages", "/corporation/employees/status-changes",
            "/corporation/working/attendances", "/corporation/working/attendances/events",
            "/corporation/working/attendances/events/import", "/corporation/working/absences/timesheet",
            "/corporation/transportation/employees/rides", "/corporation/transportation/employees/rides/worksheet",
            "/corporation/transportation/reports/employees", "/corporation/transportation/taxies/rides",
            "/corporation/transportation/taxies/rides/worksheet", "/corporation/transportation/reports/taxies",
            "/corporation/working/presences", "/corporation/transportation/reports", "/corporation/working/issues/logs",
            "/corporation/working/issues/logs/timesheet", "/corporation/working/timesheets/issues",
            "/corporation/working/timesheets/employees", "/corporation/working/employees/logs/timesheet",
            "/corporation/transportation/employees", "/corporation/working/employees",
        ]

        for page in link:
            driver.get(base_url + page)
            try:
                driver.find_element_by_css_selector("button.search")
            except:
                print ("Error in " + page)

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
