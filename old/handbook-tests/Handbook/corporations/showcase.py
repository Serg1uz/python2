# -*- coding: utf-8 -*-
import os
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Showcase(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_showcase(self):
        print("")
        print("Showcase")
        driver = self.driver
        driver.get(base_url + "/corporation/showcases")
        driver.find_element_by_link_text("United Technology Trade Corp.").click()
        driver.find_element_by_link_text("Create").click()
        driver.find_element_by_id("showcase-id").send_keys("CA")
        driver.find_element_by_id("showcase-phone").send_keys("1234567")
        driver.find_element_by_id("showcase-name").send_keys("test")
        driver.find_element_by_id("showcase-address").send_keys("test")
        driver.find_element_by_id("showcase-fax").send_keys("1234567")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("showcase-fax").clear()
        driver.find_element_by_id("showcase-fax").send_keys("643423423465")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def test_showcase_all(self):
        print("")
        print("Showcase all")
        driver = self.driver
        driver.get(base_url + "/corporation/showcases")
        driver.find_element_by_link_text("United Technology Trade Corp.").click()
        driver.find_element_by_link_text("Create").click()
        driver.find_element_by_id("showcase-id").send_keys("CA")
        driver.find_element_by_id("showcase-phone").send_keys("1234567")
        driver.find_element_by_id("showcase-name").send_keys("test")
        driver.find_element_by_id("showcase-address").send_keys("test")
        driver.find_element_by_id("showcase-url").send_keys(
            "http://handbook2-stage.uttc-usa.com/corporation/company/zxcvzvc/showcase")
        driver.find_element_by_id("showcase-fax").send_keys("1234567")
        self.upload("showcase-logoUpload")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("showcase-fax").clear()
        driver.find_element_by_id("showcase-fax").send_keys("643423423465")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
