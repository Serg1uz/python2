# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver
from time import sleep

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Issue(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(15)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_issue(self):
        print("")
        print("Issue")
        driver = self.driver
        self.create_issue(driver, base_url)
        self.update_issue(driver)
        self.clickDeleteButton()
        self.clickSuccessButton()
        try:
            driver.get(base_url + "/corporation/working/issues/check")
            driver.find_element_by_css_selector("button.search").click()
        except:
            print ("Error in " + driver.current_url)

    def test_issue_all(self):
        print("")
        print("Issue all")
        driver = self.driver
        self.create_issue_all_fields(driver, base_url)
        self.update_issue(driver)
        self.clickDeleteButton()
        driver.find_element_by_css_selector("button.search").click()

    def test_issue_quick(self):
        print("")
        print("Issue Quick")
        driver = self.driver
        self.create_issue_quick(driver, base_url)
        driver.find_element_by_link_text("Autotest").click()
        self.update_issue(driver)
        self.clickDeleteButton()
        driver.find_element_by_css_selector("button.search").click()

    def test_issue_spreadsheet(self):
        print ("")
        print ("Issue Spreadsheet")
        driver = self.driver
        driver.get(base_url + "/corporation/working/issues/spreadsheet?statuses=&employees=")
        sleep(6)
        driver.find_element_by_id("issue-0-title").clear()
        driver.find_element_by_id("issue-0-title").send_keys("new test")
        self.clickSuccessButton()

    def create_issue(self, driver, base_url):
        driver.get(base_url + "/corporation/working/issues")
        driver.find_element_by_link_text("Create Issue").click()
        driver.find_element_by_id("issue-title").send_keys("Autotest")
        sleep(2)
        self.dropDownList("select2-issue-unit-uri-container", "//*[@id='select2-issue-unit-uri-results']/li[6]")
        self.cke_editor("cke_issue-description")
        self.clickSaveButton()

    def create_issue_all_fields(self, driver, base_url):
        driver.get(base_url + "/corporation/working/issues")
        driver.find_element_by_link_text("Create Issue").click()
        driver.find_element_by_id("issue-title").send_keys("Test")
        self.cke_editor("cke_issue-description")
        self.dropDownListCss("span.select2-selection.select2-selection--single", "//span[2]/ul/li[2]")
        self.dropDownListCss("input.select2-search__field", "//span/ul/li[5]")
        sleep(1)
        self.dropDownList("select2-issue-employee-uri-container",
                          "//ul[@id='select2-issue-employee-uri-results']/li[@aria-selected='false']")
        sleep(2)
        # self.dropDownList("select2-issue-unit-uri-container", "//*[@id='select2-issue-unit-uri-results']/li[6]")
        # sleep(2)
        driver.find_element_by_xpath("(//input[@type='search'])[2]").click()
        driver.find_element_by_xpath("//ul[@id='select2-issue-labels-uri-results']/li[@aria-selected='false']").click()
        self.dropDownList("select2-issue-frequency-container", "//span[2]/ul/li[3]")
        self.dropDownList("select2-issue-shift-container", "//span[2]/ul/li[2]")
        self.dropDownList("select2-issue-priority-container", "//span[2]/ul/li[2]")
        driver.find_element_by_id("issue-estimatedPeriod").clear()
        driver.find_element_by_id("issue-estimatedPeriod").send_keys("04:00")
        driver.find_element_by_id("issue-startTime").send_keys("2017-10-30 11:00:00")
        driver.find_element_by_id("issue-endTime").send_keys("2017-10-31 12:00:00")
        self.cke_editor("cke_issue-comment")
        self.cke_editor("cke_issue-log")
        self.upload("issue-attachments-upload")
        self.clickSaveButton()

    def update_issue(self, driver):
        self.clickUpdateButton()
        driver.find_element_by_id("issue-title").clear()
        driver.find_element_by_id("issue-title").send_keys("New Autotest")
        self.clickSaveButton()

    def create_issue_quick(self, driver, base_url):
        driver.get(base_url + "/corporation/working/issues")
        driver.find_element_by_css_selector(".quick-create").click()
        self.cke_editor("cke_issue-description")
        sleep(1)
        driver.find_element_by_css_selector("p.action > button.save").click()
        try:
            # иногда валидация не проходит и нужно еще раз нажать save
            sleep(1)
            driver.find_element_by_css_selector("p.action > button.save").click()
        except:
            print ("")
        sleep(5)
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()
        # self.driver.title


if __name__ == "__main__":
    unittest.main()
