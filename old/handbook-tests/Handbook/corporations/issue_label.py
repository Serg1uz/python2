# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class IssueLabel(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_issue_label(self):
        print("")
        print("IssueLabel")
        driver = self.driver
        driver.get(base_url + "/corporation/working/issues/labels")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Issue Label").click()
        driver.find_element_by_id("issueLabel-name").send_keys("test")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("issueLabel-name").clear()
        driver.find_element_by_id("issueLabel-name").send_keys("test 2")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def test_issue_label_all(self):
        print("")
        print("IssueLabel all")
        driver = self.driver
        driver.get(base_url + "/corporation/working/issues/labels")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Issue Label").click()
        driver.find_element_by_id("issueLabel-name").send_keys("test")
        self.cke_editor("cke_issueLabel-description")
        self.dropDownListCss("input.select2-search__field",
                             "//span/ul/li[3]")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("issueLabel-name").clear()
        driver.find_element_by_id("issueLabel-name").send_keys("test 2")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
