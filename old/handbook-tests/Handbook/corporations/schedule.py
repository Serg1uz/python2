# -*- coding: utf-8 -*-
import sys
import unittest

from selenium import webdriver

sys.path.append("..")
from base import base_url, PageCUD
from login import LoginPage


class Schedule(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(15)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_schedule(self):
        print("")
        print("Schedule")
        driver = self.driver
        driver.get(base_url + "/corporation/employees/index")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Employee").click()
        driver.find_element_by_id("employee-firstName").send_keys("test")
        driver.find_element_by_id("employee-lastName").send_keys("test")
        self.dropDownList("employee-hiringDate", "//td[4]")
        self.cke_editor("cke_employee-hiringNote")
        self.dropDownList("select2-employee-unit-uri-container", "//span[2]/ul/li[4]")
        self.dropDownListCss("#select2-employee-position-uri-container > span.select2-selection__placeholder",
                             "//span[2]/ul/li[3]")
        driver.find_element_by_id("employeeContact-phone").send_keys("54638348778")
        self.clickSaveButton()
        employee_url = driver.current_url

        driver.get(base_url + "/corporation/working/employee/ttest")
        driver.find_element_by_xpath("//dd[@class='schedules']/a").click()
        driver.find_element_by_xpath("//td[@class='startDate']/a").click()
        self.clickUpdateButton()
        driver.find_element_by_id("select2-employeeSchedule-shift2-uri-container").click()
        driver.find_element_by_xpath("//ul[@id='select2-employeeSchedule-shift2-uri-results']/li[@aria-selected='false' and contains(text(), '(')]").click()
        self.clickSaveButton()
        self.clickSuccessButton()
        self.driver.find_element_by_xpath("//ul[@class='breadcrumb']/li/a[contains(text(), 'Schedules')]").click()
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Employee Schedule").click()
        driver.find_element_by_id("employeeSchedule-startDate").clear()
        driver.find_element_by_id("employeeSchedule-startDate").send_keys("2020-07-07")
        driver.find_element_by_id("select2-employeeSchedule-mode-container").click()
        driver.find_element_by_xpath("//ul[@id='select2-employeeSchedule-mode-results']/li").click()
        driver.find_element_by_id("select2-employeeSchedule-shift-uri-container").click()
        driver.find_element_by_xpath("//ul[@id='select2-employeeSchedule-shift-uri-results']/li[@aria-selected='false' and contains(text(), '(')]").click()
        driver.find_element_by_id("select2-employeeSchedule-shift2-uri-container").click()
        driver.find_element_by_xpath("//ul[@id='select2-employeeSchedule-shift2-uri-results']/li[@aria-selected='false' and contains(text(), '(')]").click()
        driver.find_element_by_id("employeeSchedule-placeNumber").send_keys("579")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

        driver.get(employee_url)
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
