#!/bin/bash
cd Handbook/access
python access_account.py
cd ../../Handbook/manufacturer
python run_manuf.py
cd ../../Handbook/help
python run_help.py
cd ../../Handbook/corporations
python run_corp.py
cd ../../Handbook/processing
python run_proc.py
cd ../../Handbook/geography
python run_geography.py
cd ../../Handbook/support
python run_support.py
cd ../../Finance
python run_all.py