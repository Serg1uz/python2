# -*- coding: utf-8 -*-
import unittest

from selenium import webdriver
from base import base_url, PageCUD
from login import LoginPage


class Probation(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_probation(self):
        print ("")
        print ("Probation")
        driver = self.driver
        driver.get(base_url + "/personnel/employee/okryzhanovskaia/probation")
        driver.find_element_by_id("employeeProbation-startDate").clear()
        driver.find_element_by_id("employeeProbation-startDate").send_keys("2019-11-05")
        driver.find_element_by_id("employeeProbation-finishDate").clear()
        driver.find_element_by_id("employeeProbation-finishDate").send_keys("2019-12-17")
        driver.find_element_by_id("employeeProbation-depositPeriod").clear()
        driver.find_element_by_id("employeeProbation-depositPeriod").send_keys("2")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("employeeProbation-finishDate").clear()
        driver.find_element_by_id("employeeProbation-finishDate").send_keys("2019-11-15")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()
        try:
            driver.get(base_url + "/personnel/employees/probations/check")
            self.clickSuccessButton()
        except:
            print ("Error in " + driver.current_url)



    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
