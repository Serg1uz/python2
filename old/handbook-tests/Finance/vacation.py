# -*- coding: utf-8 -*-
import unittest

from time import sleep
from selenium import webdriver
from base import base_url, PageCUD
from login import LoginPage


class Vacation(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_vacation(self):
        print ("")
        print ("Vacation")
        driver = self.driver
        driver.get(base_url + "/personnel/vacations")
        driver.find_element_by_link_text("Create Vacation").click()
        driver.find_element_by_id("vacation-year").clear()
        driver.find_element_by_id("vacation-year").send_keys("2020")
        self.cke_editor("cke_vacation-comment")
        self.clickSaveButton()
        driver.get(base_url + "/personnel/vacation/2020")
        self.clickReviseButton()
        driver.find_element_by_xpath("//input[@class='checkbox-select-all']").click()
        sleep(2)
        self.cke_editor_new("cke_vacation-comment")
        self.clickSaveButton()
        for i in range(999):
            try:
                driver.find_element_by_xpath("//ul[@class='pagination']/li[@class='next']/a").click()
            except:
                break
            sleep(2)
            driver.find_element_by_xpath("//input[@class='checkbox-select-all']").click()
            self.clickSaveButton()
            sleep(2)
        self.clickCancelButton()
        self.clickOpenButton()
        driver.find_element_by_css_selector("button.open").click()
        self.clickReviseButton()
        driver.find_element_by_xpath("//input[@class='checkbox-select-all']").click()
        driver.find_element_by_xpath("//input[@class='checkbox-select-all']").click()
        sleep(2)
        self.clickSaveButton()
        for i in range(999):
            try:
                driver.find_element_by_xpath("//ul[@class='pagination']/li[@class='next']/a").click()
            except:
                break
            sleep(2)
            driver.find_element_by_xpath("//input[@class='checkbox-select-all']").click()
            driver.find_element_by_xpath("//input[@class='checkbox-select-all']").click()
            self.clickSaveButton()
            sleep(2)
        self.clickCancelButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
