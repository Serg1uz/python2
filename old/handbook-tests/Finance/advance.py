# -*- coding: utf-8 -*-
import unittest
from time import sleep
from selenium import webdriver
from base import base_url, PageCUD
from login import LoginPage


class Advance(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_advance(self):
        print ("")
        print ("Advance")
        driver = self.driver
        driver.get(base_url + "/personnel/employees/advances")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Employee Advance").click()
        driver.find_element_by_id("employeeAdvance-date-kvdate").click()
        driver.find_element_by_xpath("//div[@class='datepicker-days']/table/tbody/tr[2]/td[2]").click()
        driver.find_element_by_class_name("select2-search__field").send_keys(unicode('Свириденко', 'utf-8'))
        sleep(1)
        driver.find_element_by_xpath("//span[@class='select2-results']/ul/li").click()
        driver.find_element_by_id("employeeAdvance-amount").send_keys("100")
        driver.find_element_by_id("select2-employeeAdvance-currency-code-container").click()
        driver.find_element_by_xpath("//span[@class='select2-results']/ul/li[2]").click()
        self.cke_editor("cke_1_contents")
        self.clickSaveButton()
        driver.get(base_url + "/personnel/employee/asviridenko/advances?dateFrom=&dateTo=")
        driver.find_element_by_xpath("//td[@class='date']/a").click()
        self.clickUpdateButton()
        sleep(2)
        driver.find_element_by_id("employeeAdvance-date").click()
        driver.find_element_by_xpath("//div[@class='datepicker-days']/table/tbody/tr[4]/td[3]").click()
        driver.find_element_by_id("employeeAdvance-amount").clear()
        driver.find_element_by_id("employeeAdvance-amount").send_keys("wrong")
        driver.find_element_by_id("select2-employeeAdvance-currency-code-container").click()
        sleep(2)
        driver.find_element_by_xpath("//span[@class='select2-results']/ul/li[1]").click()
        self.cke_editor_new("cke_1_contents")
        self.clickSaveButton()
        driver.find_element_by_id("employeeAdvance-amount").clear()
        driver.find_element_by_id("employeeAdvance-amount").send_keys("200")
        self.clickSaveButton()
        print ("")
        print ("Advance Installment")
        driver.find_element_by_link_text("Create").click()
        driver.find_element_by_id("employeeAdvanceInstallment-month").click()
        driver.find_element_by_xpath("//span[@class='month'][11]").click()
        driver.find_element_by_id("employeeAdvanceInstallment-amount").clear()
        driver.find_element_by_id("employeeAdvanceInstallment-amount").send_keys("wrong")
        self.clickSaveButton()
        driver.find_element_by_id("employeeAdvanceInstallment-amount").clear()
        driver.find_element_by_id("employeeAdvanceInstallment-amount").send_keys("125")
        self.cke_editor_new("cke_1_contents")
        self.clickSaveButton()
        self.clickDeleteButton()
        driver.find_element_by_xpath("//ul[@class='breadcrumb']/li[6]/a").click()
        self.clickDeleteButton()
        driver.find_element_by_css_selector("button.search").click()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
