# -*- coding: utf-8 -*-
import unittest
from selenium import webdriver
from base import base_url, PageCUD
from login import LoginPage
from time import sleep

class AccountingAccount(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_accounting_account(self):
        print ("")
        print ("Accounting Account")
        driver = self.driver
        driver.get(base_url + "/accounting/accounts")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Account").click()
        driver.find_element_by_id("account-name").send_keys("Autotest")
        self.dropDownList("select2-account-type-container", "//span[@class='select2-results']/ul/li[3]")
        self.dropDownList("select2-account-currency-code-container", "//span[@class='select2-results']/ul/li")
        self.cke_editor("cke_account-description")
        self.upload("account-attachments-upload")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("account-name").clear()
        driver.find_element_by_id("account-name").send_keys("New Autotest")
        self.dropDownList("select2-account-type-container", "//span[@class='select2-results']/ul/li")
        self.dropDownList("select2-account-currency-code-container", "//span[@class='select2-results']/ul/li[2]")
        self.cke_editor_new("cke_account-description")
        driver.find_element_by_xpath("//form[@id='w0']/p/button").click()
        driver.find_element_by_link_text("Child").click()
        driver.find_element_by_id("account-name").send_keys("Autotest Child")
        self.cke_editor("cke_account-description")
        self.upload("account-attachments-upload")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("account-name").clear()
        driver.find_element_by_id("account-name").send_keys("New Child Autotest")
        self.cke_editor_new("cke_account-description")
        driver.find_element_by_xpath("//form[@id='w0']/p/button").click()
        self.clickDeleteButton()
        self.clickDeleteButton()


    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()