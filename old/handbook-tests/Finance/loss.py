# -*- coding: utf-8 -*-
import unittest
from selenium import webdriver
from base import base_url, PageCUD
from login import LoginPage


class Loss(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_loss(self):
        print ("")
        print ("Loss")
        driver = self.driver
        driver.get(base_url + "/personnel/employee/asviridenko/losses?dateFrom=&dateTo=")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Employee Loss").click()
        driver.find_element_by_id("employeeLoss-date").click()
        driver.find_element_by_xpath("//td[@class='day'][5]").click()
        driver.find_element_by_id("employeeLoss-amount").send_keys("100")
        driver.find_element_by_id("select2-employeeLoss-currency-code-container").click()
        driver.find_element_by_xpath("//span[@class='select2-results']/ul/li[@aria-selected='false']").click()
        self.cke_editor("cke_1_contents")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("employeeLoss-date").click()
        driver.find_element_by_xpath("//td[@class='day'][6]").click()
        driver.find_element_by_id("employeeLoss-amount").clear()
        driver.find_element_by_id("employeeLoss-amount").send_keys("wrong")
        driver.find_element_by_id("select2-employeeLoss-currency-code-container").click()
        driver.find_element_by_xpath("//span[@class='select2-results']/ul/li[@aria-selected='false']").click()
        self.cke_editor_new("cke_1_contents")
        self.clickSaveButton()
        driver.find_element_by_id("employeeLoss-amount").clear()
        driver.find_element_by_id("employeeLoss-amount").send_keys("200")
        self.clickSaveButton()
        print ("")
        print ("Loss Installment")
        driver.find_element_by_link_text("Create").click()
        driver.find_element_by_id("employeeLossInstallment-month").click()
        driver.find_element_by_xpath("//span[@class='month'][11]").click()
        driver.find_element_by_id("employeeLossInstallment-amount").clear()
        driver.find_element_by_id("employeeLossInstallment-amount").send_keys("125")
        self.cke_editor("cke_1_contents")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("employeeLossInstallment-amount").clear()
        driver.find_element_by_id("employeeLossInstallment-amount").send_keys("wrong")
        self.clickSaveButton()
        driver.find_element_by_id("employeeLossInstallment-amount").clear()
        driver.find_element_by_id("employeeLossInstallment-amount").send_keys("110")
        self.cke_editor_new("cke_1_contents")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickDeleteButton()
        driver.find_element_by_css_selector("button.search").click()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
