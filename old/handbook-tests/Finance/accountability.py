# -*- coding: utf-8 -*-
import unittest
from selenium import webdriver
from base import base_url, PageCUD
from login import LoginPage


class Accountability(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path='../Webdriver.log')
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_accountability(self):
        print ("")
        print ("Accountability")
        driver = self.driver
        driver.get(base_url + "/personnel/accountability")
        self.clickUpdateButton()
        self.clickSaveButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
