# -*- coding: utf-8 -*-
import unittest
import re

from selenium import webdriver

from base import base_url, PageCUD
from login import LoginPage


class Transfer(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_transfer(self):
        print("")
        print("Transfer")
        driver = self.driver
        driver.get(base_url + "/accounting/transactions/entries")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Transaction").click()
        driver.find_element_by_xpath("//input[@id='transaction-date']").send_keys("2017-12-20")
        driver.find_element_by_css_selector("main").click()
        self.dropDownList("select2-transactionEntry-new-transactionEntry-1-account-uri-container", "//span[2]/ul/li[3]")
        self.dropDownList("select2-transactionEntry-new-transactionEntry-2-account-uri-container", "//span[2]/ul/li[5]")
        driver.find_element_by_id("transactionEntry-new-transactionEntry-1-debit").send_keys("100")
        driver.find_element_by_id("transactionEntry-new-transactionEntry-2-credit").send_keys("100")
        driver.find_element_by_id("transactionEntry-new-transactionEntry-1-memo").send_keys("test1")
        self.upload("transactionEntry-new-transactionEntry-1-attachments-upload")
        self.upload("transactionEntry-new-transactionEntry-2-attachments-upload")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("add-new-transactionEntry").click()
        driver.find_element_by_id("add-new-transactionEntry").click()
        self.dropDownList("select2-transactionEntry-new-transactionEntry-1-account-uri-container", "//span[2]/ul/li[4]")
        driver.find_element_by_id("transactionEntry-new-transactionEntry-1-debit").send_keys("10")
        driver.find_element_by_id("add-new-transactionEntry").click()
        self.dropDownList("select2-transactionEntry-new-transactionEntry-2-account-uri-container", "//span[2]/ul/li[6]")
        driver.find_element_by_id("transactionEntry-new-transactionEntry-2-credit").send_keys("20")
        url = driver.current_url
        tr_id = re.search("\/(?P<id>\d+)\/", url).group("id")
        driver.find_element_by_id("transactionEntry-" + tr_id + "-1-debit").clear()
        driver.find_element_by_id("transactionEntry-" + tr_id + "-1-debit").send_keys("110")
        driver.find_element_by_id("transactionEntry-" + tr_id + "-1-memo").clear()
        driver.find_element_by_id("transactionEntry-" + tr_id + "-1-memo").send_keys("test2")
        self.upload("transactionEntry-new-transactionEntry-1-attachments-upload")
        self.clickSaveButton()
        self.clickDeleteButton()
        driver.find_element_by_xpath("//th[2]")

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
