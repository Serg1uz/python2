# -*- coding: utf-8 -*-
from account import *
from create_inventory import *
from currency import *
from accounting_account import *
from report_payroll_all import *
from transfer import *
from vendor import *
from payroll_wage import *
from loss import *
from advance import *
from vacation import *
from import_from_hb import *
from pages import *
from billing import *
from accountability import *
from working_view import *
from probation import *


if __name__ == "__main__":
    unittest.main()
