# -*- coding: utf-8 -*-
import unittest

from selenium import webdriver

from base import base_url, PageCUD
from login import LoginPage


class ImportFromHB(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_import_from_hb(self):
        print("")
        print("Import")
        driver = self.driver

        link = [
            "/corporation/calendar-turns/import", "/corporation/holidays/import", "/corporation/positions/import",
            "/corporation/units/import", "/corporation/sanctions/import", "/corporation/employees/import",
            "/corporation/locations/import",
        ]

        for page in link:
            driver.get(base_url + page)
            try:
                driver.find_element_by_css_selector("button.search")
            except:
                print ("Error in " + page)

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
