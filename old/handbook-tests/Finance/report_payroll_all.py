# -*- coding: utf-8 -*-
import unittest

from selenium import webdriver
from base import base_url, PageCUD
from login import LoginPage
from time import sleep


class ReportAndPayroll(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_report_payroll(self):
        print("")
        print("Reports and Payrolls all statuses")
        driver = self.driver

        print ("Create Report")
        driver.get(base_url + "/accounting/reports")
        driver.find_element_by_link_text("Create Report").click()
        sleep(1)
        driver.find_element_by_id("report-month").clear()
        driver.find_element_by_id("report-month").send_keys("2019-10")
        self.clickSaveButton()

        print ("Create Vacation")
        driver.get(base_url + "/personnel/vacations")
        driver.find_element_by_link_text("Create Vacation").click()
        driver.find_element_by_id("vacation-year").clear()
        driver.find_element_by_id("vacation-year").send_keys("2019")
        self.cke_editor("cke_vacation-comment")
        self.clickSaveButton()
        driver.get(base_url + "/personnel/vacation/2019")
        self.clickReviseButton()
        sleep(2)
        driver.find_element_by_xpath("//input[@class='checkbox-select-all']").click()
        self.clickSaveButton()
        for i in range(999):
            try:
                driver.find_element_by_xpath("//ul[@class='pagination']/li[@class='next']/a").click()
            except:
                break
            sleep(2)
            driver.find_element_by_xpath("//input[@class='checkbox-select-all']").click()
            self.clickSaveButton()
            sleep(2)

        print ("Create Payroll")
        driver.get(base_url + "/personnel/payrolls")
        driver.find_element_by_link_text("Create Payroll").click()
        sleep(1)
        driver.find_element_by_id("payroll-month").clear()
        driver.find_element_by_id("payroll-month").send_keys("2019-10")
        self.clickSaveButton()

        print ("Update Payroll")
        self.clickUpdateButton()
        driver.find_element_by_id("payroll-exchangeRate").clear()
        driver.find_element_by_id("payroll-exchangeRate").send_keys("32.99")
        self.cke_editor("cke_payroll-comment")
        self.clickSaveButton()

        print ("Complete Payroll")
        self.clickCompleteButton()
        self.clickSaveButton()

        print ("Revise Payroll")
        self.clickReviseButton()
        self.cke_editor_new("cke_payroll-comment")
        sleep(5)
        driver.execute_script("arguments[0].scrollIntoView();",
                              driver.find_element_by_xpath("//input[@class='checkbox-select-all']"))
        driver.execute_script("arguments[0].scrollIntoView();",
                              driver.find_element_by_xpath("//a[@class='cancel']"))
        driver.find_element_by_id("adjustment-amount").clear()
        driver.find_element_by_id("adjustment-amount").send_keys("-5.256")
        driver.find_element_by_id("comment").send_keys("Autotest")
        self.cke_editor("cke_payroll-comment")
        driver.execute_script("arguments[0].scrollIntoView();",
                              driver.find_element_by_xpath("//input[@class='checkbox-select-all']"))
        driver.execute_script("arguments[0].scrollIntoView();",
                              driver.find_element_by_xpath("//a[@class='cancel']"))
        driver.find_element_by_xpath("//input[@class='checkbox-select-all']").click()
        driver.execute_script("arguments[0].scrollIntoView();",
                              driver.find_element_by_xpath("//header/h1"))
        self.clickSaveButton()
        self.clickSuccessButton()

        for i in range(999):
            try:
                driver.find_element_by_xpath("//ul[@class='pagination']/li[@class='next']/a").click()
            except:
                break
            sleep(2)
            driver.execute_script("arguments[0].scrollIntoView();",
                                  driver.find_element_by_xpath("//input[@class='checkbox-select-all']"))
            driver.execute_script("arguments[0].scrollIntoView();",
                                  driver.find_element_by_xpath("//a[@class='cancel']"))
            driver.find_element_by_xpath("//input[@class='checkbox-select-all']").click()
            driver.execute_script("arguments[0].scrollIntoView();",
                                  driver.find_element_by_xpath("//header/h1"))
            self.clickSaveButton()
            self.clickSuccessButton()
        self.clickCancelButton()

        print ("Notify")
        driver.find_element_by_link_text("Notify").click()
        driver.execute_script("arguments[0].scrollIntoView();",
                              driver.find_element_by_xpath("//input[@class='checkbox-select-all']"))
        driver.execute_script("arguments[0].scrollIntoView();",
                              driver.find_element_by_xpath("//a[@class='cancel']"))
        driver.find_element_by_xpath("//input[@class='checkbox-select-all']").click()
        driver.execute_script("arguments[0].scrollIntoView();",
                              driver.find_element_by_xpath("//header/h1"))
        self.clickSaveButton()
        self.clickSuccessButton()
        self.clickCancelButton()

        print ("Open Payroll")
        self.clickOpenButton()
        self.clickSaveButton()
        self.clickCompleteButton()
        self.clickSaveButton()
        self.clickReviseButton()
        sleep(3)
        driver.execute_script("arguments[0].scrollIntoView();",
                              driver.find_element_by_xpath("//input[@class='checkbox-select-all']"))
        driver.execute_script("arguments[0].scrollIntoView();",
                              driver.find_element_by_xpath("//a[@class='cancel']"))
        driver.find_element_by_xpath("//input[@class='checkbox-select-all']").click()
        driver.find_element_by_xpath("//input[@class='checkbox-select-all']").click()
        driver.execute_script("arguments[0].scrollIntoView();",
                              driver.find_element_by_xpath("//header/h1"))
        self.clickSaveButton()
        self.clickSuccessButton()
        for i in range(999):
            try:
                driver.find_element_by_xpath("//ul[@class='pagination']/li[@class='next']/a").click()
            except:
                break
            sleep(2)
            driver.execute_script("arguments[0].scrollIntoView();",
                                  driver.find_element_by_xpath("//input[@class='checkbox-select-all']"))
            driver.execute_script("arguments[0].scrollIntoView();",
                                  driver.find_element_by_xpath("//a[@class='cancel']"))
            driver.find_element_by_xpath("//input[@class='checkbox-select-all']").click()
            driver.find_element_by_xpath("//input[@class='checkbox-select-all']").click()
            driver.execute_script("arguments[0].scrollIntoView();",
                                  driver.find_element_by_xpath("//header/h1"))
            self.clickSaveButton()
            self.clickSuccessButton()
        self.clickCancelButton()
        self.clickOpenButton()
        self.clickSaveButton()

        print ("Delete Payroll")
        self.clickDeleteButton()
        self.clickSuccessButton()

        print ("Open Vacation")
        driver.get(base_url + "/personnel/vacation/2019")
        self.clickOpenButton()
        driver.find_element_by_css_selector("button.open").click()
        self.clickReviseButton()
        sleep(2)
        driver.find_element_by_xpath("//input[@class='checkbox-select-all']").click()
        driver.find_element_by_xpath("//input[@class='checkbox-select-all']").click()
        self.clickSaveButton()
        for i in range(999):
            try:
                driver.find_element_by_xpath("//ul[@class='pagination']/li[@class='next']/a").click()
            except:
                break
            sleep(2)
            driver.find_element_by_xpath("//input[@class='checkbox-select-all']").click()
            driver.find_element_by_xpath("//input[@class='checkbox-select-all']").click()
            self.clickSaveButton()
            sleep(2)
        self.clickCancelButton()
        print ("Delete Vacation")
        self.clickDeleteButton()
        self.clickSuccessButton()

        print ("Update Report")
        driver.get(base_url + "/accounting/report/2019-10")
        self.clickUpdateButton()
        self.cke_editor("cke_report-comment")
        self.clickSaveButton()

        print ("Complete Report")
        self.driver.find_element_by_link_text("Complete").click()
        self.driver.find_element_by_css_selector("button.save").click()

        print ("Revise Report")
        self.clickReviseButton()
        self.cke_editor_new("cke_report-comment")
        driver.find_element_by_class_name("checkbox-select-all").click()
        self.clickSaveButton()
        for i in range(999):
            try:
                driver.find_element_by_xpath("//ul[@class='pagination']/li[@class='next']/a").click()
            except:
                break
            driver.find_element_by_class_name("checkbox-select-all").click()
            self.clickSaveButton()
        self.clickCancelButton()

        print ("Open Report")
        self.clickOpenButton()
        self.clickDeleteOne()
        self.clickCompleteButton()
        self.clickSaveButton()
        self.clickReviseButton()
        driver.find_element_by_class_name("checkbox-select-all").click()
        driver.find_element_by_class_name("checkbox-select-all").click()
        self.clickSaveButton()
        for i in range(999):
            try:
                driver.find_element_by_xpath("//ul[@class='pagination']/li[@class='next']/a").click()
            except:
                break
            driver.find_element_by_class_name("checkbox-select-all").click()
            driver.find_element_by_class_name("checkbox-select-all").click()
            self.clickSaveButton()
        self.clickCancelButton()
        self.clickOpenButton()
        self.clickDeleteOne()

        print ("Delete Report")
        self.clickDeleteButton()
        driver.find_element_by_xpath("//th[2]/a").click()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
