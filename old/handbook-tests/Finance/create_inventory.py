# -*- coding: utf-8 -*-
import unittest

from selenium import webdriver

from base import base_url, PageCUD
from login import LoginPage


class CreateInventory(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_create_inventory(self):
        print("")
        print("Create Inventory")
        driver = self.driver
        driver.get(base_url + "/accounting/inventories")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Inventory").click()
        driver.find_element_by_xpath("//input[@id='transaction-date']").send_keys("2017-12-01")
        self.dropDownList("select2-transactionEntry-new-transactionEntry-1-account-uri-container", "//span[2]/ul/li[4]")
        driver.find_element_by_id("transactionEntry-new-transactionEntry-1-qty").send_keys("2")
        driver.find_element_by_id("inventory-warrantyPeriod").send_keys("2")
        self.dropDownList("select2-inventory-vendor-uri-container", "//span[2]/ul/li[2]")
        self.clickSaveButton()
        self.clickSuccessButton()
        driver.find_element_by_xpath("//td[@class='number']/a").click()
        self.clickUpdateButton()
        driver.find_element_by_id("inventory-number").clear()
        driver.find_element_by_id("inventory-number").send_keys("Autotest")
        driver.find_element_by_id("select2-inventory-location-uri-container").click()
        driver.find_element_by_xpath("//ul[@id='select2-inventory-location-uri-results']/li").click()
        self.upload("inventory-attachments-upload")
        self.clickSaveButton()
        driver.find_element_by_link_text("Write Off").click()
        self.cke_editor("cke_inventory-writeOffNote")
        driver.find_element_by_css_selector("button.delete").click()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
