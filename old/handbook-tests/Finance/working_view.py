# -*- coding: utf-8 -*-
import unittest
from selenium import webdriver
from base import base_url, PageCUD
from login import LoginPage


class WorkingView(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_working_view(self):
        print("")
        print("Working Viewe")
        driver = self.driver

        list = [
            ["/corporation/working/issues/logs", "//td[contains(@class, 'date')]", "//dt[@class='issue']"],
            ["/corporation/working/timesheets/issues", "//td[@class='month']/a", "//dt[@class='issue']"],
            ["/corporation/working/timesheets/employees", "//td[@class='month']/a", "//dt[@class='summary']"],
            ["/corporation/working/timesheets", "//td[@class='month']/a", "//dt[@class='month']"],
            ["/corporation/working/employees/issues/logs?employees=", "//td[@class='startTime']/a", "//dt[@class='employee']"],
            ["/corporation/working/employees/issues/logs?employees=", "//td[@class='employeeLog']/a", "//dt[@class='employee']"],
        ]

        for item in list:
            try:
                driver.get(base_url + item[0])
                driver.find_element_by_xpath(item[1]).click()
                driver.find_element_by_xpath(item[2])
            except:
                print ("Error in " + driver.current_url)

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
