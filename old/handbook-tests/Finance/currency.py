# -*- coding: utf-8 -*-
import unittest

from selenium import webdriver

from base import base_url, PageCUD
from login import LoginPage


class Currency(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_currency(self):
        print("")
        print("Currency")
        driver = self.driver
        driver.get(base_url + "/accounting/currencies")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Currency").click()
        driver.find_element_by_id("currency-code").send_keys("TSW")
        driver.find_element_by_id("currency-number").send_keys("123")
        driver.find_element_by_id("currency-name").send_keys("TesSW")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("currency-name").clear()
        driver.find_element_by_id("currency-name").send_keys("TesSWA")
        self.clickSaveButton()
        self.clickDeleteButton()
        driver.find_element_by_xpath("//th[2]").click()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
