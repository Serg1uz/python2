# -*- coding: utf-8 -*-
import unittest

from selenium import webdriver

from base import base_url, PageCUD
from login import LoginPage


class Vendor(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_vendor(self):
        print("")
        print("Vendor")
        driver = self.driver
        driver.get(base_url + "/accounting/vendors")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Vendor").click()
        driver.find_element_by_id("vendor-name").send_keys("autotest")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("vendor-address").send_keys("autotest")
        driver.find_element_by_id("vendor-phone").send_keys("1232343423423")
        driver.find_element_by_id("vendor-email").send_keys("autotest@test.co")
        driver.find_element_by_id("vendor-name").clear()
        driver.find_element_by_id("vendor-name").send_keys("autotest12")
        self.clickSaveButton()
        self.clickDeleteButton()
        driver.find_element_by_xpath("//th[2]/a").click()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
