# -*- coding: utf-8 -*-
import unittest

from selenium import webdriver
from base import base_url, PageCUD
from login import LoginPage


class Pages(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_pages(self):
        print ("")
        print ("Pages")
        driver = self.driver

        link = [
            "/accounting/accounts/balance/usd", "/accounting/transactions/entries/ledger/usd",
            "/accounting/accounts/profit-loss/usd", "/accounting/accounts/trial/usd", "/personnel/employees",
            "/personnel/payrolls/wages", "/personnel/vacations/employees/timesheet",
            "/personnel/employees/rewards/timesheet", "/personnel/employees/penalties/timesheet",
            "/corporation/transportation/employees/rides", "/corporation/transportation/employees/rides/worksheet",
            "/corporation/transportation/reports/employees", "/corporation/transportation/taxies/rides",
            "/corporation/transportation/taxies/rides/worksheet", "/corporation/transportation/reports/taxies",
            "/corporation/transportation/reports", "/corporation/transportation/employees/taxies/rides",
            "/corporation/transportation/taxies", "/corporation/working/issues/logs",
            "/corporation/working/issues/logs/timesheet", "/corporation/working/timesheets/issues",
            "/corporation/working/timesheets/employees", "/corporation/working/employees/logs/timesheet",
            "/personnel/employees/telegrams", "/personnel/payrolls/wages/telegrams",
            "/corporation/transportation/employees", "/corporation/working/employees",
        ]

        for page in link:
            driver.get(base_url + page)
            try:
                driver.find_element_by_css_selector("button.search")
            except:
                print ("Error in " + page)

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
