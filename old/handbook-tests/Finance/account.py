# -*- coding: utf-8 -*-
import unittest

from selenium import webdriver

from base import base_url, PageCUD
from login import LoginPage


class Account(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_account(self):
        print("")
        print("Account")
        driver = self.driver
        driver.get(base_url + "/access/accounts")
        driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()
        driver.find_element_by_link_text("Create Account").click()
        driver.find_element_by_id("account-username").send_keys("autotestm")
        driver.find_element_by_id("account-email").send_keys("autotestm@test.co")
        self.clickSaveButton()
        self.clickUpdateButton()
        driver.find_element_by_id("account-username").clear()
        driver.find_element_by_id("account-username").send_keys("autotestm32")
        self.clickSaveButton()
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
