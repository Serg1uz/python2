# -*- coding: utf-8 -*-
import unittest

from selenium import webdriver
from base import base_url, PageCUD
from login import LoginPage
from time import sleep


class PayrollWage(unittest.TestCase, PageCUD):
    def setUp(self):
        self.driver = webdriver.Firefox(log_path="../Webdriver.log")
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        login = LoginPage(self.driver)
        login.loginform()

    def test_payroll_wage(self):
        print ("")
        print ("Payroll Wage")
        driver = self.driver

        print ("Create Report")
        driver.get(base_url + "/accounting/reports")
        driver.find_element_by_link_text("Create Report").click()
        sleep(1)
        driver.find_element_by_id("report-month").clear()
        driver.find_element_by_id("report-month").send_keys("2019-10")
        self.clickSaveButton()

        print ("Create Payroll")
        driver.get(base_url + "/personnel/payrolls")
        driver.find_element_by_link_text("Create Payroll").click()
        sleep(1)
        driver.find_element_by_id("payroll-month").clear()
        driver.find_element_by_id("payroll-month").send_keys("2019-10")
        self.clickSaveButton()

        print ("Payroll Wage")
        driver.find_element_by_xpath("//dd[@class='wage-entries']/a").click()
        driver.find_element_by_xpath("//td[@class='month']/a").click()
        print ("Print")
        driver.find_element_by_link_text("Print").click()
        driver.find_element_by_xpath("//header/h1[contains(text(), 'Payroll Wage:')]")
        driver.back()
        print ("Revise")
        driver.find_element_by_link_text("Revise").click()
        driver.find_element_by_id("payrollWage-adjustmentAmount").clear()
        driver.find_element_by_id("payrollWage-adjustmentAmount").send_keys("-2.26")
        self.cke_editor("cke_payrollWage-comment")
        self.clickSaveButton()
        print ("Update")
        driver.find_element_by_link_text("Update").click()
        driver.find_element_by_id("payrollWage-baseRate").clear()
        driver.find_element_by_id("payrollWage-baseRate").send_keys("2.226")
        driver.find_element_by_id("payrollWage-baseRate2").clear()
        driver.find_element_by_id("payrollWage-baseRate2").send_keys("3.226")
        driver.find_element_by_id("payrollWage-bonusRate").clear()
        driver.find_element_by_id("payrollWage-bonusRate").send_keys("0.226")
        driver.find_element_by_id("payrollWage-bonusRate2").clear()
        driver.find_element_by_id("payrollWage-bonusRate2").send_keys("1.226")
        driver.find_element_by_id("payrollWage-comment").send_keys("NewAutotest")
        self.clickSaveButton()
        wage_url = driver.current_url
        print ("Payout")
        try:
            driver.find_element_by_link_text("Payout").click()
            self.clickSaveButton()
            self.clickDeleteButton()
            self.clickSuccessButton()
        except:
            print ("Error in " + driver.current_url)
        driver.get(wage_url)
        print ("Delete Payroll Wage")
        self.clickDeleteButton()
        self.clickSuccessButton()

        print ("Delete Payroll")
        driver.get(base_url + "/personnel/payroll/2019-10-01")
        self.clickDeleteButton()
        self.clickSuccessButton()

        print ("Delete Report")
        driver.get(base_url + "/accounting/report/2019-10")
        self.clickDeleteButton()
        self.clickSuccessButton()

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
