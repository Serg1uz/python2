base_url = "https://devbook2-stage.itdevelopcorp.com"
login = "aokunev"
password = "7777777"
import os

class PageCUD:
    def __init__(self, driver):
        self.driver = driver

    def clickSaveButton(self):
        self.driver.find_element_by_css_selector("button.save").click()

    def clickUpdateButton(self):
        self.driver.find_element_by_link_text("Update").click()

    def clickDeleteButton(self):
        self.driver.find_element_by_link_text("Delete").click()
        self.driver.find_element_by_css_selector("button.delete").click()

    def clickCancelButton(self):
        self.driver.find_element_by_link_text("Cancel").click()

    def clickSuccessButton(self):
        self.driver.find_element_by_css_selector(".alert-success > button.close").click()

    def clickShowButton(self):
        self.driver.find_element_by_xpath("//div[@class='panel-heading']/a[@class='show-advanced info']").click()

    def cke_editor(self, div_id):
        self.driver.find_element_by_xpath("//div[@id='" + div_id + "']//div[contains(@class, 'cke_wysiwyg_div')]").send_keys("Autotest")

    def cke_editor_new(self, div_id):
        self.driver.find_element_by_xpath("//div[@id='" + div_id + "']//div[contains(@class, 'cke_wysiwyg_div')]").clear()
        self.driver.find_element_by_xpath("//div[@id='" + div_id + "']//div[contains(@class, 'cke_wysiwyg_div')]").send_keys("Autotest New")

    def dropDownList(self, div_class, xpath_2):
        self.driver.find_element_by_id(div_class).click()
        self.driver.find_element_by_xpath(xpath_2).click()

    def dropDownListCss(self, div_class, xpath_2):
        self.driver.find_element_by_css_selector(div_class).click()
        self.driver.find_element_by_xpath(xpath_2).click()

    def dropDownListXpath(self, div_class, xpath_2):
        self.driver.find_element_by_xpath(div_class).click()
        self.driver.find_element_by_xpath(xpath_2).click()

    def dropDownListThree(self, div_class, xpath_1, xpath_2):
        self.driver.find_element_by_id(div_class).click()
        self.driver.find_element_by_xpath(xpath_1).click()
        self.driver.find_element_by_xpath(xpath_2).click()

    def upload(self, input_id):
        self.driver.find_element_by_id(input_id).send_keys(os.getcwd() + "/../../Handbook/test.png")
