# -*- coding: utf-8 -*-
from company_dev import *
from showcase_dev import *
from position_dev import *
from unit_dev import *
from outbox_dev import *
from issue_label_dev import *
from issue_dev import *
from holiday_dev import *
from calendar_turn_dev import *
from location_dev import *
from employee_dev import *
from membership_dev import *
from sanction_dev import *
from pages_dev import *
from absence_reason_dev import *
from shifts_dev import *
from absences_dev import *
from  employee_taxi_ride_dev import *
from report_transportation_dev import *
from routine_dev import *
from taxi_dev import *
from transportation_employee_dev import *
from schedule_dev import *
from working_view_dev import *
from timesheet_dev import *


if __name__ == "__main__":
    unittest.main()
