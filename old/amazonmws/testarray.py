# -*- coding: utf-8 -*-
class MyObject():
    def __init__(self, subjects='', froms=''):
        self.subjects = subjects
        self.froms = froms

    def __repr__(self):
        return f'subject = {self.subjects} and froms = {self.froms}'


myarray = []
obj1 = MyObject(subjects='test1', froms='test from1')
obj2 = MyObject(subjects='test2', froms='test from2')

print (myarray)

myarray.append(obj1)
myarray.append(obj2)

print (myarray[1].subjects)
