from django.db import models



class TestModel(models.Model):
    name = models.CharField("Name", max_length=25)
    description = models.CharField("Description", max_length=250)

    def __str__(self):
        return self.name



class Avatars(models.Model):
    name = models.CharField("Name", max_length=10)
    web_url = models.CharField("Web Url", max_length=200)
    local_url = models.CharField("Local Url", max_length=200)

    class Meta:
        verbose_name = "Avatar"
        verbose_name_plural = "Avatars"


class Employees(models.Model):
    account = models.CharField("Account ID", max_length=100, unique=True)
    url = models.CharField("URL", max_length=200)
    name = models.CharField("Name", max_length=100)
    active = models.BooleanField("Active")
    avatars = models.ManyToManyField(Avatars)


    class Meta:
        verbose_name = "Employee"
        verbose_name_plural = "Employees"


class Projects(models.Model):
    name = models.CharField("Name", max_length=100)
    url = models.CharField("URL", max_length=200)
    jira_id = models.CharField("Project ID", max_length=10, unique=True)
    key = models.CharField("Project key", max_length=10, unique=True)
    avatars = models.ManyToManyField(Avatars)

    class Meta:
        verbose_name = "Project"
        verbose_name_plural = "Projects"


class Issues(models.Model):
    jira_id = models.CharField("Issue ID", max_length=10, unique=True)
    url = models.CharField("URL", max_length=200)
    key = models.CharField("Project key", max_length=10, unique=True)
    project = models.ForeignKey(Projects, related_name='issue_project', verbose_name='Project',
                                on_delete=models.PROTECT)
    creator = models.ForeignKey(Employees, related_name='issue_creator', verbose_name='Creator', on_delete=models.PROTECT)
    updater = models.ForeignKey(Employees, related_name='issue_updater', verbose_name='Updater', on_delete=models.PROTECT, blank=True)
    assigned = models.ForeignKey(Employees, related_name='issue_assigned', verbose_name='Assigned',
                                 on_delete=models.PROTECT)
    start_at = models.DateTimeField('Start Date')
    time_spent = models.IntegerField("Time Spend")
    create_at = models.DateTimeField('Create Date', auto_now=True)
    update_at = models.DateTimeField('Create Date', auto_now=True)
    description = models.TextField('Description')

    class Meta:
        verbose_name = "Issue"
        verbose_name_plural = "Issue"


class WorkLogs(models.Model):
    jira_id = models.CharField("WorkLog ID", max_length=10, unique=True)
    url = models.CharField("URL", max_length=200)
    creator = models.ForeignKey(Employees, related_name='worklogs_creator', verbose_name='Creator',
                                on_delete=models.PROTECT)
    updater = models.ForeignKey(Employees, related_name='worklogs_updater', verbose_name='Updater',
                                on_delete=models.PROTECT)
    comment = models.TextField("Comment")
    start_at = models.DateTimeField('Start Date')
    time_spent = models.IntegerField("Time Spend")
    issue = models.ForeignKey(Issues, related_name='worklogs_issue', verbose_name='Issue', on_delete=models.CASCADE)

    class Meta:
        verbose_name = "WorkLog"
        verbose_name_plural = "WorkLogs"
