from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.decorators import action

from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User

from .serializers import TestModelSerializer, AvatarsSerializer, ProjectsSerializer, WorkLogsSerializer, \
    IssuesSerializer, EmployeesSerializer
from .models import TestModel, Projects, Avatars, Issues, Employees, WorkLogs
from .services.JiraLib import JiraLib


class TestModelViewSet(viewsets.ModelViewSet):
    queryset = TestModel.objects.all().order_by('name')
    serializer_class = TestModelSerializer


class EmployeesViewSet(viewsets.ViewSet):
    queryset = Employees.objects.all()
    serializer_class = EmployeesSerializer

    def list(self, request):
        queryset = Employees.objects.all()
        serializer = EmployeesSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Employees.objects.all()
        employee = get_object_or_404(queryset, pk=pk)
        serializer = EmployeesSerializer(employee)
        return Response(serializer.data)

    def destroy(self, request, pk=None):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)

    # url_path = 'employees/jira_sync/'
    @action(detail=True, methods=['get'], url_path='jira_sync', url_name='jira_sync')
    def jira_sync(self, request, pk=None):
        #TODO get out jira params
        options = {"server": "https://itdevelopcorp.atlassian.net/"}
        jira = JiraLib(options=options, basic_auth=('schernov.mdllc@gmail.com', 'ruqhy6qoNItm7w7ms3EXBE54'))
        users = jira.search_all_users(includeActive=True, includeInactive=True)
        for user in users:
            employee = Employees()
            employee.account = user.accountId
            employee.name = user.displayName
            employee.url = user.self
            employee.active = user.active
            employee.save()
        return Response({'status': 'sync complete'})

class EmployeesViewSet(viewsets.ViewSet):
    queryset = Employees.objects.all()
    serializer_class = EmployeesSerializer

    def list(self, request):
        queryset = Employees.objects.all()
        serializer = EmployeesSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Employees.objects.all()
        employee = get_object_or_404(queryset, pk=pk)
        serializer = EmployeesSerializer(employee)
        return Response(serializer.data)

    def destroy(self, request, pk=None):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)

    # url_path = 'employees/jira_sync/'
    @action(detail=True, methods=['get'], url_path='jira_sync', url_name='jira_sync')
    def jira_sync(self, request, pk=None):
        #TODO get out jira params
        options = {"server": "https://itdevelopcorp.atlassian.net/"}
        jira = JiraLib(options=options, basic_auth=('schernov.mdllc@gmail.com', 'ruqhy6qoNItm7w7ms3EXBE54'))
        users = jira.search_all_users(includeActive=True, includeInactive=True)
        for user in users:
            employee = Employees()
            employee.account = user.accountId
            employee.name = user.displayName
            employee.url = user.self
            employee.active = user.active
            employee.save()
        return Response({'status': 'sync complete'})

class ProjectsViewSet(viewsets.ViewSet):
    queryset = Projects.objects.all()
    serializer_class = ProjectsSerializer

    def list(self, request):
        # queryset = Projects.objects.all()
        serializer = self.serializer_class(self.queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        # queryset = Employees.objects.all()
        project = get_object_or_404(self.queryset, pk=pk)
        serializer = self.serializer_class(project)
        return Response(serializer.data)

    def destroy(self, request, pk=None):
        # instance = self.queryset
        # self.perform_destroy(instance)
        # return Response(status=status.HTTP_204_NO_CONTENT)
        pass

    # url_path = 'employees/jira_sync/'
    @action(detail=True, methods=['get'], url_path='jira_sync', url_name='jira_sync')
    def jira_sync(self, request, pk=None):
        #TODO get out jira params
        options = {"server": "https://itdevelopcorp.atlassian.net/"}
        jira = JiraLib(options=options, basic_auth=('schernov.mdllc@gmail.com', 'ruqhy6qoNItm7w7ms3EXBE54'))
        projects = jira.projects()
        print(projects)
        for p in projects:
            project = Projects()
            project.url = p.self
            project.jira_id = p.id
            project.name = p.name
            project.key = p.key
            project.save()

        return Response({'status': 'sync complete'})

class IssuesViewSet(viewsets.ViewSet):
    queryset = Issues.objects.all()
    serializer_class = IssuesSerializer

    def list(self, request):
        # queryset = Projects.objects.all()
        serializer = self.serializer_class(self.queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        # queryset = Employees.objects.all()
        project = get_object_or_404(self.queryset, pk=pk)
        serializer = self.serializer_class(project)
        return Response(serializer.data)

    def destroy(self, request, pk=None):
        # instance = self.queryset
        # self.perform_destroy(instance)
        # return Response(status=status.HTTP_204_NO_CONTENT)
        pass

    # url_path = 'employees/jira_sync/'
    @action(detail=True, methods=['get'], url_path='jira_sync', url_name='jira_sync')
    def jira_sync(self, request, pk=None):
        #TODO get out jira params
        options = {"server": "https://itdevelopcorp.atlassian.net/"}
        jira = JiraLib(options=options, basic_auth=('schernov.mdllc@gmail.com', 'ruqhy6qoNItm7w7ms3EXBE54'))
        # print(request.query_params['project'])
        project = request.query_params['project']
        if project is not None:
            print(project, pk, int(pk)==0)
            if int(pk) == 0:

            #     all
                jql = f'project = {project}'
                issues = jira.search_issues(jql_str=jql, maxResults=50, startAt=0)
                total_issue = issues.total
                print(total_issue)
                start = 0
                res = 50
                complete = total_issue
                while complete > 0:
                    issue_by_project2 = jira.search_issues(jql_str=jql, maxResults=res, startAt=start)
                    complete -= res
                    start += res
                    for i in issue_by_project2:
                        issue = Issues()
                        try:
                            issue.jira_id = i.id
                            issue.url = i.self
                            issue.key = i.key
                            issue.description = i.fields.summary
                            issue.project = Projects.objects.get(jira_id=i.fields.project.id)
                            issue.creator = Employees.objects.get(account=i.fields.reporter.accountId)
                            issue.assigned = Employees.objects.get(pk=1)
                            issue.updater = Employees.objects.get(pk=1) #TODO Employees.objects.get(account=i.fields.updater.accountId)
                            issue.create_at = i.fields.created
                            issue.start_at = i.fields.created
                            issue.time_spent = i.fields.timespent
                            issue.save()
                        except Exception as e:
                            print (f'Error - {e}')
                            pass
            else:
            #     one issue
                pass
            return Response({'status': 'sync complete'})
        return Response({'status': 'Error: need project key'})