from django.urls import include, path
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'test', views.TestModelViewSet)
router.register(r'employees',views.EmployeesViewSet, basename='Employees')
router.register(r'projects',views.ProjectsViewSet, basename='Projects')
router.register(r'issues',views.IssuesViewSet, basename='Issues')

# employees_url = [
#     path(r'employees/', views.EmployeesViewSet.as_view({'get': 'list'}))
# ]

urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]