from rest_framework import serializers

from .models import TestModel, WorkLogs, Projects, Employees, Avatars, Issues

class TestModelSerializer (serializers.ModelSerializer):
    class Meta:
        model = TestModel
        fields = ('name', 'description')

class WorkLogsSerializer (serializers.ModelSerializer):
    class Meta:
        model = WorkLogs
        fields = '__all__'

class ProjectsSerializer (serializers.ModelSerializer):
    class Meta:
        model = Projects
        fields = '__all__'

class EmployeesSerializer (serializers.ModelSerializer):
    class Meta:
        model = Employees
        fields = '__all__'

class AvatarsSerializer (serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Avatars
        fields = '__all__'

class IssuesSerializer (serializers.ModelSerializer):
    class Meta:
        model = Issues
        fields = '__all__'