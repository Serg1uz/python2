from django.contrib import admin

from .models import TestModel,  Employees, WorkLogs, Projects, Avatars, Issues

admin.site.register(TestModel)
admin.site.register(Employees)
admin.site.register(WorkLogs)
admin.site.register(Projects)
admin.site.register(Avatars)
admin.site.register(Issues)




