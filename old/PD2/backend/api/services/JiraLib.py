from jira import JIRA, User


class JiraLib(JIRA):
    def __init__(self, **kwargs):
        super(JiraLib, self).__init__(**kwargs)

    def search_all_users(self, startAt=0, maxResults=50, includeActive=True, includeInactive=False):
        params = {
            'includeActive': includeActive,
            'includeInactive': includeInactive}
        return self._fetch_pages(User, None, 'users/search', startAt, maxResults, params)

    def search_users_by_name(self, user, startAt=0, maxResults=50, includeActive=True, includeInactive=False):
        all_users = self.search_all_users(startAt, maxResults, includeActive, includeInactive)
        users = [u for u in all_users if user in u.displayName]
        return users