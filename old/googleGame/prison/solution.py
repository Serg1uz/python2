# -*- coding: utf-8 -*-

def solution(x,y):
    # result = []
    # if len(x)>=len(y):
    #     result = set(x) -set(y)
    # else:
    #     result = set(y)-set(x)
    #
    # return result
    return list(set(x).symmetric_difference(set(y)))[0]


x = [14, 27, 1, 4, 2, 50, 3, 1]
y = [2, 4, -4, 3, 1, 1, 14, 27, 50]

print(solution(x,y))


