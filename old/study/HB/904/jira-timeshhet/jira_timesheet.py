from jira.client import JIRA
from jira.config import get_jira
import datetime
import dateutil.parser
import json
import argparse


class WorkLog(object):
    def __init__(self, issue_key, updated, author, seconds_spent):
        self._issue_key = issue_key
        self._updated = updated
        self._author = author
        self._seconds_spent = seconds_spent

    @property
    def issue_key(self):
        return self._issue_key

    @property
    def updated(self):
        return self._updated

    @property
    def author(self):
        return self._author

    @property
    def seconds_spent(self):
        return self._seconds_spent

    @property
    def __dict__(self):
        return {'issue_key': self.issue_key,
                'updated': self.updated,
                'author': self.author,
                'seconds_spent': self.seconds_spent}


def get_timesheet(jira, users, date_start, date_end):
    users_str = ','.join(users)
    date_start_str = date_start.strftime('%Y-%m-%d')
    date_end_str = date_end.strftime('%Y-%m-%d')
    jql = 'assignee in (%s) && issueFunction in workLogged("after %s before %s")' % (users_str, date_start_str, date_end_str)

    tickets = jira.search_issues(jql,
        maxResults=65535)

    worklogs = []

    for ticket in tickets:
        for wl in jira.worklogs(ticket.key):
            author = wl.updateAuthor.name
            updated = dateutil.parser.parse(wl.started).date()
            seconds = wl.timeSpentSeconds

            if date_start <= updated <= date_end and author in users:
                worklogs.append(WorkLog(ticket.key,
                                        updated,
                                        author,
                                        int(seconds)))

    return worklogs


def aggregate_timesheet(worklogs):
    total_times = {}

    for w in worklogs:
        print (w.author)
        total_times[w.author] = total_times.get(w.author, 0) + w.seconds_spent

    return total_times


def to_human_readable_duration(seconds):
    hours, rem = divmod(seconds, 3600)
    min, _ = divmod(rem, 60)

    if not hours and not min:
        return "0m"

    hours_str = "%dh" % hours if hours else ""
    min_str = "%dm" % min if min else ""

    return " ".join([hours_str, min_str])


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('--begin', '-b')
    parser.add_argument('--end', '-e')
    parser.add_argument('--summary', '-s', action='store_true', default=False)
    parser.add_argument('--human-readable', '-u', action='store_true', default=False)
    parser.add_argument('users', nargs='+')

    args = parser.parse_args()

    today = datetime.datetime.now().date()

    if args.begin:
        date_begin = dateutil.parser.parse(args.begin).date()
    else:
        date_begin = today - datetime.timedelta(days=today.weekday())

    if args.end:
        date_end = dateutil.parser.parse(args.end).date()
    else:
        date_end = today + datetime.timedelta(days=6-today.weekday())

    jira = get_jira(profile='jira_timesheet')

    worklogs = get_timesheet(jira, args.users, date_begin, date_end)

    if args.summary:
        print (aggregate_timesheet(worklogs))
    else:
        for w in worklogs:
            if args.human_readable:
                time_spent = to_human_readable_duration(w.seconds_spent)
            else:
                time_spent = str(w.seconds_spent)

            print ("\t".join([w.issue_key,
                             str(w.updated),
                             w.author,
                             time_spent]))