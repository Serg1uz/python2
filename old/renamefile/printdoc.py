# -*- coding: utf-8 -*-
import os
import csv
import subprocess

dir_path = os.path.dirname(os.path.realpath(__file__))
# os.chdir(dir_path)

def printfile(file_name):
    if os.path.isfile(file_name):
        subprocess.run("soffice -p '{0}' '{0}'".format(file_name))
        # print("soffice -p '{0}' '{0}'".format(file_name))


#Step 1 read csv
file_csv = 'print.csv'
with open(file_csv) as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    header = next(reader)
    for row in reader:
        printfile(row[0])
