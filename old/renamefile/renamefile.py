# -*- coding: utf-8 -*-
import os
import csv

dir_path = os.path.dirname(os.path.realpath(__file__))
# os.chdir(dir_path)

def renamefile(old_name,new_name):
    for file in os.listdir(dir_path):
        src = file
        src_name, src_extension = os.path.splitext(src)
        if src_name == old_name:
            dst = new_name + src_extension
            os.rename(src, dst)
            print ('rename file '+src+' to '+dst)


#Step 1 read csv
file_csv = 'rename.csv'
with open(file_csv) as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    header = next(reader)
    for row in reader:
        renamefile(row[0],row[1])
