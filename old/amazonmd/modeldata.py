# -*- coding: utf-8 -*-

class ModelData:
    def __init__(self, datetime=None,  settlement_id=None, type=None,  order_id=None,  sku=None,  description=None,  quantity=None,  marketplace=None,  fulfillment=None,  order_city=None,  order_state=None,  order_postal=None,  product_sales=None,  shipping_credits=None,  gift_wrap_credits=None,  promotional_rebates=None,  sales_tax_collected=None,  Marketplace_Facilitator_Tax=None,  selling_fees=None,  fba_fees=None,  other_transaction_fees=None,  other=None,  total=None):
        self.datetime = datetime
        self.settlement_id = settlement_id
        self.type = type
        self.order_id = order_id
        self.sku = sku
        self.description = description
        self.quantity = quantity
        self.marketplace = marketplace
        self.fulfillment = fulfillment
        self.order_city = order_city
        self.order_state = order_state
        self.order_postal = order_postal
        self.product_sales = product_sales
        self.shipping_credits = shipping_credits
        self.gift_wrap_credits = gift_wrap_credits
        self.promotional_rebates = promotional_rebates
        self.sales_tax_collected = sales_tax_collected
        self.Marketplace_Facilitator_Tax = Marketplace_Facilitator_Tax
        self.selling_fees = selling_fees
        self.fba_fees = fba_fees
        self.other_transaction_fees = other_transaction_fees
        self.other = other
        self.total = total

    def __repr__(self):
        return "%s:%s:%s:%s:%s:%s:%s:%s:%s:%s:%s:%s:%s:%s:%s:%s:%s:%s:%s:%s:%s:%s:%s" % (self.datetime, self.settlement_id, self.type, self.order_id, self.sku, self.description,self.quantity, self.marketplace, self.fulfillment, self.order_city, self.order_state,self.order_postal, self.product_sales, self.shipping_credits, self.gift_wrap_credits,self.promotional_rebates, self.sales_tax_collected, self.Marketplace_Facilitator_Tax,self.selling_fees, self.fba_fees, self.other_transaction_fees, self.other, self.total)



