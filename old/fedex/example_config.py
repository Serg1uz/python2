import os
import sys

# Use the fedex directory included in the downloaded package instead of
# any globally installed versions.
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from fedex.config import FedexConfig

# Change these values to match your testing account/meter number.
CONFIG_OBJ = FedexConfig(key='0NC9CCYkPPUrJsMP',
                         password='cZdGPmSmWLXjNdbtwE812u05c',
                         account_number='450964441',
                         meter_number='106316861',
                         # freight_account_number='xxxxxxxxxxx',
                         use_test_server=False)