# -*- coding: utf-8 -*-
import time
from pyvirtualdisplay import Display
from selenium import webdriver

class Taxjar:
    def __init__(self):
        # self.display = Display(visible=0, size=(800, 600))
        # self.display.start()
        self.wd = webdriver.Firefox()
        self.wd.implicitly_wait(10)
        url = 'https://www.taxjar.com/sales-tax-calculator/'
        self.wd.get(url)
        self.wd.find_element_by_xpath(".//button[@class='u-button u-button--color-blue u-button--medium gdpr-accepted']").click()

    def destroy(self):
        self.wd.quit()
        # self.display.stop()

    def get_rates(self,zip_code):
        wd = self.wd
        wd.find_element_by_id('zip').clear()
        wd.find_element_by_id('zip').send_keys(zip_code)
        wd.find_element_by_id('lookupBtn').click()
        time.sleep(2)
        rates = wd.find_element_by_xpath(".//h4[@id='summary']/b").text[:5]
        return rates
