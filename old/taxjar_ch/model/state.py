# -*- coding: utf-8 -*-
import csv


class State:
    def __init__(self,abbr=None,zip_code=None,tax_region=None,old_tax=None,new_tax=None):
        self.abbr = abbr
        self.zip_code = zip_code
        self.tax_region = tax_region
        self.old_tax = old_tax
        self.new_tax = new_tax

    def __repr__(self):
        return "%s:%s:%s:%s:%s" % (self.abbr, self.zip_code, self.tax_region, self.old_tax, self.new_tax)

    def load_from_csv(self, csv_file, delim):
        states = []
        with open(csv_file) as file:
            reader=csv.reader(file, delimiter=delim)
            states.append(reader)
        return states
