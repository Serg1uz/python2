# -*- coding: utf-8 -*-
import sys
import time

import click

import jsonpickle as jsonpickle
from dictionary import Dictionary
from module.taxjar import Taxjar

st_abbr = ['CO', 'IL', 'NC', 'NY', 'OK', 'VT', 'WA', 'NJ']


def load_from_json(file):
    with open(file) as f:
        return jsonpickle.decode(f.read())


def save_to_json(obj, file):
    f = open(file, 'w')
    json_obj = jsonpickle.encode(obj)
    f.write(json_obj)
    f.close()


states = Dictionary()
states.set(load_from_json('tax2.json'))

dl = 0
total = len(states)

print(total)
# # state = State()
# client = Taxjar()
#
# for state in states:
#     dl += 1
#     rates = client.get_rates(state['zip_code'])
#     state['new_tax'] = float(rates) / 100
#     print(dl)
#
#     # print(state)
#
# print(states)
#
# save_to_json(states, 'tax2.json')
#
# client.destroy()

print('end')
