import time
from selenium import webdriver
from selenium.webdriver.firefox.options import Options as FirefoxOptions


def main():
    options = FirefoxOptions()
    options.add_argument("--headless")
    driver = webdriver.Firefox(options=options)

    tickets = ['BB3-116 ', 'BB3-115 ', 'BB3-114 ', 'BB3-113 ', 'BB3-112 ', 'BB3-111 ', 'BB3-110 ', 'BB3-109 ', 'BB3-108 ', 'BB3-107 ', 'BB3-106 ', 'BB3-105 ', 'BB3-104 ', 'BB3-103 ', 'BB3-102 ', 'BB3-101 ', 'BB3-100 ', 'BB3-97 ', 'BB3-95 ', 'BB3-94 ', 'BB3-93 ', 'BB3-92 ', 'BB3-91 ', 'BB3-90 ', 'BB3-89 ', 'BB3-88 ', 'BB3-87 ', 'BB3-86 ', 'BB3-85 ', 'BB3-84 ', 'BB3-83 ', 'BB3-82 ', 'BB3-81 ', 'BB3-80 ', 'BB3-79 ', 'BB3-77 ', 'BB3-76 ', 'BB3-75 ', 'BB3-74 ', 'BB3-73 ', 'BB3-72 ', 'BB3-71 ', 'BB3-70 ', 'BB3-69 ', 'BB3-68 ', 'BB3-67 ', 'BB3-66 ', 'BB3-65 ', 'BB3-64 ', 'BB3-63 ', 'BB3-62 ', 'BB3-61 ', 'BB3-60 ', 'BB3-59 ', 'BB3-58 ', 'BB3-57 ', 'BB3-56 ', 'BB3-55 ', 'BB3-54 ', 'BB3-53 ', 'BB3-52 ', 'BB3-51 ', 'BB3-50 ', 'BB3-49 ', 'BB3-48 ', 'BB3-47 ', 'BB3-46 ', 'BB3-45 ', 'BB3-44 ', 'BB3-43 ', 'BB3-42 ', 'BB3-41 ', 'BB3-40 ', 'BB3-39 ', 'BB3-38 ', 'BB3-37 ', 'BB3-36 ', 'BB3-35 ', 'BB3-34 ', 'BB3-33 ', 'BB3-32 ', 'BB3-31 ', 'BB3-30 ', 'BB3-29 ', 'BB3-28 ', 'BB3-27 ', 'BB3-26 ', 'BB3-25 ', 'BB3-24 ', 'BB3-23 ', 'BB3-22 ', 'BB3-21 ', 'BB3-20 ', 'BB3-19 ', 'BB3-18 ', 'BB3-17 ', 'BB3-16 ', 'BB3-15 ', 'BB3-14 ', 'BB3-13 ', 'BB3-12 ', 'BB3-11 ', 'BB3-10 ', 'BB3-9 ', 'BB3-8 ', 'BB3-7 ', 'BB3-5 ', 'BB3-3']
    for ticket in tickets:
        link = f'https://itdevelopcorp.atlassian.net/browse/{ticket}'
        driver.get(link)
        time.sleep(2)
        new_link = driver.current_url
        print(new_link)
        exit()
    # driver.get("https://legacy.export.gov/csl-search")
    # time.sleep(1)
    #
    # name = driver.find_element_by_id('name')
    # name.send_keys('Ali')
    # button = driver.find_element_by_class_name('explorer__form__submit.pure-button.pure-button-primary')
    # button.click()
    #
    # time.sleep(1)
    # ele = driver.find_element_by_class_name('page-wrapper')
    # total_height = ele.size["height"] + 1000
    #
    # driver.set_window_size(1920, total_height)  # the trick
    # time.sleep(1)
    # driver.save_screenshot("screenshot1.png")
    driver.quit()

if __name__ == "__main__":
    # execute only if run as a script
    main()