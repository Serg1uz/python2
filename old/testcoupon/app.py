# -*- coding: utf-8 -*-
import requests
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

query = '{"couponCode":"Lovemega","productUriList":[' \
       '"greenlee-ebs12ml110-10-8v-micro-bolt-cutting-tool-110v"],' \
       '"accountNumber":"A-MD091118-1","productsList":[{' \
       '"uri":"greenlee-ebs12ml110-10-8v-micro-bolt-cutting-tool-110v","qty":"1","amount":673}],"showcase":"MD"}'
# headers = '{"Content-type":"application/json"}'

r = requests.get("https://handbook.megadepotllc.com/getDiscount.php", data=query,  verify=False)

print(r.text)

#
# curl -d '{"couponCode":"Lovemega","productUriList":["greenlee-ebs12ml110-10-8v-micro-bolt-cutting-tool-110v"],' \
#         '"accountNumber":"A-MD091118-1","productsList":[{"uri":"greenlee-ebs12ml110-10-8v-micro-bolt-cutting-tool-110v","qty":"1","amount":673}],"showcase":"MD"}' -H "Content-Type: application/json" -X POST https://handbook.megadepotllc.com/getDiscount.php