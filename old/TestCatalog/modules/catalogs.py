# -*- coding: utf-8 -*-
import requests
import logging

module_logger = logging.getLogger('TestCatalog.catalog')


class catalogs:
    base_url = 'https://new-catalog.megadepotllc.com:5557/'
    session = None
    response = None
    status_code = None
    logger = None

    def __init__(self):
        self.logger = logging.getLogger("JiraWorkLogs.DevBook.init")
        self.logger.info("Init DevBook Session")
        try:
            self.session = requests.Session()
            self.response = self.session.get(self.base_url, verify=False)
            self.status_code = self.response.status_code
        except Exception as e:
            self.logger.error('Runtime error', e)

    def getFeaturedBrandsByCategory(self, uri_list=[], showcase='', debug=1):
        """
        uriList - uri_list: [] /*список uri департментов. Если не указывать - выводить весь список департментов по
        витрине*/
        showcase:  абрревиатура витрины
        :return:
        """
        self.logger = logging.getLogger("JiraWorkLogs.DevBook.getFeaturedBrandsByCategory")
        self.logger.info("getFeaturedBrandsByCategory")

        name_service = 'getFeaturedBrandsByCategory.php'
        data = {"showcase": showcase, "debug": debug}
        if len(uri_list) > 0: data = {"uriList": uri_list, "showcase": showcase, "debug": debug}
        self.response = self.session.get(self.base_url + name_service, data=data)
        print(self.base_url + name_service)
        return self.response
