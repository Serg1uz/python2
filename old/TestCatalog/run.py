# -*- coding: utf-8 -*-
import logging
import logging.config

from modules.catalogs import catalogs

logger = None


def enable_log():
    """
    enabled logger config
    """
    global logger
    logging.config.fileConfig('logging.conf')
    logger = logging.getLogger('TestCatalog')


def main():
    enable_log()
    logger.info('Start app')

    cat = catalogs()
    print(cat.getFeaturedBrandsByCategory(showcase='TD').text)
    logger.info('End app')


if __name__ == '__main__':
    main()
