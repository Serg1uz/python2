def is_palindrom(number):
    if str(number) == str(number)[::-1]:
        return True
    return False

n = int(10E7)

count = 0
result = []
for i in xrange(1,n):
    sum = i**2
    if sum>n: break
    for j in xrange(i+1,n):
        sum += j**2      
        if sum>n: break
        if is_palindrom(sum): result.append(sum)
        
        
   
print(len(result))